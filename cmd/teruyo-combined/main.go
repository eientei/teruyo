// Package main provides combined binary with all services compiled in
package main

import (
	"context"
	"fmt"
	"os"
	"regexp"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"
	"gitlab.eientei.org/eientei/teruyo/internal/constructor/flake"
	"gitlab.eientei.org/eientei/teruyo/internal/constructor/repository/cassandra"
	"gitlab.eientei.org/eientei/teruyo/internal/envar"
	"gitlab.eientei.org/eientei/teruyo/internal/model"
	"gitlab.eientei.org/eientei/teruyo/internal/tracer"
	"gitlab.eientei.org/eientei/teruyo/internal/zerolog/hook/lineinfo"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/propagation"
)

func main() {
	ctx := context.Background()

	log.Logger = log.Output(zerolog.ConsoleWriter{
		Out:        os.Stderr,
		TimeFormat: time.RFC3339,
	}).Hook(&lineinfo.Hook{
		Depth: 4,
		SkipPatterns: []*regexp.Regexp{
			regexp.MustCompile(`internal/locked`),
			regexp.MustCompile(`internal/zerolog/wrapper`),
		},
	})

	ctx = log.Logger.WithContext(ctx)

	globalTracer, tracerShutdown, err := tracer.New("teruyo-combined")
	if err != nil {
		panic(err)
	}

	defer tracerShutdown()

	otel.SetTracerProvider(globalTracer)
	otel.SetTextMapPropagator(
		propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}, propagation.Baggage{}),
	)

	envar.PrintEnv()

	flakeGenerator := flake.New()

	repo, err := cassandra.New(ctx, flakeGenerator, globalTracer)
	if err != nil {
		panic(err)
	}

	/*
		for i := 0; i < 10240; i++ {
			err = repo.CollectionItemSave(ctx, &model.CollectionItem{
				Created: time.Time{},
				Type:    model.CollectionTypeLikes,
				Source:  "foo",
				Target:  vocab.ID(uuid.New().String()),
				Data:    nil,
			})
			if err != nil {
				panic(err)
			}
		}

	*/

	res, err := repo.CollectionItemList(
		ctx,
		[]vocab.IRI{"foo"},
		[]model.CollectionType{model.CollectionTypeLikes},
	)
	if err != nil {
		panic(err)
	}

	fmt.Println(len(res))
}
