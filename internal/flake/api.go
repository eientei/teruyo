// Package flake provides 128-bit flake ID generation
// FlakeID is a variation on snowflakeID/ULID, a time-ordered 128-bit ID that can be generated on distributed workers
// layout:
// - 64-bit timestamp since UNIX epoch with millisecond resolution
// - 48-bit worker ID [only 16 bits actually used in this implementation]
// - 16 bit serial counter within millisecond
package flake

import (
	"errors"
	"math/bits"
	"sync/atomic"
	"time"

	"gitlab.eientei.org/eientei/teruyo/internal/byteops"
)

var errInvalidID = errors.New("invalid ID")

const (
	base62CharacterSet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	invMaskSequence    = 0xFFFFFFFFFF0000
	waitFraction       = time.Microsecond * 10
)

// Generator flake ID generator
type Generator interface {
	Next() ID
	MachineID() uint16
}

// New returns new instance of flake ID generator using machineID provided
func New(machineID uint16) Generator {
	return &generator{
		machineID: uint64(machineID) << 16,
	}
}

// ID flake ID
type ID struct {
	Time     uint64
	Instance uint64
}

// String base62 encoding
func (id ID) String() string {
	var runes []byte

	var r uint64

	for id.Time != 0 || id.Instance != 0 {
		id.Time, r = bits.Div64(0, id.Time, 62)
		id.Instance, r = bits.Div64(r, id.Instance, 62)

		runes = append(runes, base62CharacterSet[r])
	}

	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}

	return string(runes)
}

// Bytes 16-byte big-endian slice representation
func (id ID) Bytes() []byte {
	return byteops.UInt128Marshal(id.Time, id.Instance)
}

// IsZero returns true for 0,0 flake ID
func (id ID) IsZero() bool {
	return id.Time == 0 && id.Instance == 0
}

// FromBytes parses 16-byte slice representation
func FromBytes(bs []byte) (id ID, err error) {
	id.Time, id.Instance, err = byteops.UInt128Unmarshal(bs)

	return
}

// Parse base62-encoded flake ID
func Parse(s string) (res ID, err error) {
	if len(s) > 22 {
		return ID{}, errInvalidID
	}

	runes := ([]rune)(s)

	var tmp ID

	var hi, n, lo uint64

	power := ID{Time: 0, Instance: 62}

	first := true

	for i := len(runes) - 1; i >= 0; i-- {
		switch {
		case runes[i] < '0' || runes[i] > 'z':
			return ID{}, errInvalidID
		case runes[i] <= '9':
			n = uint64(runes[i] - '0')
		case runes[i] <= 'Z':
			n = uint64(runes[i] - '0' - 7)
		case runes[i] <= 'z':
			n = uint64(runes[i] - '0' - 13)
		}

		if first {
			res.Instance = n
			first = false

			continue
		}

		// tmp = power * n
		hi, tmp.Instance = bits.Mul64(power.Instance, n)
		_, lo = bits.Mul64(power.Time, n)
		tmp.Time, _ = bits.Add64(hi, lo, 0)

		// res += tmp
		res.Instance, hi = bits.Add64(tmp.Instance, res.Instance, 0)
		res.Time, _ = bits.Add64(tmp.Time, res.Time, hi)

		// power *= 62
		hi, power.Instance = bits.Mul64(power.Instance, 62)
		_, lo = bits.Mul64(power.Time, 62)
		power.Time, _ = bits.Add64(hi, lo, 0)
	}

	return
}

type generator struct {
	machineID uint64
	timestamp uint64
	sequence  uint64
}

// MachineID returns worker ID
func (g *generator) MachineID() uint16 {
	return uint16((g.machineID >> 16) & 0xFFFF)
}

// Next returns next ID, blocking in spin-lock in case all 16-bit sequence range is exhausted
func (g *generator) Next() ID {
	var ts, seq uint64

	for {
		ts = uint64(time.Now().UnixMilli())

		if atomic.CompareAndSwapUint64(&g.timestamp, ts, ts) {
			seq = atomic.AddUint64(&g.sequence, 1)

			if (seq & invMaskSequence) != 0 {
				time.Sleep(waitFraction)

				// spin until millisecond changes, should seldom happen

				continue
			}
		} else {
			atomic.StoreUint64(&g.timestamp, ts)
			atomic.StoreUint64(&g.sequence, 0)
		}

		break
	}

	return ID{
		Time:     ts,
		Instance: g.machineID | seq,
	}
}
