package flake

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStringParse(t *testing.T) {
	id := ID{0x9e107d9d372bb682, 0x6bd81d3542a419d6}

	assert.Equal(t, "4oGK5AaNEEtflKvmIgkiBC", id.String())

	parsed, err := Parse(id.String())
	assert.NoError(t, err)

	assert.Equal(t, id, parsed)
}

func TestBytesParse(t *testing.T) {
	id := ID{0x9e107d9d372bb682, 0x6bd81d3542a419d6}

	assert.Equal(
		t,
		[]byte{
			0x9e, 0x10, 0x7d, 0x9d, 0x37, 0x2b, 0xb6, 0x82,
			0x6b, 0xd8, 0x1d, 0x35, 0x42, 0xa4, 0x19, 0xd6,
		},
		id.Bytes(),
	)

	parsed, err := FromBytes(id.Bytes())
	assert.NoError(t, err)

	assert.Equal(t, id, parsed)
}

func TestGenerator(t *testing.T) {
	g := New(123)

	m := make(map[uint64]int)

	for i := 0; i < 16384*2; i++ {
		m[g.Next().Time]++
	}

	for _, v := range m {
		assert.LessOrEqual(t, v, 16384)
	}

	assert.GreaterOrEqual(t, len(m), 2)
}

func BenchmarkGenerator(b *testing.B) {
	g := New(123)

	for i := 0; i < b.N; i++ {
		g.Next()
	}
}
