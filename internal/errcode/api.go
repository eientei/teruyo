// Package errcode provides errors enriched with general classification of error code
package errcode

import (
	"errors"
	"fmt"
)

// Translator function signature returns coded error from raw error instance
type Translator func(error) *Error

// Translate processes error with list of translators
func Translate(err error, translators ...Translator) *Error {
	if err == nil {
		return nil
	}

	var tgt *Error

	if errors.As(err, &tgt) {
		return tgt
	}

	for _, trans := range translators {
		tgt = trans(err)
		if tgt != nil {
			return tgt
		}
	}

	return CodeUnknown.From(err)
}

// Error with code
type Error struct {
	Original error
	Message  string
	Service  string
	Code     Code
}

// Error message
func (coded *Error) Error() string {
	if coded.Original != nil {
		return coded.Original.Error()
	}

	return coded.Message
}

// Extensions error details
func (coded *Error) Extensions() map[string]interface{} {
	m := map[string]interface{}{
		"code":    coded.Code,
		"details": coded.Message,
	}

	return m
}

// Unwrap implementation
func (coded *Error) Unwrap() error {
	return coded.Original
}

// Code error code
type Code string

// Error implementation
func (c Code) Error() string {
	return string(c)
}

// Extensions implementation
func (c Code) Extensions() map[string]interface{} {
	m := map[string]interface{}{
		"code":    c,
		"details": c.Error(),
	}

	return m
}

// From returns error wrapped in code instance
func (c Code) From(err error) *Error {
	return &Error{
		Original: err,
		Code:     c,
	}
}

// New returns new error from message wrapped in code instance
func (c Code) New(message string) *Error {
	return &Error{
		Message: message,
		Code:    c,
	}
}

// Newf returns new formatted error from message wrapped in code instance
func (c Code) Newf(format string, args ...interface{}) *Error {
	return &Error{
		Message: fmt.Sprintf(format, args...),
		Code:    c,
	}
}

// Known codes
const (
	CodeUnknown          Code = "unknown"
	CodeNotAuthenticated Code = "not_authenticated"
	CodeNotAuthorized    Code = "not_authorized"
	CodeNotFound         Code = "not_found"
	CodeInvalidData      Code = "invalid_data"
	CodeInvalidState     Code = "invalid_state"
	CodeInternal         Code = "internal"
)
