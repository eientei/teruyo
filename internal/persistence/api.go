// Package persistence provides common utils for persistent storages
package persistence

import (
	"context"
	"runtime"
	"strings"

	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
)

const (
	instrumentationName    = "gitlab.eientei.org/eientei/teruyo/internal/persistence"
	instrumentationVersion = "1.0.0"
)

// Tracer function-scoped persistent tracer
type Tracer struct {
	trace.Tracer
	kind string
	repo string
}

// NewTracer creates new Tracer instance
func NewTracer(repo, kind string, tracerProvider trace.TracerProvider) *Tracer {
	return &Tracer{
		Tracer: tracerProvider.Tracer(
			instrumentationName+"."+repo,
			trace.WithInstrumentationVersion(instrumentationVersion),
			trace.WithInstrumentationAttributes(attribute.String("repository", repo)),
		),
		kind: kind,
		repo: repo,
	}
}

// NewTracerSvc returns new tracer for a service
func NewTracerSvc(repo string, tracerProvider trace.TracerProvider) *Tracer {
	return NewTracer(repo, "svc", tracerProvider)
}

// NewTracerDB returns new tracer for a database
func NewTracerDB(repo string, tracerProvider trace.TracerProvider) *Tracer {
	return NewTracer(repo, "db", tracerProvider)
}

// Trace starts a span using calling function signature and updates context pointer with span-included context,
// returning deferrable closure which would end the span and update it's state using pointer to error provided
func (t *Tracer) Trace(ctx *context.Context, err *error) func() {
	span := trace.SpanFromContext(*ctx)

	if !span.IsRecording() && span.SpanContext().IsValid() {
		return func() {}
	}

	rpc := make([]uintptr, 2)

	runtime.Callers(2, rpc)

	frames := runtime.CallersFrames(rpc)

	var name string

	for f, m := frames.Next(); m; f, m = frames.Next() {
		idx := strings.LastIndex(f.Function, ".") + 1
		name = f.Function[idx:]
	}

	nctx, span := t.Tracer.Start(
		*ctx,
		t.kind+" "+t.repo+"."+name,
		trace.WithSpanKind(trace.SpanKindInternal),
		trace.WithAttributes(attribute.String("method", name)),
	)

	*ctx = nctx

	return func() {
		if *err != nil {
			span.RecordError(*err)
			span.SetStatus(codes.Error, (*err).Error())
		}

		span.End()
	}
}
