package cassandra

import (
	"context"

	"github.com/gocql/gocql"
)

type batch gocql.Batch

func (b *batch) BatchEntries() []gocql.BatchEntry {
	return (*gocql.Batch)(b).Entries
}

func (b *batch) Trace(trace gocql.Tracer) Batch {
	return (*batch)((*gocql.Batch)(b).Trace(trace))
}

func (b *batch) Observer(observer gocql.BatchObserver) Batch {
	return (*batch)((*gocql.Batch)(b).Observer(observer))
}

func (b *batch) Keyspace() string {
	return (*gocql.Batch)(b).Keyspace()
}

func (b *batch) Table() string {
	return (*gocql.Batch)(b).Table()
}

func (b *batch) Attempts() int {
	return (*gocql.Batch)(b).Attempts()
}

func (b *batch) AddAttempts(i int, host *gocql.HostInfo) {
	(*gocql.Batch)(b).AddAttempts(i, host)
}

func (b *batch) Latency() int64 {
	return (*gocql.Batch)(b).Latency()
}

func (b *batch) AddLatency(l int64, host *gocql.HostInfo) {
	(*gocql.Batch)(b).AddLatency(l, host)
}

func (b *batch) GetConsistency() gocql.Consistency {
	return (*gocql.Batch)(b).GetConsistency()
}

func (b *batch) SetConsistency(c gocql.Consistency) {
	(*gocql.Batch)(b).SetConsistency(c)
}

func (b *batch) Context() context.Context {
	return (*gocql.Batch)(b).Context()
}

func (b *batch) IsIdempotent() bool {
	return (*gocql.Batch)(b).IsIdempotent()
}

func (b *batch) SpeculativeExecutionPolicy(sp gocql.SpeculativeExecutionPolicy) Batch {
	return (*batch)((*gocql.Batch)(b).SpeculativeExecutionPolicy(sp))
}

func (b *batch) Query(stmt string, args ...interface{}) {
	(*gocql.Batch)(b).Query(stmt, args...)
}

func (b *batch) Bind(stmt string, bind func(q *gocql.QueryInfo) ([]interface{}, error)) {
	(*gocql.Batch)(b).Bind(stmt, bind)
}

func (b *batch) RetryPolicy(r gocql.RetryPolicy) Batch {
	return (*batch)((*gocql.Batch)(b).RetryPolicy(r))
}

func (b *batch) WithContext(ctx context.Context) Batch {
	return (*batch)((*gocql.Batch)(b).WithContext(ctx))
}

func (b *batch) Cancel() {
	(*gocql.Batch)(b).Cancel()
}

func (b *batch) Size() int {
	return (*gocql.Batch)(b).Size()
}

func (b *batch) SerialConsistency(cons gocql.SerialConsistency) Batch {
	return (*batch)((*gocql.Batch)(b).SerialConsistency(cons))
}

func (b *batch) DefaultTimestamp(enable bool) Batch {
	return (*batch)((*gocql.Batch)(b).DefaultTimestamp(enable))
}

func (b *batch) WithTimestamp(timestamp int64) Batch {
	return (*batch)((*gocql.Batch)(b).WithTimestamp(timestamp))
}

func (b *batch) GetRoutingKey() ([]byte, error) {
	return (*gocql.Batch)(b).GetRoutingKey()
}
