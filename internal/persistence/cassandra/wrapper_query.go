package cassandra

import (
	"context"

	"github.com/gocql/gocql"
)

type query gocql.Query

func (q *query) Statement() string {
	return (*gocql.Query)(q).Statement()
}

func (q *query) Values() []interface{} {
	return (*gocql.Query)(q).Values()
}

func (q *query) String() string {
	return (*gocql.Query)(q).String()
}

func (q *query) Attempts() int {
	return (*gocql.Query)(q).Attempts()
}

func (q *query) AddAttempts(i int, host *gocql.HostInfo) {
	(*gocql.Query)(q).AddAttempts(i, host)
}

func (q *query) Latency() int64 {
	return (*gocql.Query)(q).Latency()
}

func (q *query) AddLatency(l int64, host *gocql.HostInfo) {
	(*gocql.Query)(q).AddLatency(l, host)
}

func (q *query) Consistency(c gocql.Consistency) Query {
	return (*query)((*gocql.Query)(q).Consistency(c))
}

func (q *query) GetConsistency() gocql.Consistency {
	return (*gocql.Query)(q).GetConsistency()
}

func (q *query) SetConsistency(c gocql.Consistency) {
	(*gocql.Query)(q).SetConsistency(c)
}

func (q *query) CustomPayload(customPayload map[string][]byte) Query {
	return (*query)((*gocql.Query)(q).CustomPayload(customPayload))
}

func (q *query) Context() context.Context {
	return (*gocql.Query)(q).Context()
}

func (q *query) Trace(trace gocql.Tracer) Query {
	return (*query)((*gocql.Query)(q).Trace(trace))
}

func (q *query) Observer(observer gocql.QueryObserver) Query {
	return (*query)((*gocql.Query)(q).Observer(observer))
}

func (q *query) PageSize(n int) Query {
	return (*query)((*gocql.Query)(q).PageSize(n))
}

func (q *query) DefaultTimestamp(enable bool) Query {
	return (*query)((*gocql.Query)(q).DefaultTimestamp(enable))
}

func (q *query) WithTimestamp(timestamp int64) Query {
	return (*query)((*gocql.Query)(q).WithTimestamp(timestamp))
}

func (q *query) RoutingKey(routingKey []byte) Query {
	return (*query)((*gocql.Query)(q).RoutingKey(routingKey))
}

func (q *query) WithContext(ctx context.Context) Query {
	return (*query)((*gocql.Query)(q).WithContext(ctx))
}

func (q *query) Cancel() {
	(*gocql.Query)(q).Cancel()
}

func (q *query) Keyspace() string {
	return (*gocql.Query)(q).Keyspace()
}

func (q *query) Table() string {
	return (*gocql.Query)(q).Table()
}

func (q *query) GetRoutingKey() ([]byte, error) {
	return (*gocql.Query)(q).GetRoutingKey()
}

func (q *query) Prefetch(p float64) Query {
	return (*query)((*gocql.Query)(q).Prefetch(p))
}

func (q *query) RetryPolicy(r gocql.RetryPolicy) Query {
	return (*query)((*gocql.Query)(q).RetryPolicy(r))
}

func (q *query) SetSpeculativeExecutionPolicy(sp gocql.SpeculativeExecutionPolicy) Query {
	return (*query)((*gocql.Query)(q).SetSpeculativeExecutionPolicy(sp))
}

func (q *query) IsIdempotent() bool {
	return (*gocql.Query)(q).IsIdempotent()
}

func (q *query) Idempotent(value bool) Query {
	return (*query)((*gocql.Query)(q).Idempotent(value))
}

func (q *query) Bind(v ...interface{}) Query {
	return (*query)((*gocql.Query)(q).Bind(v...))
}

func (q *query) SerialConsistency(cons gocql.SerialConsistency) Query {
	return (*query)((*gocql.Query)(q).SerialConsistency(cons))
}

func (q *query) PageState(state []byte) Query {
	return (*query)((*gocql.Query)(q).PageState(state))
}

func (q *query) NoSkipMetadata() Query {
	return (*query)((*gocql.Query)(q).NoSkipMetadata())
}

func (q *query) Exec() error {
	return (*gocql.Query)(q).Exec()
}

func (q *query) Iter() Iter {
	return (*gocql.Query)(q).Iter()
}

func (q *query) MapScan(m map[string]interface{}) error {
	return (*gocql.Query)(q).MapScan(m)
}

func (q *query) Scan(dest ...interface{}) error {
	return (*gocql.Query)(q).Scan(dest...)
}

func (q *query) ScanCAS(dest ...interface{}) (applied bool, err error) {
	return (*gocql.Query)(q).ScanCAS(dest...)
}

func (q *query) MapScanCAS(dest map[string]interface{}) (applied bool, err error) {
	return (*gocql.Query)(q).MapScanCAS(dest)
}

func (q *query) Release() {
	(*gocql.Query)(q).Release()
}
