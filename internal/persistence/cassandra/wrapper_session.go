package cassandra

import (
	"context"

	"github.com/gocql/gocql"
)

type session gocql.Session

func (s *session) AwaitSchemaAgreement(ctx context.Context) error {
	return (*gocql.Session)(s).AwaitSchemaAgreement(ctx)
}

func (s *session) SetConsistency(cons gocql.Consistency) {
	(*gocql.Session)(s).SetConsistency(cons)
}

func (s *session) SetPageSize(n int) {
	(*gocql.Session)(s).SetPageSize(n)
}

func (s *session) SetPrefetch(p float64) {
	(*gocql.Session)(s).SetPrefetch(p)
}

func (s *session) SetTrace(trace gocql.Tracer) {
	(*gocql.Session)(s).SetTrace(trace)
}

func (s *session) Query(stmt string, values ...interface{}) Query {
	return (*query)((*gocql.Session)(s).Query(stmt, values...))
}

func (s *session) QueryContext(ctx context.Context, stmt string, values ...interface{}) Query {
	return (*query)((*gocql.Session)(s).Query(stmt, values...).WithContext(ctx))
}

func (s *session) Bind(stmt string, b func(q *gocql.QueryInfo) ([]interface{}, error)) Query {
	return (*query)((*gocql.Session)(s).Bind(stmt, b))
}

func (s *session) BindContext(
	ctx context.Context,
	stmt string,
	b func(q *gocql.QueryInfo) ([]interface{}, error),
) Query {
	return (*query)((*gocql.Session)(s).Bind(stmt, b).WithContext(ctx))
}

func (s *session) Close() {
	(*gocql.Session)(s).Close()
}

func (s *session) Closed() bool {
	return (*gocql.Session)(s).Closed()
}

func (s *session) KeyspaceMetadata(keyspace string) (*gocql.KeyspaceMetadata, error) {
	return (*gocql.Session)(s).KeyspaceMetadata(keyspace)
}

func (s *session) ExecuteBatch(b Batch) error {
	bo, _ := b.(*batch)

	return (*gocql.Session)(s).ExecuteBatch((*gocql.Batch)(bo))
}

func (s *session) ExecuteBatchCAS(b Batch, dest ...interface{}) (applied bool, iter Iter, err error) {
	bo, _ := b.(*batch)

	return (*gocql.Session)(s).ExecuteBatchCAS((*gocql.Batch)(bo), dest...)
}

func (s *session) MapExecuteBatchCAS(b Batch, dest map[string]interface{}) (applied bool, iter Iter, err error) {
	bo, _ := b.(*batch)

	return (*gocql.Session)(s).MapExecuteBatchCAS((*gocql.Batch)(bo), dest)
}

func (s *session) NewBatch(typ gocql.BatchType) Batch {
	return (*batch)((*gocql.Session)(s).NewBatch(typ))
}

func (s *session) NewBatchContext(ctx context.Context, typ gocql.BatchType) Batch {
	return (*batch)((*gocql.Session)(s).NewBatch(typ).WithContext(ctx))
}
