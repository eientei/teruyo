// Package cassandra provides persistent utils for cassandra repositories
package cassandra

import (
	"bytes"
	"compress/gzip"
	"context"
	"crypto/ed25519"
	"encoding/base64"
	"encoding/binary"
	"errors"
	"io"
	"reflect"
	"sync"

	"github.com/gocql/gocql"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/cassandra"
	"github.com/golang-migrate/migrate/v4/source"
	"github.com/rs/zerolog/log"
	"github.com/scylladb/gocqlx/v2/table"
	"gitlab.eientei.org/eientei/libs/otelgocql"
	"gitlab.eientei.org/eientei/teruyo/internal/errcode"
	"go.opentelemetry.io/otel/trace"
)

// ErrInvalidState state signature invalid
var ErrInvalidState = errcode.CodeInvalidData.New("invalid cursor state")

// PagingStateFormat returns signed query paging state
func PagingStateFormat(
	q Query,
	bucketID uint64,
	state []byte,
	priv ed25519.PrivateKey,
) (string, error) {
	ids := make([]byte, 8)

	binary.BigEndian.PutUint64(ids, bucketID)

	state = append(ids, state...)
	data := append([]byte(q.Statement()), state...)

	sig := ed25519.Sign(priv, data)

	var buf bytes.Buffer

	w := gzip.NewWriter(&buf)

	_, err := w.Write(sig)
	if err != nil {
		return "", err
	}

	_, err = w.Write(state)
	if err != nil {
		return "", err
	}

	err = w.Flush()
	if err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString(buf.Bytes()), nil
}

// PagingStateParse parses signed query paging state
func PagingStateParse(
	q Query,
	sigstr string,
	pub ed25519.PublicKey,
) (bucketID uint64, state []byte, err error) {
	bs, err := base64.StdEncoding.DecodeString(sigstr)
	if err != nil {
		return 0, nil, err
	}

	r, err := gzip.NewReader(bytes.NewReader(bs))
	if err != nil {
		return 0, nil, err
	}

	sigdata, _ := io.ReadAll(r)

	if len(sigdata) < ed25519.SignatureSize+8 {
		return 0, nil, io.ErrUnexpectedEOF
	}

	sig, state := sigdata[:ed25519.SignatureSize], sigdata[ed25519.SignatureSize:]
	data := append([]byte(q.Statement()), state...)

	if !ed25519.Verify(pub, data, sig) {
		return 0, nil, ErrInvalidState
	}

	bucketID = binary.BigEndian.Uint64(state[:8])
	state = state[8:]

	return bucketID, state, nil
}

// DefaultReplication for a keyspace
const DefaultReplication = `{'class': 'NetworkTopologyStrategy', 'replication_factor': 1}`

// PrepareKeyspace ensures keyspace exists with given replication
func PrepareKeyspace(ctx context.Context, cfg *gocql.ClusterConfig, keyspace, replication string) error {
	db := *cfg
	db.Keyspace = "system"

	sess, err := db.CreateSession()
	if err != nil {
		return err
	}

	defer sess.Close()

	if replication == "" {
		replication = DefaultReplication
	}

	q := sess.Query(`create keyspace if not exists ` + keyspace + ` with replication = ` + replication).WithContext(ctx)

	defer q.Release()

	err = q.Exec()
	if err != nil {
		return err
	}

	err = sess.AwaitSchemaAgreement(ctx)
	if err != nil {
		return err
	}

	return nil
}

// DropKeyspace drops the entire keyspace
func DropKeyspace(ctx context.Context, cfg *gocql.ClusterConfig, keyspace string) error {
	db := *cfg
	db.Keyspace = "system"

	sess, err := db.CreateSession()
	if err != nil {
		return err
	}

	defer sess.Close()

	q := sess.Query("drop keyspace if exists " + keyspace).WithContext(ctx)

	defer q.Release()

	err = q.Exec()
	if err != nil {
		return err
	}

	err = sess.AwaitSchemaAgreement(ctx)
	if err != nil {
		return err
	}

	return nil
}

var migrationMutex sync.Mutex

// Migrate performs cassandra migration
func Migrate(
	ctx context.Context,
	cfg *gocql.ClusterConfig,
	facility, keyspace, replication string,
	source source.Driver,
	tracerProvider trace.TracerProvider,
) (sess Session, err error) {
	migrationMutex.Lock()
	defer migrationMutex.Unlock()

	err = PrepareKeyspace(ctx, cfg, keyspace, replication)
	if err != nil {
		return nil, err
	}

	nsess, err := otelgocql.NewSessionWithTracing(ctx, cfg, otelgocql.WithTracerProvider(tracerProvider))
	if err != nil {
		return nil, err
	}

	defer func() {
		if err != nil {
			nsess.Close()
		}
	}()

	driver, err := cassandra.WithInstance(nsess, &cassandra.Config{
		MigrationsTable:       "schema_migrations_" + facility,
		KeyspaceName:          keyspace,
		MultiStatementEnabled: true,
		MultiStatementMaxSize: 0,
	})
	if err != nil {
		return nil, err
	}

	m, err := migrate.NewWithInstance("httpfs", source, "cassandra", driver)
	if err != nil {
		return nil, err
	}

	err = m.Up()

	if err == nil {
		err = nsess.AwaitSchemaAgreement(ctx)
	}

	switch {
	case err == nil:
		log.Ctx(ctx).Debug().Msg("Migrated.")
	case errors.Is(err, migrate.ErrNoChange):
		log.Ctx(ctx).Debug().Msg("Migration not required.")
	default:
		return nil, err
	}

	return (*session)(nsess), nil
}

// ReflectTable provides reflective introspection into cql table struct
func ReflectTable(name string, v interface{}) *table.Table {
	t := reflect.TypeOf(reflect.Indirect(reflect.ValueOf(v)))
	if t.Kind() != reflect.Struct {
		return nil
	}

	md := table.Metadata{
		Name:    name,
		Columns: nil,
		PartKey: nil,
		SortKey: nil,
	}

	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		if !f.IsExported() || f.Anonymous {
			continue
		}

		fname := f.Tag.Get("db")
		role := f.Tag.Get("cqlr")

		md.Columns = append(md.Columns, fname)

		switch role {
		case "part":
			md.PartKey = append(md.PartKey, fname)
		case "sort":
			md.SortKey = append(md.SortKey, fname)
		}
	}

	return table.New(md)
}
