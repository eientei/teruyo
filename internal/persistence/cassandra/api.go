//go:generate mockgen -package=mock -destination=mock/wrapper.go . Session,Query,Iter,Batch
//go:generate mockgen -package=mock -destination=mock/gocql.go github.com/gocql/gocql Scanner

// Package cassandra provides utils for cassandra DB
package cassandra

import (
	"context"

	"github.com/gocql/gocql"
)

// Session mockable interface
type Session interface {
	AwaitSchemaAgreement(ctx context.Context) error
	SetConsistency(cons gocql.Consistency)
	SetPageSize(n int)
	SetPrefetch(p float64)
	SetTrace(trace gocql.Tracer)
	Query(stmt string, values ...interface{}) Query
	QueryContext(ctx context.Context, stmt string, values ...interface{}) Query
	Bind(stmt string, b func(q *gocql.QueryInfo) ([]interface{}, error)) Query
	BindContext(ctx context.Context, stmt string, b func(q *gocql.QueryInfo) ([]interface{}, error)) Query
	Close()
	Closed() bool
	KeyspaceMetadata(keyspace string) (*gocql.KeyspaceMetadata, error)
	ExecuteBatch(batch Batch) error
	ExecuteBatchCAS(batch Batch, dest ...interface{}) (applied bool, iter Iter, err error)
	MapExecuteBatchCAS(batch Batch, dest map[string]interface{}) (applied bool, iter Iter, err error)
	NewBatch(typ gocql.BatchType) Batch
	NewBatchContext(ctx context.Context, typ gocql.BatchType) Batch
}

// Query mockable interface
type Query interface {
	Statement() string
	Values() []interface{}
	String() string
	Attempts() int
	AddAttempts(i int, host *gocql.HostInfo)
	Latency() int64
	AddLatency(l int64, host *gocql.HostInfo)
	Consistency(c gocql.Consistency) Query
	GetConsistency() gocql.Consistency
	SetConsistency(c gocql.Consistency)
	CustomPayload(customPayload map[string][]byte) Query
	Context() context.Context
	Trace(trace gocql.Tracer) Query
	Observer(observer gocql.QueryObserver) Query
	PageSize(n int) Query
	DefaultTimestamp(enable bool) Query
	WithTimestamp(timestamp int64) Query
	RoutingKey(routingKey []byte) Query
	WithContext(ctx context.Context) Query
	Cancel()
	Keyspace() string
	Table() string
	GetRoutingKey() ([]byte, error)
	Prefetch(p float64) Query
	RetryPolicy(r gocql.RetryPolicy) Query
	SetSpeculativeExecutionPolicy(sp gocql.SpeculativeExecutionPolicy) Query
	IsIdempotent() bool
	Idempotent(value bool) Query
	Bind(v ...interface{}) Query
	SerialConsistency(cons gocql.SerialConsistency) Query
	PageState(state []byte) Query
	NoSkipMetadata() Query
	Exec() error
	Iter() Iter
	MapScan(m map[string]interface{}) error
	Scan(dest ...interface{}) error
	ScanCAS(dest ...interface{}) (applied bool, err error)
	MapScanCAS(dest map[string]interface{}) (applied bool, err error)
	Release()
}

// Iter mockable interface
type Iter interface {
	Host() *gocql.HostInfo
	Columns() []gocql.ColumnInfo

	Scanner() gocql.Scanner
	Scan(dest ...interface{}) bool
	Warnings() []string
	Close() error
	WillSwitchPage() bool
	PageState() []byte
	NumRows() int
}

// Batch mockable interface
type Batch interface {
	Trace(trace gocql.Tracer) Batch
	Observer(observer gocql.BatchObserver) Batch
	Keyspace() string
	Table() string
	Attempts() int
	AddAttempts(i int, host *gocql.HostInfo)
	Latency() int64
	AddLatency(l int64, host *gocql.HostInfo)
	GetConsistency() gocql.Consistency
	SetConsistency(c gocql.Consistency)
	Context() context.Context
	IsIdempotent() bool
	SpeculativeExecutionPolicy(sp gocql.SpeculativeExecutionPolicy) Batch
	Query(stmt string, args ...interface{})
	Bind(stmt string, bind func(q *gocql.QueryInfo) ([]interface{}, error))
	RetryPolicy(r gocql.RetryPolicy) Batch
	WithContext(ctx context.Context) Batch
	Cancel()
	Size() int
	SerialConsistency(cons gocql.SerialConsistency) Batch
	DefaultTimestamp(enable bool) Batch
	WithTimestamp(timestamp int64) Batch
	GetRoutingKey() ([]byte, error)
	BatchEntries() []gocql.BatchEntry
}
