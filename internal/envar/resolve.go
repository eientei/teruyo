package envar

import (
	"fmt"
	"os"
	"sort"
	"strconv"
	"time"
)

// EnvDump registers all environment variables used by application
var EnvDump = map[string]interface{}{}

// PrintEnv prints currently registered environment variables in alphanumerical sorting
func PrintEnv() {
	var keys []string

	for k := range EnvDump {
		keys = append(keys, k)
	}

	sort.Strings(keys)

	for _, k := range keys {
		fmt.Printf("%-30s: %v\n", k, EnvDump[k])
	}
}

// ResolveString resolves string from environment variable or fallbacks to default
func ResolveString(name, def string) (res string) {
	defer func() {
		EnvDump[name] = res
	}()

	if v, ok := os.LookupEnv(name); ok {
		return v
	}

	return def
}

// ResolveInt resolves int from environment variable or fallbacks to default
func ResolveInt(name string, def int) (res int) {
	defer func() {
		EnvDump[name] = res
	}()

	if v, ok := os.LookupEnv(name); ok {
		if u, err := strconv.ParseInt(v, 10, 64); err == nil {
			return int(u)
		}
	}

	return def
}

// ResolveUint resolves uint from environment variable or fallbacks to default
func ResolveUint(name string, def uint) (res uint) {
	defer func() {
		EnvDump[name] = res
	}()

	if v, ok := os.LookupEnv(name); ok {
		if u, err := strconv.ParseUint(v, 10, 64); err == nil {
			return uint(u)
		}
	}

	return def
}

// ResolveInt8 resolves int8 from environment variable or fallbacks to default
func ResolveInt8(name string, def int8) (res int8) {
	defer func() {
		EnvDump[name] = res
	}()

	if v, ok := os.LookupEnv(name); ok {
		if u, err := strconv.ParseInt(v, 10, 8); err == nil {
			return int8(u)
		}
	}

	return def
}

// ResolveUint8 resolves uint8 from environment variable or fallbacks to default
func ResolveUint8(name string, def uint8) (res uint8) {
	defer func() {
		EnvDump[name] = res
	}()

	if v, ok := os.LookupEnv(name); ok {
		if u, err := strconv.ParseUint(v, 10, 8); err == nil {
			return uint8(u)
		}
	}

	return def
}

// ResolveInt16 resolves int16 from environment variable or fallbacks to default
func ResolveInt16(name string, def int16) (res int16) {
	defer func() {
		EnvDump[name] = res
	}()

	if v, ok := os.LookupEnv(name); ok {
		if u, err := strconv.ParseInt(v, 10, 16); err == nil {
			return int16(u)
		}
	}

	return def
}

// ResolveUint16 resolves uint16 from environment variable or fallbacks to default
func ResolveUint16(name string, def uint16) (res uint16) {
	defer func() {
		EnvDump[name] = res
	}()

	if v, ok := os.LookupEnv(name); ok {
		if u, err := strconv.ParseUint(v, 10, 16); err == nil {
			return uint16(u)
		}
	}

	return def
}

// ResolveInt32 resolves int32 from environment variable or fallbacks to default
func ResolveInt32(name string, def int32) (res int32) {
	defer func() {
		EnvDump[name] = res
	}()

	if v, ok := os.LookupEnv(name); ok {
		if u, err := strconv.ParseInt(v, 10, 32); err == nil {
			return int32(u)
		}
	}

	return def
}

// ResolveUint32 resolves uint32 from environment variable or fallbacks to default
func ResolveUint32(name string, def uint32) (res uint32) {
	defer func() {
		EnvDump[name] = res
	}()

	if v, ok := os.LookupEnv(name); ok {
		if u, err := strconv.ParseUint(v, 10, 32); err == nil {
			return uint32(u)
		}
	}

	return def
}

// ResolveInt64 resolves int64 from environment variable or fallbacks to default
func ResolveInt64(name string, def int64) (res int64) {
	defer func() {
		EnvDump[name] = res
	}()

	if v, ok := os.LookupEnv(name); ok {
		if u, err := strconv.ParseInt(v, 10, 64); err == nil {
			return u
		}
	}

	return def
}

// ResolveUint64 resolves uint64 from environment variable or fallbacks to default
func ResolveUint64(name string, def uint64) (res uint64) {
	defer func() {
		EnvDump[name] = res
	}()

	if v, ok := os.LookupEnv(name); ok {
		if u, err := strconv.ParseUint(v, 10, 64); err == nil {
			return u
		}
	}

	return def
}

// ResolveFloat64 resolves  loat64 from environment variable or fallbacks to default
func ResolveFloat64(name string, def float64) (res float64) {
	defer func() {
		EnvDump[name] = res
	}()

	if v, ok := os.LookupEnv(name); ok {
		if u, err := strconv.ParseFloat(v, 64); err == nil {
			return u
		}
	}

	return def
}

// ResolveDuration resolves duration from environment variable or fallbacks to default
func ResolveDuration(name string, def time.Duration) (res time.Duration) {
	defer func() {
		EnvDump[name] = res
	}()

	if v, ok := os.LookupEnv(name); ok {
		if dur, err := time.ParseDuration(v); err == nil {
			return dur
		}
	}

	return def
}

// ResolveBoolean resolves boolean from environment variable or fallbacks to default
func ResolveBoolean(name string, def bool) (res bool) {
	defer func() {
		EnvDump[name] = res
	}()

	if v, ok := os.LookupEnv(name); ok {
		if dur, err := strconv.ParseBool(v); err == nil {
			return dur
		}
	}

	return def
}
