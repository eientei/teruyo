// Package hclog provides wrapper for zerolog logger into hclog interface
package hclog

import (
	"fmt"
	"io"
	"log"

	"github.com/hashicorp/go-hclog"
	"github.com/rs/zerolog"
)

// New returns new zerolog wrapper
func New(logger zerolog.Logger) hclog.Logger {
	return &wrapper{
		logger: logger,
	}
}

type wrapper struct {
	logger zerolog.Logger
	name   string
	fields []interface{}
}

func (w *wrapper) GetLevel() hclog.Level {
	return levelZerologToHCLog(w.logger.GetLevel())
}

func (w *wrapper) Log(level hclog.Level, msg string, args ...interface{}) {
	w.logger.WithLevel(levelHCLogToZerolog(level)).Fields(formatArgs(args)).Msg(msg)
}

func (w *wrapper) Trace(msg string, args ...interface{}) {
	w.logger.Debug().Fields(formatArgs(args)).Msg(msg)
}

func (w *wrapper) Debug(msg string, args ...interface{}) {
	w.logger.Debug().Fields(formatArgs(args)).Msg(msg)
}

func (w *wrapper) Info(msg string, args ...interface{}) {
	w.logger.Info().Fields(formatArgs(args)).Msg(msg)
}

func (w *wrapper) Warn(msg string, args ...interface{}) {
	w.logger.Warn().Fields(formatArgs(args)).Msg(msg)
}

func (w *wrapper) Error(msg string, args ...interface{}) {
	w.logger.Error().Fields(formatArgs(args)).Msg(msg)
}

func (w *wrapper) IsTrace() bool {
	return w.logger.GetLevel() <= zerolog.DebugLevel
}

func (w *wrapper) IsDebug() bool {
	return w.logger.GetLevel() <= zerolog.DebugLevel
}

func (w *wrapper) IsInfo() bool {
	return w.logger.GetLevel() <= zerolog.InfoLevel
}

func (w *wrapper) IsWarn() bool {
	return w.logger.GetLevel() <= zerolog.WarnLevel
}

func (w *wrapper) IsError() bool {
	return w.logger.GetLevel() <= zerolog.ErrorLevel
}

func (w *wrapper) ImpliedArgs() []interface{} {
	return w.fields
}

func (w *wrapper) With(args ...interface{}) hclog.Logger {
	return &wrapper{
		logger: w.logger.With().Fields(formatArgs(args)).Logger(),
		fields: args,
		name:   w.name,
	}
}

func (w *wrapper) Name() string {
	return w.name
}

func (w *wrapper) Named(name string) hclog.Logger {
	return &wrapper{
		logger: w.logger.With().Str("name", name).Logger(),
		fields: w.fields,
		name:   name,
	}
}

func (w *wrapper) ResetNamed(_ string) hclog.Logger {
	return w
}

func (w *wrapper) SetLevel(level hclog.Level) {
	w.logger.Level(levelHCLogToZerolog(level))
}

func (w *wrapper) StandardLogger(opts *hclog.StandardLoggerOptions) *log.Logger {
	return hclog.Default().StandardLogger(opts)
}

func (w *wrapper) StandardWriter(opts *hclog.StandardLoggerOptions) io.Writer {
	return hclog.Default().StandardWriter(opts)
}

func formatArgs(args []interface{}) (res map[string]interface{}) {
	res = make(map[string]interface{})

	for i := 0; i < len(res); i += 2 {
		arg := args[i+1]

		f, ok := arg.(hclog.Format)
		if ok {
			arg = fmt.Sprintf(f[0].(string), f[1:]...)
		}

		res[fmt.Sprint(args[i])] = arg
	}

	return
}

func levelHCLogToZerolog(level hclog.Level) zerolog.Level {
	switch level {
	case hclog.NoLevel:
		return zerolog.NoLevel
	case hclog.Trace:
		return zerolog.DebugLevel
	case hclog.Debug:
		return zerolog.DebugLevel
	case hclog.Info:
		return zerolog.InfoLevel
	case hclog.Warn:
		return zerolog.WarnLevel
	case hclog.Error:
		return zerolog.ErrorLevel
	case hclog.Off:
		return zerolog.Disabled
	}

	return zerolog.NoLevel
}

func levelZerologToHCLog(level zerolog.Level) hclog.Level {
	switch level {
	case zerolog.NoLevel:
		return hclog.NoLevel
	case zerolog.DebugLevel:
		return hclog.Debug
	case zerolog.InfoLevel:
		return hclog.Info
	case zerolog.WarnLevel:
		return hclog.Warn
	case zerolog.ErrorLevel:
		return hclog.Error
	case zerolog.Disabled:
		return hclog.Off
	}

	return hclog.NoLevel
}
