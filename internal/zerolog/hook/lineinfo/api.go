// Package lineinfo provides zerolog hook adding line number/enclosing function/file name to logs
package lineinfo

import (
	"fmt"
	"path"
	"regexp"
	"runtime"

	"github.com/rs/zerolog"
)

// Hook zerolog hook instance
type Hook struct {
	SkipPatterns []*regexp.Regexp
	Depth        int
}

// Run implementation
func (h *Hook) Run(e *zerolog.Event, _ zerolog.Level, _ string) {
	rpc := make([]uintptr, h.Depth)
	runtime.Callers(h.Depth, rpc)

	frames := runtime.CallersFrames(rpc)
outer:
	for frame, m := frames.Next(); m; frame, m = frames.Next() {
		for _, p := range h.SkipPatterns {
			if p.MatchString(frame.Function) {
				continue outer
			}
		}

		e.Str("line", fmt.Sprintf("%s:%d", path.Base(frame.File), frame.Line))
		e.Str("func", path.Base(frame.Function))

		break
	}
}
