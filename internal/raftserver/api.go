// Package raftserver provides helper for instantiation a grpc-replicating raft node
package raftserver

import (
	"os"
	"path/filepath"

	"github.com/Jille/raft-grpc-leader-rpc/leaderhealth"
	transport "github.com/Jille/raft-grpc-transport"
	"github.com/Jille/raftadmin"
	"github.com/hashicorp/raft"
	boltdb "github.com/hashicorp/raft-boltdb/v2"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"go.opentelemetry.io/otel/trace"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/reflection"
)

// New returns new initialized raft instance
// first entry of servers list is assumed to be a bootstrap node
func New(
	config *raft.Config,
	basedir string,
	server raft.Server,
	snapshots int,
	fsm raft.FSM,
	grpcServer *grpc.Server,
	servers []raft.Server,
	traceProvider trace.TracerProvider,
) (r *raft.Raft, err error) {
	var (
		ldb raft.LogStore
		sdb raft.StableStore
		fss raft.SnapshotStore
	)

	if basedir == "" {
		ldb, sdb, fss = raft.NewInmemStore(), raft.NewInmemStore(), raft.NewInmemSnapshotStore()
	} else {
		path := filepath.Join(basedir, string(server.ID))

		err = os.MkdirAll(path, 0755)
		if err != nil {
			return
		}

		ldb, err = boltdb.NewBoltStore(filepath.Join(path, "logs.db"))
		if err != nil {
			return
		}

		sdb, err = boltdb.NewBoltStore(filepath.Join(path, "stable.db"))
		if err != nil {
			return
		}

		fss, err = raft.NewFileSnapshotStore(path, snapshots, os.Stderr)
		if err != nil {
			return
		}
	}

	if err != nil {
		return
	}

	tm := transport.New(server.Address, []grpc.DialOption{
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithStatsHandler(
			otelgrpc.NewClientHandler(
				otelgrpc.WithTracerProvider(traceProvider),
			),
		),
	})

	r, err = raft.NewRaft(config, fsm, ldb, sdb, fss, tm.Transport())
	if err != nil {
		return
	}

	leaderhealth.Setup(r, grpcServer, nil)
	raftadmin.Register(grpcServer, r)
	reflection.Register(grpcServer)
	tm.Register(grpcServer)

	if server.ID == servers[0].ID {
		go func() {
			err = bootstrap(r, basedir, servers)
			if err != nil {
				panic(err)
			}
		}()
	}

	return
}

func bootstrap(r *raft.Raft, basedir string, servers []raft.Server) error {
	var bootf string

	if basedir != "" {
		bootf = filepath.Join(basedir, "bootstrap")

		_, err := os.Stat(bootf)
		if err == nil {
			return nil
		}

		if !os.IsNotExist(err) {
			return err
		}
	}

	err := r.BootstrapCluster(raft.Configuration{
		Servers: servers,
	}).Error()
	if err != nil {
		return err
	}

	if bootf != "" {
		var f *os.File

		f, err = os.Create(bootf)
		if err != nil {
			return err
		}

		err = f.Close()
		if err != nil {
			return err
		}
	}

	return nil
}
