// Package byteops provides common byte operations
package byteops

import "io"

// UInt128Marshal render 128-bit integer in big-endian representation
func UInt128Marshal(hi, lo uint64) []byte {
	return []byte{
		byte(hi>>56) & 0xFF,
		byte(hi>>48) & 0xFF,
		byte(hi>>40) & 0xFF,
		byte(hi>>32) & 0xFF,
		byte(hi>>24) & 0xFF,
		byte(hi>>16) & 0xFF,
		byte(hi>>8) & 0xFF,
		byte(hi) & 0xFF,
		byte(lo>>56) & 0xFF,
		byte(lo>>48) & 0xFF,
		byte(lo>>40) & 0xFF,
		byte(lo>>32) & 0xFF,
		byte(lo>>24) & 0xFF,
		byte(lo>>16) & 0xFF,
		byte(lo>>8) & 0xFF,
		byte(lo) & 0xFF,
	}
}

// UInt128Unmarshal parse big-endian representation of 128-bit integer
func UInt128Unmarshal(bs []byte) (hi, lo uint64, err error) {
	if len(bs) < 16 {
		return 0, 0, io.EOF
	}

	return uint64(bs[0])<<56 | uint64(bs[1])<<48 | uint64(bs[2])<<40 | uint64(bs[3])<<32 |
			uint64(bs[4])<<24 | uint64(bs[5])<<16 | uint64(bs[6])<<8 | uint64(bs[7]),
		uint64(bs[8])<<56 | uint64(bs[9])<<48 | uint64(bs[10])<<40 | uint64(bs[11])<<32 |
			uint64(bs[12])<<24 | uint64(bs[13])<<16 | uint64(bs[14])<<8 | uint64(bs[15]),
		nil
}
