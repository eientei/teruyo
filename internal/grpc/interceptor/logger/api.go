// Package logger provides logging for grpc calls
package logger

import (
	"context"
	"strings"

	"github.com/rs/zerolog"
	"google.golang.org/grpc"
)

type serverStreamLog struct {
	grpc.ServerStream
	info *grpc.StreamServerInfo
	log  zerolog.Logger
}

func (sl *serverStreamLog) SendMsg(m interface{}) error {
	err := sl.ServerStream.SendMsg(m)

	sl.log.Debug().Str("grpc_method", sl.info.FullMethod).Interface("grpc_stream_response", m).Err(err).Send()

	return err
}

func (sl *serverStreamLog) RecvMsg(m interface{}) error {
	err := sl.ServerStream.RecvMsg(m)

	sl.log.Debug().Str("grpc_method", sl.info.FullMethod).Interface("grpc_stream_request", m).Err(err).Send()

	return err
}

type clientStreamLog struct {
	grpc.ClientStream
	log    zerolog.Logger
	method string
}

func (sl *clientStreamLog) SendMsg(m interface{}) error {
	err := sl.ClientStream.SendMsg(m)

	sl.log.Debug().Str("grpc_method", sl.method).Interface("grpc_stream_request", m).Err(err).Send()

	return err
}

func (sl *clientStreamLog) RecvMsg(m interface{}) error {
	err := sl.ClientStream.RecvMsg(m)

	sl.log.Debug().Str("grpc_method", sl.method).Interface("grpc_stream_response", m).Err(err).Send()

	return err
}

func (sl *clientStreamLog) CloseSend() error {
	err := sl.ClientStream.CloseSend()

	sl.log.Debug().Str("grpc_method", sl.method).Err(err).Msg("close send")

	return err
}

// ServerUnaryInterceptor implementation
func ServerUnaryInterceptor(log zerolog.Logger, skip ...string) grpc.UnaryServerInterceptor {
	return func(
		ctx context.Context,
		req interface{},
		info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler,
	) (resp interface{}, err error) {
		for _, s := range skip {
			if strings.HasPrefix(info.FullMethod, s) {
				return handler(ctx, req)
			}
		}

		log.Debug().Str("grpc_method", info.FullMethod).Interface("grpc_request", req).Send()

		resp, err = handler(ctx, req)

		log.Debug().Str("grpc_method", info.FullMethod).Interface("grpc_response", resp).Err(err).Send()

		return
	}
}

// ServerStreamInterceptor implementation
func ServerStreamInterceptor(log zerolog.Logger, skip ...string) grpc.StreamServerInterceptor {
	return func(
		srv interface{},
		ss grpc.ServerStream,
		info *grpc.StreamServerInfo,
		handler grpc.StreamHandler,
	) (err error) {
		for _, s := range skip {
			if strings.HasPrefix(info.FullMethod, s) {
				return handler(srv, ss)
			}
		}

		log.Debug().Str("grpc_method", info.FullMethod).Msg("stream started")

		wrap := &serverStreamLog{
			ServerStream: ss,
			info:         info,
			log:          log,
		}

		err = handler(srv, wrap)

		log.Debug().Str("grpc_method", info.FullMethod).Err(err).Msg("stream ended")

		return
	}
}

// ClientUnaryInterceptor implementation
func ClientUnaryInterceptor(log zerolog.Logger, skip ...string) grpc.UnaryClientInterceptor {
	return func(
		ctx context.Context,
		method string,
		req, reply interface{},
		cc *grpc.ClientConn,
		invoker grpc.UnaryInvoker,
		opts ...grpc.CallOption,
	) (err error) {
		for _, s := range skip {
			if strings.HasPrefix(method, s) {
				return invoker(ctx, method, req, reply, cc, opts...)
			}
		}

		log.Debug().Str("grpc_method", method).Interface("grpc_request", req).Send()

		err = invoker(ctx, method, req, reply, cc, opts...)

		log.Debug().Str("grpc_method", method).Interface("grpc_response", reply).Err(err).Send()

		return
	}
}

// ClientStreamInterceptor implementation
func ClientStreamInterceptor(log zerolog.Logger, skip ...string) grpc.StreamClientInterceptor {
	return func(
		ctx context.Context,
		desc *grpc.StreamDesc,
		cc *grpc.ClientConn,
		method string,
		streamer grpc.Streamer,
		opts ...grpc.CallOption,
	) (grpc.ClientStream, error) {
		for _, s := range skip {
			if strings.HasPrefix(method, s) {
				return streamer(ctx, desc, cc, method, opts...)
			}
		}

		s, err := streamer(ctx, desc, cc, method, opts...)

		log.Debug().Str("grpc_method", method).Err(err).Msg("stream started")

		if err != nil {
			return nil, err
		}

		wrap := &clientStreamLog{
			ClientStream: s,
			method:       method,
			log:          log,
		}

		return wrap, nil
	}
}
