// Package error provides grpc interceptor for serializing and deserializing extended error codes
package error

import (
	"context"
	"encoding/base64"

	"gitlab.eientei.org/eientei/teruyo/internal/errcode"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

func errorServerTrailer(ctx context.Context, service string, err error, translators []errcode.Translator) error {
	if err == nil {
		return nil
	}

	errc := errcode.Translate(err, translators...)

	errc.Service = service

	md := metadata.New(map[string]string{
		"app.error.message": base64.StdEncoding.EncodeToString(([]byte)(errc.Error())),
		"app.error.code":    string(errc.Code),
		"app.error.service": errc.Service,
	})

	_ = grpc.SetTrailer(ctx, md)

	return errc
}

func errorClientTrailer(md metadata.MD, err error) error {
	if err == nil {
		return nil
	}

	codeerr := errcode.CodeUnknown.From(err)

	if md != nil {
		message := md.Get("app.error.message")
		code := md.Get("app.error.code")
		service := md.Get("app.error.service")

		if len(message) > 0 {
			if bs, errdec := base64.StdEncoding.DecodeString(message[0]); errdec == nil {
				codeerr.Message = string(bs)
			}
		}

		if len(code) > 0 {
			codeerr.Code = errcode.Code(code[0])
		}

		if len(service) > 0 {
			codeerr.Service = service[0]
		}
	}

	return codeerr
}

// ServerUnary implementation
func ServerUnary(service string, translators ...errcode.Translator) grpc.UnaryServerInterceptor {
	return func(
		ctx context.Context,
		req interface{},
		info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler,
	) (interface{}, error) {
		resp, err := handler(ctx, req)

		err = errorServerTrailer(ctx, service, err, translators)

		return resp, err
	}
}

// ServerStream implementation
func ServerStream(service string, translators ...errcode.Translator) grpc.StreamServerInterceptor {
	return func(
		srv interface{},
		ss grpc.ServerStream,
		info *grpc.StreamServerInfo,
		handler grpc.StreamHandler,
	) error {
		err := handler(srv, ss)

		err = errorServerTrailer(ss.Context(), service, err, translators)

		return err
	}
}

// ClientUnaryInterceptor implementation
func ClientUnaryInterceptor() grpc.UnaryClientInterceptor {
	return func(
		ctx context.Context,
		method string,
		req, reply interface{},
		cc *grpc.ClientConn,
		invoker grpc.UnaryInvoker,
		opts ...grpc.CallOption,
	) error {
		md := make(metadata.MD)

		opts = append(opts, grpc.Trailer(&md))

		err := invoker(ctx, method, req, reply, cc, opts...)

		return errorClientTrailer(md, err)
	}
}

type errorClientStreamWrapper struct {
	grpc.ClientStream
}

func (w *errorClientStreamWrapper) CloseSend() error {
	err := w.ClientStream.CloseSend()

	md := w.ClientStream.Trailer()

	return errorClientTrailer(md, err)
}

func (w *errorClientStreamWrapper) RecvMsg(m interface{}) error {
	err := w.ClientStream.RecvMsg(m)

	if err != nil {
		md := w.ClientStream.Trailer()

		return errorClientTrailer(md, err)
	}

	return errorClientTrailer(nil, err)
}

// ClientStreamInterceptor implementation
func ClientStreamInterceptor() grpc.StreamClientInterceptor {
	return func(
		ctx context.Context,
		desc *grpc.StreamDesc,
		cc *grpc.ClientConn,
		method string,
		streamer grpc.Streamer,
		opts ...grpc.CallOption,
	) (grpc.ClientStream, error) {
		cs, err := streamer(ctx, desc, cc, method, opts...)
		if err != nil {
			return nil, err
		}

		return &errorClientStreamWrapper{
			ClientStream: cs,
		}, nil
	}
}
