// Package postgres provides postgres DB error code translator
package postgres

import (
	"database/sql"
	"errors"

	"gitlab.eientei.org/eientei/teruyo/internal/errcode"
)

// ErrorTranslator implementation
func ErrorTranslator(err error) *errcode.Error {
	if errors.Is(err, sql.ErrNoRows) {
		return errcode.CodeNotFound.From(err)
	}

	return nil
}
