// Package cassandra provides cassandra DB error code translator
package cassandra

import (
	"errors"

	"github.com/gocql/gocql"
	"gitlab.eientei.org/eientei/teruyo/internal/errcode"
)

// ErrorTranslator implementation
func ErrorTranslator(err error) *errcode.Error {
	if errors.Is(err, gocql.ErrNotFound) {
		return errcode.CodeNotFound.From(err)
	}

	return nil
}
