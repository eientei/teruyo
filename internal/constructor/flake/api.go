// Package flake creates flake ID generator
package flake

import (
	"gitlab.eientei.org/eientei/teruyo/internal/envar"
	"gitlab.eientei.org/eientei/teruyo/internal/flake"
)

var (
	// EnvMachineID worker ID
	EnvMachineID = envar.ResolveUint16("MACHINE_ID", 1)
)

// New returns new flake generator instance
func New() flake.Generator {
	return flake.New(EnvMachineID)
}
