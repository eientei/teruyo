// Package cassandra creates cassandra repository
package cassandra

import (
	"context"
	"strings"
	"time"

	"github.com/gocql/gocql"
	"gitlab.eientei.org/eientei/teruyo/internal/envar"
	"gitlab.eientei.org/eientei/teruyo/internal/flake"
	"gitlab.eientei.org/eientei/teruyo/internal/model/cassandra"
	"go.opentelemetry.io/otel/trace"
)

// Configuration
var (
	EnvCassandraKeyspace = envar.ResolveString("CASSANDRA_KEYSPACE", "teruyo")
	EnvCassandraHosts    = strings.Split(envar.ResolveString(
		"CASSANDRA_HOSTS",
		"127.0.0.1",
	), ",")
	EnvCassandraReplicationStrategy = envar.ResolveString(
		"CASSANDRA_REPLICATION_STRATEGY",
		"{'class': 'NetworkTopologyStrategy', 'replication_factor': 1}",
	)
	EnvCassandraInitialGenerationHash       = envar.ResolveUint64("CASSANDRA_INITIAL_GENERATION_HASH", 0)
	EnvCassandraInitialGenerationTime       = envar.ResolveDuration("CASSANDRA_INITIAL_GENERATION_TIME", time.Hour*24*30)
	EnvCassandraUpgradeBatchSizeCollections = envar.ResolveInt("CASSANDRA_UPGRADE_BATCH_SIZE_COLLECTIONS", 100)
	EnvCassandraUpgradeBatchSizeItems       = envar.ResolveInt("CASSANDRA_UPGRADE_BATCH_SIZE_ITEMS", 100)
	EnvCassandraUpgradeBatchSizeTimelines   = envar.ResolveInt("CASSANDRA_UPGRADE_BATCH_SIZE_TIMELINES", 100)
)

// CreateCassandraCluster initialize cluster
func CreateCassandraCluster() *gocql.ClusterConfig {
	cassandraCluster := gocql.NewCluster(EnvCassandraHosts...)
	cassandraCluster.Keyspace = EnvCassandraKeyspace

	return cassandraCluster
}

// New repository constructor, executes migrations
func New(
	ctx context.Context,
	flakeGenerator flake.Generator,
	globalTracer trace.TracerProvider,
) (cassandra.Repository, error) {
	repoCassandra, err := cassandra.New(&cassandra.Config{
		Generator:                   flakeGenerator,
		ClusterConfig:               *CreateCassandraCluster(),
		TracerProvider:              globalTracer,
		Replication:                 EnvCassandraReplicationStrategy,
		InitialGenerationHash:       EnvCassandraInitialGenerationHash,
		InitialGenerationTime:       EnvCassandraInitialGenerationTime,
		UpgradeBatchSizeCollections: EnvCassandraUpgradeBatchSizeCollections,
		UpgradeBatchSizeItems:       EnvCassandraUpgradeBatchSizeItems,
		UpgradeBatchSizeTimelines:   EnvCassandraUpgradeBatchSizeTimelines,
	})
	if err != nil {
		return nil, err
	}

	err = repoCassandra.Migrate(ctx)
	if err != nil {
		return nil, err
	}

	return repoCassandra, nil
}
