// Package env for common environment variables
package env

import (
	"os"
	"time"

	"gitlab.eientei.org/eientei/teruyo/internal/envar"
)

var (
	// LogLevel - log verbosity level
	LogLevel = envar.ResolveString("LOG_LEVEL", "INFO")

	// InstanceID - ID of instance
	InstanceID = envar.ResolveString("INSTANCE_ID", "0")

	// GrpcListenerURI - grpc server listener URI
	GrpcListenerURI = envar.ResolveString("GRPC_LISTENER_URI", ":10000")

	// GrpcListenerCertFile - certificate file of grpc server
	GrpcListenerCertFile = envar.ResolveString("GRPC_LISTENER_CERT_FILE", os.Args[0]+"-cert.pem")

	// GrpcListenerKeyFile - key file of grpc server
	GrpcListenerKeyFile = envar.ResolveString("GRPC_LISTENER_KEY_FILE", os.Args[0]+"-key.pem")

	// GrpcClientDialRetries - num of retries to connect to coordinator as client
	GrpcClientDialRetries = envar.ResolveUint("GRPC_CLIENT_DIAL_RETRIES", 10)

	// GrpcClientDialTimeout - timeout for every dial tryout
	GrpcClientDialTimeout = envar.ResolveDuration("GRPC_CLIENT_DIAL_TIMEOUT", time.Second*10)

	// EpochStart - timestamp epoch start
	EpochStart = envar.ResolveString("EPOCH_START", "2019-01-01T00:00:00.000Z")

	// Benchmarking - benchmarking mode
	Benchmarking = envar.ResolveBoolean("BENCHMARKING", false)
)
