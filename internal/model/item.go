package model

import (
	"context"
	"time"

	"github.com/mailru/easyjson/jwriter"

	"gitlab.eientei.org/eientei/teruyo/internal/flake"

	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"
)

// ItemRepository stores generally unordered activities/objects
type ItemRepository interface {
	ItemSave(ctx context.Context, item *Item) error
	ItemGetByID(ctx context.Context, itemID vocab.IRI) (item *Item, err error)
	ItemGetByIDs(ctx context.Context, itemIDs ...vocab.IRI) (items []*Item, err error)
	ItemUpdateReplies(ctx context.Context, itemID vocab.IRI) error
	ItemList(
		ctx context.Context,
		authentication *Actor,
		filters *Filters,
		typ TimelineType,
		actor, reference vocab.IRI,
		limit int,
	) (relations []*Item, err error)
}

// Item of activity pub, generally activity/object
type Item struct {
	Data         vocab.Item
	Created      time.Time
	Updated      time.Time
	Actor        vocab.IRI // source actor
	ReplyToItem  vocab.IRI
	ReplyToActor vocab.IRI
	Item         vocab.IRI // item ID
	Context      vocab.IRI
	List         vocab.IRI
	Recipients   []vocab.IRI // actors
	Tags         []string    // tag names
	Resolved     struct {
		Reactions  []*Reaction
		Local      bool
		Shared     bool
		Liked      bool
		Bookmarked bool
		Muted      bool
	}
	ID         flake.ID
	Replies    int64
	Shares     int64
	Likes      int64
	Reactions  int64
	Visibility vocab.Visibility
	Sensitive  bool
	Pinned     bool // item is pinned
	Media      bool // item contains media
}

// Reaction entry
type Reaction struct {
	Name   string    // either unicode emoji sequence or custom emoji name
	URL    vocab.IRI // custom emoji IRI
	Actors []flake.ID
	Me     bool
}

// MarshalJSON implementation
func (r *Reaction) MarshalJSON() ([]byte, error) {
	var w jwriter.Writer

	w.RawByte('{')

	w.String("name")
	w.RawByte(':')
	w.String(r.Name)

	w.RawByte(',')
	w.String("me")
	w.RawByte(':')
	w.Bool(r.Me)

	w.RawByte(',')
	w.String("count")
	w.RawByte(':')
	w.Int(len(r.Actors))

	w.RawByte(',')
	w.String("account_ids")
	w.RawByte(':')
	w.RawByte('[')

	for _, a := range r.Actors {
		w.String(a.String())
	}

	w.RawByte(']')

	if r.URL != "" {
		w.RawByte(',')
		w.String("url")
		w.RawByte(':')
		w.String(string(r.URL))
	}

	w.RawByte('}')

	return w.BuildBytes()
}
