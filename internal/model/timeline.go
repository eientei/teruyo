package model

import (
	"context"
	"time"

	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"
	"gitlab.eientei.org/eientei/teruyo/internal/flake"
	"gitlab.eientei.org/eientei/teruyo/internal/sorted"
)

// TimelineType timeline type
type TimelineType int

// Known timeline types
const (
	TimelineTypeUnknown      TimelineType = 0
	TimelineTypeInbox        TimelineType = 1 // objects delivered to actor (home timeline)
	TimelineTypeOutbox       TimelineType = 2 // objects sent by actor (viewing actor profile)
	TimelineTypeLiked        TimelineType = 3 // objects liked by actor (favorites timeline)
	TimelineTypeBookmarked   TimelineType = 4 // objects bookmarked by actor (bookmarks timeline)
	TimelineTypeNotification TimelineType = 5 // objects actively mentioning the actor (notifications)
)

// String implementation
func (v TimelineType) String() string {
	return timelineTypeValueName[v]
}

// ParseTimelineType string parser implementation
func ParseTimelineType(s string) TimelineType {
	return timelineTypeNameValue[s]
}

var timelineTypeValueName = map[TimelineType]string{
	TimelineTypeUnknown:      "",
	TimelineTypeInbox:        "inbox",
	TimelineTypeOutbox:       "outbox",
	TimelineTypeLiked:        "liked",
	TimelineTypeBookmarked:   "bookmarked",
	TimelineTypeNotification: "notification",
}

var timelineTypeNameValue = map[string]TimelineType{
	"":             TimelineTypeUnknown,
	"inbox":        TimelineTypeInbox,
	"outbox":       TimelineTypeOutbox,
	"liked":        TimelineTypeLiked,
	"bookmarked":   TimelineTypeBookmarked,
	"notification": TimelineTypeNotification,
}

// TimelineRepository stores ordered Timelines, such as inbox and outbox
type TimelineRepository interface {
	TimelineSave(ctx context.Context, relation *TimelineItem) error
	TimelineList(
		ctx context.Context,
		authentication vocab.IRI,
		filters *Filters,
		key vocab.IRI,
		typ TimelineType,
		reference flake.ID,
		limit int,
	) (relations []*TimelineItem, err error)
}

// TimelineItem entry model
type TimelineItem struct {
	Created          time.Time
	Key              vocab.IRI
	Activity         vocab.IRI
	ActivityActor    vocab.IRI
	ActivityType     vocab.Type
	ItemActor        vocab.IRI
	ItemReplyToItem  vocab.IRI
	ItemReplyToActor vocab.IRI
	Item             vocab.IRI // item ID
	ItemContext      vocab.IRI
	ItemType         vocab.Type
	Recipients       sorted.IRISet
	Type             TimelineType
	ItemVisibility   vocab.Visibility
	ID               flake.ID
	ItemReply        bool // item is a reply
	ItemShare        bool // the activity is a share
	ItemPinned       bool // item is pinned
	ItemMedia        bool // item contains media
	ItemTags         bool // the activity contains tags
}
