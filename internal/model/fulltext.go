// Package model generic interface for persistent storage
package model

import (
	"context"
	"time"

	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"
)

// FulltextRepository index/search
type FulltextRepository interface {
	FulltextIndex(ctx context.Context, entries ...*Fulltext) error
	FulltextSearch(
		ctx context.Context,
		term string,
		actors, mentions []vocab.IRI,
		start, end time.Time,
	) ([]*Fulltext, error)
}

// Fulltext entry
type Fulltext struct {
	Created  time.Time
	Item     vocab.IRI
	Actor    vocab.IRI
	Text     string
	Mentions []vocab.IRI
}
