package model

// Inclusion criteria
type Inclusion int32

// Inclusion values
const (
	InclusionInclude Inclusion = iota
	InclusionExclude
	InclusionRequire
)

// Filters for timelines
type Filters struct {
	Share   Inclusion
	Reply   Inclusion
	Media   Inclusion
	Tags    Inclusion
	Pinned  Inclusion
	Muted   bool
	Blocked bool
}
