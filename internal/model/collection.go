package model

import (
	"context"
	"time"

	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"
)

// CollectionType relation type between Source and Target
type CollectionType int

// Known relation types
const (
	CollectionTypeUnknown   CollectionType = 0
	CollectionTypeItems     CollectionType = 1
	CollectionTypeActors    CollectionType = 2
	CollectionTypeReplies   CollectionType = 3
	CollectionTypeShares    CollectionType = 4
	CollectionTypeLikes     CollectionType = 5
	CollectionTypeReactions CollectionType = 6
	CollectionTypeFollows   CollectionType = 7
	CollectionTypeFollowers CollectionType = 8
	CollectionTypeBlocks    CollectionType = 9
	CollectionTypeMutes     CollectionType = 10
	CollectionTypeMembers   CollectionType = 11
)

// String implementation
func (v CollectionType) String() string {
	return collectionTypeValueName[v]
}

// ParseCollectionType string parser implementation
func ParseCollectionType(s string) CollectionType {
	return collectionTypeNameValue[s]
}

var collectionTypeValueName = map[CollectionType]string{
	CollectionTypeUnknown:   "",
	CollectionTypeFollows:   "follows",
	CollectionTypeFollowers: "followers",
	CollectionTypeBlocks:    "blocks",
	CollectionTypeMutes:     "mutes",
	CollectionTypeMembers:   "members",
	CollectionTypeLikes:     "likes",
	CollectionTypeShares:    "shares",
	CollectionTypeReactions: "reactions",
}

var collectionTypeNameValue = map[string]CollectionType{
	"":          CollectionTypeUnknown,
	"follows":   CollectionTypeFollows,
	"followers": CollectionTypeFollowers,
	"blocks":    CollectionTypeBlocks,
	"mutes":     CollectionTypeMutes,
	"members":   CollectionTypeMembers,
	"likes":     CollectionTypeLikes,
	"shares":    CollectionTypeShares,
	"reactions": CollectionTypeReactions,
}

// CollectionRepository stores unordered relationships such as Follows, Blocks, etc.
type CollectionRepository interface {
	CollectionItemSave(ctx context.Context, collection *CollectionItem) error
	CollectionItemRemove(ctx context.Context, key vocab.IRI, typ CollectionType, targets ...vocab.IRI) error
	CollectionItemList(
		ctx context.Context,
		keys []vocab.IRI,
		types []CollectionType,
	) (collections []*CollectionItem, err error)
	CollectionItemContains(
		ctx context.Context,
		keys []vocab.IRI,
		types []CollectionType,
		targets []vocab.IRI,
	) (matched map[vocab.IRI]struct{}, err error)
	CollectionItemCountBySources(
		ctx context.Context,
		keys []vocab.IRI,
		types []CollectionType,
	) (count map[CollectionTypeSource]int64, err error)
}

// CollectionTypeSource combined key
type CollectionTypeSource struct {
	Key  vocab.IRI
	Type CollectionType
}

// CollectionItem between Actor and Item
type CollectionItem struct {
	Data    vocab.Item // inline data
	Created time.Time
	Key     vocab.IRI // actor or item
	Element vocab.IRI // actor or item
	Type    CollectionType
}
