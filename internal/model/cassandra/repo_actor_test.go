package cassandra

import (
	"time"

	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"
	"gitlab.eientei.org/eientei/teruyo/internal/flake"
	"gitlab.eientei.org/eientei/teruyo/internal/model"
)

func (s *CassandraSuite) TestActorSave() {
	actor1 := vocab.LocalActor("TestItemSave1")
	actor2 := vocab.IRI("https://example.com/users/foo")

	item1 := model.Actor{
		Attributes: map[string]string{"foo": "aaa"},
		Settings:   map[string]string{"bar": "bbb"},
		Data: &vocab.Actor{
			ObjectBase: vocab.ObjectBase{
				Content: vocab.ValueString("test"),
				ID:      vocab.LocalObject("bar"),
				Type:    vocab.TypeActor,
			},
		},
		Created:        time.Now().UTC().Truncate(time.Millisecond),
		Updated:        time.Now().UTC().Truncate(time.Millisecond),
		Posted:         time.Time{},
		Seen:           time.Time{},
		Fetched:        time.Time{},
		Actor:          actor1,
		ID:             flake.ID{},
		Email:          "aaa",
		Username:       "bbb",
		PasswordHash:   "ccc",
		PrivateKey:     []byte{1, 2, 3},
		FollowersCount: 1,
		FollowingCount: 2,
		Confirmed:      true,
		Locked:         true,
	}

	item2 := model.Actor{
		Data: &vocab.Actor{
			ObjectBase: vocab.ObjectBase{
				Content: vocab.ValueString("test"),
				ID:      vocab.LocalObject("bar"),
				Type:    vocab.TypeActor,
			},
		},
		Created:        time.Now().UTC().Truncate(time.Millisecond),
		Updated:        time.Now().UTC().Truncate(time.Millisecond),
		Actor:          actor2,
		ID:             flake.ID{},
		FollowersCount: 1,
		FollowingCount: 2,
	}

	err := s.repo.ActorSave(s.ctx, &item1)
	s.NoError(err)

	load1, err := s.repo.ActorGetLocalByID(s.ctx, item1.Actor)
	s.NoError(err)

	s.EqualValues(&item1, load1)

	err = s.repo.ActorSave(s.ctx, &item2)
	s.NoError(err)

	load2, err := s.repo.ActorGetFederatedByID(s.ctx, item2.Actor)
	s.NoError(err)

	s.EqualValues(&item2, load2)
}
