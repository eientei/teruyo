package cassandra

import (
	"time"

	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"
	"gitlab.eientei.org/eientei/teruyo/internal/model"
)

func (s *CassandraSuite) TestCollectionItemSave() {
	actor := vocab.LocalActor("TestCollectionItemSave")

	item := &model.CollectionItem{
		Created: time.Now().UTC().Truncate(time.Millisecond),
		Type:    model.CollectionTypeLikes,
		Key:     actor,
		Element: objectBar,
		Data:    vocab.IRI("foo"),
	}

	err := s.repo.CollectionItemSave(s.ctx, item)
	s.NoError(err)

	items, err := s.repo.CollectionItemList(
		s.ctx,
		[]vocab.IRI{actor},
		[]model.CollectionType{model.CollectionTypeLikes},
	)
	s.NoError(err)

	s.EqualValues([]*model.CollectionItem{item}, items)
}

func (s *CassandraSuite) TestCollectionItemRemove() {
	actor := vocab.LocalActor("TestCollectionItemRemove")

	item := &model.CollectionItem{
		Created: time.Now().UTC().Truncate(time.Millisecond),
		Type:    model.CollectionTypeLikes,
		Key:     actor,
		Element: objectBar,
		Data:    vocab.IRI("foo"),
	}

	err := s.repo.CollectionItemSave(s.ctx, item)
	s.NoError(err)

	items, err := s.repo.CollectionItemList(
		s.ctx,
		[]vocab.IRI{actor},
		[]model.CollectionType{model.CollectionTypeLikes},
	)
	s.NoError(err)

	s.Len(items, 1)

	err = s.repo.CollectionItemRemove(
		s.ctx,
		actor,
		model.CollectionTypeLikes,
		objectBar,
	)
	s.NoError(err)

	items, err = s.repo.CollectionItemList(
		s.ctx,
		[]vocab.IRI{actor},
		[]model.CollectionType{model.CollectionTypeLikes},
	)
	s.NoError(err)

	s.Len(items, 0)
}

func (s *CassandraSuite) TestCollectionItemList() {
	actor := vocab.LocalActor("TestCollectionItemList")

	item1 := &model.CollectionItem{
		Created: time.Now().UTC().Truncate(time.Millisecond),
		Type:    model.CollectionTypeLikes,
		Key:     actor,
		Element: objectBar,
		Data:    vocab.IRI("foo"),
	}

	err := s.repo.CollectionItemSave(s.ctx, item1)
	s.NoError(err)

	item2 := &model.CollectionItem{
		Created: time.Now().UTC().Truncate(time.Millisecond),
		Type:    model.CollectionTypeLikes,
		Key:     actor,
		Element: objectBaz,
		Data:    vocab.IRI("bar"),
	}

	err = s.repo.CollectionItemSave(s.ctx, item2)
	s.NoError(err)

	items, err := s.repo.CollectionItemList(
		s.ctx,
		[]vocab.IRI{actor},
		[]model.CollectionType{model.CollectionTypeLikes},
	)
	s.NoError(err)

	s.EqualValues([]*model.CollectionItem{item1, item2}, items)
}

func (s *CassandraSuite) TestCollectionItemContains() {
	actor := vocab.LocalActor("TestCollectionItemContains")

	item := &model.CollectionItem{
		Created: time.Now().UTC().Truncate(time.Millisecond),
		Type:    model.CollectionTypeLikes,
		Key:     actor,
		Element: objectBar,
		Data:    vocab.IRI("foo"),
	}

	err := s.repo.CollectionItemSave(s.ctx, item)
	s.NoError(err)

	matched, err := s.repo.CollectionItemContains(
		s.ctx,
		[]vocab.IRI{actor},
		[]model.CollectionType{model.CollectionTypeLikes},
		[]vocab.IRI{
			objectBar,
			objectBaz,
			objectBar,
		},
	)
	s.NoError(err)

	s.EqualValues(
		map[vocab.IRI]struct{}{
			objectBar: {},
		},
		matched,
	)
}

func (s *CassandraSuite) TestCollectionItemCountBySource() {
	actor1 := vocab.LocalActor("TestCollectionItemCountBySource1")
	actor2 := vocab.LocalActor("TestCollectionItemCountBySource2")
	actor3 := vocab.LocalActor("TestCollectionItemCountBySource3")

	item1 := &model.CollectionItem{
		Created: time.Now().UTC().Truncate(time.Millisecond),
		Type:    model.CollectionTypeLikes,
		Key:     actor1,
		Element: objectBar,
		Data:    vocab.IRI("foo"),
	}

	err := s.repo.CollectionItemSave(s.ctx, item1)
	s.NoError(err)

	item2 := &model.CollectionItem{
		Created: time.Now().UTC().Truncate(time.Millisecond),
		Type:    model.CollectionTypeLikes,
		Key:     actor1,
		Element: objectBaz,
		Data:    vocab.IRI("bar"),
	}

	err = s.repo.CollectionItemSave(s.ctx, item2)
	s.NoError(err)

	item3 := &model.CollectionItem{
		Created: time.Now().UTC().Truncate(time.Millisecond),
		Type:    model.CollectionTypeLikes,
		Key:     actor2,
		Element: objectBaz,
		Data:    vocab.IRI("bar"),
	}

	err = s.repo.CollectionItemSave(s.ctx, item3)
	s.NoError(err)

	item4 := &model.CollectionItem{
		Created: time.Now().UTC().Truncate(time.Millisecond),
		Type:    model.CollectionTypeBlocks,
		Key:     actor2,
		Element: objectBaz,
		Data:    vocab.IRI("bar"),
	}

	err = s.repo.CollectionItemSave(s.ctx, item4)
	s.NoError(err)

	items, err := s.repo.CollectionItemCountBySources(
		s.ctx,
		[]vocab.IRI{
			actor1,
			actor2,
			actor3,
		},
		[]model.CollectionType{model.CollectionTypeLikes, model.CollectionTypeBlocks},
	)
	s.NoError(err)

	s.EqualValues(
		map[model.CollectionTypeSource]int64{
			{Type: model.CollectionTypeLikes, Key: actor1}:  2,
			{Type: model.CollectionTypeLikes, Key: actor2}:  1,
			{Type: model.CollectionTypeBlocks, Key: actor2}: 1,
		},
		items,
	)
}

func (s *CassandraSuite) TestCollectionItemUpgradeGeneration() {
	actor := vocab.LocalActor("TestCollectionItemUpgradeGeneration")

	item1 := &model.CollectionItem{
		Created: time.Now().UTC().Truncate(time.Millisecond),
		Key:     actor,
		Type:    model.CollectionTypeLikes,
		Element: objectBar,
		Data:    vocab.IRI("foo"),
	}

	err := s.repo.CollectionItemSave(s.ctx, item1)
	s.NoError(err)

	item2 := &model.CollectionItem{
		Created: time.Now().UTC().Truncate(time.Millisecond),
		Key:     actor,
		Type:    model.CollectionTypeLikes,
		Element: objectBaz,
		Data:    vocab.IRI("bar"),
	}

	err = s.repo.CollectionItemSave(s.ctx, item2)
	s.NoError(err)

	err = s.repo.CollectionItemUpgradeGeneration(s.ctx, actor, model.CollectionTypeLikes)
	s.NoError(err)
}
