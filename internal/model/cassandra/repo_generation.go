package cassandra

import (
	"context"
	"errors"
	"sync/atomic"
	"time"

	"github.com/gocql/gocql"
	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"
	"gitlab.eientei.org/eientei/teruyo/internal/model"
)

var epoch = time.Date(2024, time.January, 1, 0, 0, 0, 0, time.UTC)

func (repo *repositoryImpl) NextHashedGeneration(power uint64) uint64 {
	var ts uint64

	for {
		ts = uint64(time.Since(epoch).Milliseconds())

		if atomic.CompareAndSwapUint64(&repo.timestamp, ts, ts) {
			time.Sleep(time.Microsecond * 100)

			continue
		}

		atomic.StoreUint64(&repo.timestamp, ts)

		break
	}

	return ts<<22 | repo.machineID | (power & 0x3f)
}

const queryGenerationTimedInit = `
insert into generations(
  repository,
  key,
  type,
  current,
  pending,
  current_last_bucket,
  pending_last_bucket
) values (
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
)
`

const queryGenerationTimedGet = `
select
  current,
  pending,
  current_last_bucket,
  pending_last_bucket
from generations where
  repository = ? and
  key = ? and
  type = ?
`

func (repo *repositoryImpl) GenerationTimedGet(
	ctx context.Context,
	repository RepositoryType,
	key vocab.IRI,
	typ model.TimelineType,
) (generation *Generation, err error) {
	defer repo.Trace(&ctx, &err)()

	q := repo.DB.QueryContext(
		ctx,
		queryGenerationTimedGet,
		repository,
		key,
		typ,
	)

	defer q.Release()

	var g Generation

	err = q.Scan(
		&g.Current,
		&g.Pending,
		&g.CurrentLastBucket,
		&g.PendingLastBucket,
	)

	if errors.Is(err, gocql.ErrNotFound) {
		return nil, nil
	}

	return &g, nil
}

const queryGenerationHashedInit = `
insert into generations(
  repository,
  key,
  type,
  current,
  pending
) values (
  ?,
  ?,
  ?,
  ?,
  ?
)
`
const queryGenerationHashedGet = `
select
  current,
  pending
from generations where
  repository = ? and
  key in ? and
  type in ?
`

func (repo *repositoryImpl) GenerationHashedGet(
	ctx context.Context,
	repository RepositoryType,
	keys []vocab.IRI,
	types []model.CollectionType,
) (generations []*Generation, err error) {
	defer repo.Trace(&ctx, &err)()

	q := repo.DB.QueryContext(
		ctx,
		queryGenerationHashedGet,
		repository,
		keys,
		types,
	)

	defer q.Release()

	scan := q.Iter().Scanner()

	for scan.Next() {
		var current, pending uint64

		err = scan.Scan(
			&current,
			&pending,
		)
		if err != nil {
			_ = scan.Err()

			return nil, err
		}

		generations = append(generations, &Generation{
			Current: current,
			Pending: pending,
		})
	}

	err = scan.Err()
	if err != nil {
		return nil, err
	}

	return
}

const queryGenerationHashedDistinctCurrent = `
select
  current
from generations where
  repository = ? and
  key in ? and
  type in ?
`

func (repo *repositoryImpl) GenerationHashedDistinctCurrent(
	ctx context.Context,
	repository RepositoryType,
	keys []vocab.IRI,
	types []model.CollectionType,
) (generations []uint64, err error) {
	defer repo.Trace(&ctx, &err)()

	q := repo.DB.QueryContext(
		ctx,
		queryGenerationHashedDistinctCurrent,
		repository,
		keys,
		types,
	)

	defer q.Release()

	scan := q.Iter().Scanner()

	distinct := make(map[uint64]struct{})

	for scan.Next() {
		var g uint64

		err = scan.Scan(
			&g,
		)
		if err != nil {
			_ = scan.Err()

			return nil, err
		}

		if _, ok := distinct[g]; !ok {
			distinct[g] = struct{}{}

			generations = append(generations, g)
		}
	}

	err = scan.Err()
	if err != nil {
		return nil, err
	}

	return
}

const queryGenerationUpdateCurrent = `
update generations set
  current = ?
where
  repository = ? and
  key = ? and
  type = ?
`

func (repo *repositoryImpl) GenerationUpdateCurrent(
	ctx context.Context,
	repository RepositoryType,
	key string,
	typ int,
	generation uint64,
) (err error) {
	defer repo.Trace(&ctx, &err)()

	q := repo.DB.QueryContext(ctx, queryGenerationUpdateCurrent, generation, repository, key, typ)

	defer q.Release()

	return q.Exec()
}

const queryGenerationUpdatePending = `
update generations set
  pending = ?
where
  repository = ? and
  key = ? and
  type = ?
`

func (repo *repositoryImpl) GenerationUpdatePending(
	ctx context.Context,
	repository RepositoryType,
	key string,
	typ int,
	generation uint64,
) (err error) {
	defer repo.Trace(&ctx, &err)()

	q := repo.DB.QueryContext(ctx, queryGenerationUpdatePending, generation, repository, key, typ)

	defer q.Release()

	return q.Exec()
}

const queryGenerationUpdateLastAll = `
update generations set
  current_last_bucket = ?,
  pending_last_bucket = ?
where
  repository = ? and
  key = ? and
  type = ?
`

const queryGenerationUpdateLastCurrent = `
update generations set
  current_last_bucket = ?
where
  repository = ? and
  key = ? and
  type = ?
`

const queryGenerationUpdateLastPending = `
update generations set
  pending_last_bucket = ?
where
  repository = ? and
  key = ? and
  type = ?
`

var queryGenerationUpdateLast = []string{
	queryGenerationUpdateLastCurrent,
	queryGenerationUpdateLastPending,
}

//nolint:revive // temporary
func (repo *repositoryImpl) GenerationPressure(
	ctx context.Context,
	repository RepositoryType,
	keys []string,
) (uint64, error) {
	return 0, nil
}
