package cassandra

import (
	"time"

	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"
	"gitlab.eientei.org/eientei/teruyo/internal/flake"
	"gitlab.eientei.org/eientei/teruyo/internal/model"
)

func (s *CassandraSuite) TestGenerationTimelineSave() {
	actor := vocab.LocalActor("TestGenerationTimelineSave")

	created1 := time.Now().UTC().Truncate(time.Millisecond)
	id1 := s.config.Generator.Next()

	err := s.repo.TimelineSave(
		s.ctx,
		&model.TimelineItem{
			Created:          created1,
			Key:              actor,
			ActivityActor:    actor,
			ItemActor:        actor,
			ItemReplyToItem:  "",
			ItemReplyToActor: "",
			Item:             objectBar,
			ItemContext:      "a",
			ItemType:         vocab.TypeObjectNote,
			Type:             model.TimelineTypeInbox,
			ItemVisibility:   vocab.VisibilityPublic,
			ID:               id1,
			ItemReply:        true,
			ItemShare:        true,
			ItemPinned:       false,
			ItemMedia:        true,
			ItemTags:         true,
		},
	)
	s.NoError(err)

	list, err := s.repo.TimelineList(
		s.ctx,
		actor,
		&model.Filters{},
		actor,
		model.TimelineTypeInbox,
		flake.ID{},
		10,
	)
	s.NoError(err)

	s.EqualValues(
		[]*model.TimelineItem{
			{
				Created:          created1,
				Key:              actor,
				ActivityActor:    actor,
				ItemActor:        actor,
				ItemReplyToItem:  "",
				ItemReplyToActor: "",
				Item:             objectBar,
				ItemContext:      "a",
				ItemType:         vocab.TypeObjectNote,
				Type:             model.TimelineTypeInbox,
				ItemVisibility:   vocab.VisibilityPublic,
				ID:               id1,
				ItemReply:        true,
				ItemShare:        true,
				ItemPinned:       false,
				ItemMedia:        true,
				ItemTags:         true,
			},
		},
		list,
	)

	next := time.Now().Add(time.Hour * 24 * 40).UTC().Truncate(time.Millisecond)

	nextID := s.config.Generator.Next()
	nextID.Time = uint64(next.UnixMilli())

	err = s.repo.TimelineSave(
		s.ctx,
		&model.TimelineItem{
			Created:          next,
			Key:              actor,
			ActivityActor:    actor,
			ItemActor:        actor,
			ItemReplyToItem:  "",
			ItemReplyToActor: "",
			Item:             objectBar,
			ItemContext:      "a",
			ItemType:         vocab.TypeObjectNote,
			Type:             model.TimelineTypeInbox,
			ItemVisibility:   vocab.VisibilityPublic,
			ID:               nextID,
			ItemReply:        true,
			ItemShare:        true,
			ItemPinned:       false,
			ItemMedia:        true,
			ItemTags:         true,
		},
	)
	s.NoError(err)

	list, err = s.repo.TimelineList(
		s.ctx,
		actor,
		&model.Filters{},
		actor,
		model.TimelineTypeInbox,
		flake.ID{},
		10,
	)
	s.NoError(err)

	s.EqualValues(
		[]*model.TimelineItem{
			{
				Created:          next,
				Key:              actor,
				ActivityActor:    actor,
				ItemActor:        actor,
				ItemReplyToItem:  "",
				ItemReplyToActor: "",
				Item:             objectBar,
				ItemContext:      "a",
				ItemType:         vocab.TypeObjectNote,
				Type:             model.TimelineTypeInbox,
				ItemVisibility:   vocab.VisibilityPublic,
				ID:               nextID,
				ItemReply:        true,
				ItemShare:        true,
				ItemPinned:       false,
				ItemMedia:        true,
				ItemTags:         true,
			},
			{
				Created:          created1,
				Key:              actor,
				ActivityActor:    actor,
				ItemActor:        actor,
				ItemReplyToItem:  "",
				ItemReplyToActor: "",
				Item:             objectBar,
				ItemContext:      "a",
				ItemType:         vocab.TypeObjectNote,
				Type:             model.TimelineTypeInbox,
				ItemVisibility:   vocab.VisibilityPublic,
				ID:               id1,
				ItemReply:        true,
				ItemShare:        true,
				ItemPinned:       false,
				ItemMedia:        true,
				ItemTags:         true,
			},
		},
		list,
	)

	err = s.repo.TimelineSave(
		s.ctx,
		&model.TimelineItem{
			Created:          next,
			Key:              actor,
			ActivityActor:    actor,
			ItemActor:        actor,
			ItemReplyToItem:  "",
			ItemReplyToActor: "",
			Item:             objectBar,
			ItemContext:      "a",
			ItemType:         vocab.TypeObjectNote,
			Type:             model.TimelineTypeInbox,
			ItemVisibility:   vocab.VisibilityPublic,
			ID:               nextID,
			ItemReply:        true,
			ItemShare:        true,
			ItemPinned:       true,
			ItemMedia:        true,
			ItemTags:         true,
		},
	)
	s.NoError(err)

	list, err = s.repo.TimelineList(
		s.ctx,
		actor,
		&model.Filters{},
		actor,
		model.TimelineTypeInbox,
		flake.ID{},
		10,
	)
	s.NoError(err)

	s.EqualValues(
		[]*model.TimelineItem{
			{
				Created:          next,
				Key:              actor,
				ActivityActor:    actor,
				ItemActor:        actor,
				ItemReplyToItem:  "",
				ItemReplyToActor: "",
				Item:             objectBar,
				ItemContext:      "a",
				ItemType:         vocab.TypeObjectNote,
				Type:             model.TimelineTypeInbox,
				ItemVisibility:   vocab.VisibilityPublic,
				ID:               nextID,
				ItemReply:        true,
				ItemShare:        true,
				ItemPinned:       true,
				ItemMedia:        true,
				ItemTags:         true,
			},
			{
				Created:          next,
				Key:              actor,
				ActivityActor:    actor,
				ItemActor:        actor,
				ItemReplyToItem:  "",
				ItemReplyToActor: "",
				Item:             objectBar,
				ItemContext:      "a",
				ItemType:         vocab.TypeObjectNote,
				Type:             model.TimelineTypeInbox,
				ItemVisibility:   vocab.VisibilityPublic,
				ID:               nextID,
				ItemReply:        true,
				ItemShare:        true,
				ItemPinned:       false,
				ItemMedia:        true,
				ItemTags:         true,
			},
			{
				Created:          created1,
				Key:              actor,
				ActivityActor:    actor,
				ItemActor:        actor,
				ItemReplyToItem:  "",
				ItemReplyToActor: "",
				Item:             objectBar,
				ItemContext:      "a",
				ItemType:         vocab.TypeObjectNote,
				Type:             model.TimelineTypeInbox,
				ItemVisibility:   vocab.VisibilityPublic,
				ID:               id1,
				ItemReply:        true,
				ItemShare:        true,
				ItemPinned:       false,
				ItemMedia:        true,
				ItemTags:         true,
			},
		},
		list,
	)
}

func (s *CassandraSuite) TestGenerationTimelineRemove() {
	actor := vocab.LocalActor("TestGenerationTimelineRemove")

	created1 := time.Now().UTC().Truncate(time.Millisecond)
	id1 := s.config.Generator.Next()

	err := s.repo.TimelineSave(
		s.ctx,
		&model.TimelineItem{
			Created:          created1,
			Key:              actor,
			ActivityActor:    actor,
			ItemActor:        actor,
			ItemReplyToItem:  "",
			ItemReplyToActor: "",
			Item:             objectBar,
			ItemContext:      "a",
			ItemType:         vocab.TypeObjectNote,
			Type:             model.TimelineTypeInbox,
			ItemVisibility:   vocab.VisibilityPublic,
			ID:               id1,
			ItemReply:        true,
			ItemShare:        true,
			ItemPinned:       true,
			ItemMedia:        true,
			ItemTags:         true,
		},
	)
	s.NoError(err)

	list, err := s.repo.TimelineList(
		s.ctx,
		actor,
		&model.Filters{},
		actor,
		model.TimelineTypeInbox,
		flake.ID{},
		10,
	)
	s.NoError(err)

	s.EqualValues(
		[]*model.TimelineItem{
			{
				Created:          created1,
				Key:              actor,
				ActivityActor:    actor,
				ItemActor:        actor,
				ItemReplyToItem:  "",
				ItemReplyToActor: "",
				Item:             objectBar,
				ItemContext:      "a",
				ItemType:         vocab.TypeObjectNote,
				Type:             model.TimelineTypeInbox,
				ItemVisibility:   vocab.VisibilityPublic,
				ID:               id1,
				ItemReply:        true,
				ItemShare:        true,
				ItemPinned:       true,
				ItemMedia:        true,
				ItemTags:         true,
			},
			{
				Created:          created1,
				Key:              actor,
				ActivityActor:    actor,
				ItemActor:        actor,
				ItemReplyToItem:  "",
				ItemReplyToActor: "",
				Item:             objectBar,
				ItemContext:      "a",
				ItemType:         vocab.TypeObjectNote,
				Type:             model.TimelineTypeInbox,
				ItemVisibility:   vocab.VisibilityPublic,
				ID:               id1,
				ItemReply:        true,
				ItemShare:        true,
				ItemPinned:       false,
				ItemMedia:        true,
				ItemTags:         true,
			},
		},
		list,
	)

	err = s.repo.TimelineRemove(s.ctx, actor, model.TimelineTypeInbox, id1)
	s.NoError(err)

	list, err = s.repo.TimelineList(
		s.ctx,
		actor,
		&model.Filters{},
		actor,
		model.TimelineTypeInbox,
		flake.ID{},
		10,
	)
	s.NoError(err)

	s.Empty(list)
}

func (s *CassandraSuite) TestGenerationTimelineList() {
	actor1 := vocab.LocalActor("TestGenerationTimelineList1")
	actor2 := vocab.LocalActor("TestGenerationTimelineList2")

	err := s.repo.TimelineSave(
		s.ctx,
		&model.TimelineItem{
			Created:          time.Now().UTC().Truncate(time.Millisecond),
			Key:              actor1,
			ActivityActor:    actor1,
			ItemActor:        actor1,
			ItemReplyToItem:  "",
			ItemReplyToActor: "",
			Item:             objectBar,
			ItemContext:      "a",
			ItemType:         vocab.TypeObjectNote,
			Type:             model.TimelineTypeInbox,
			ItemVisibility:   vocab.VisibilityPublic,
			ID:               s.config.Generator.Next(),
			ItemReply:        true,
			ItemShare:        true,
			ItemPinned:       false,
			ItemMedia:        true,
			ItemTags:         true,
		},
	)
	s.NoError(err)

	next := time.Now().Add(time.Hour * 24 * 40).UTC().Truncate(time.Millisecond)

	nextID := s.config.Generator.Next()
	nextID.Time = uint64(next.UnixMilli())

	err = s.repo.TimelineSave(
		s.ctx,
		&model.TimelineItem{
			Created:          next,
			Key:              actor1,
			ActivityActor:    actor2,
			ItemActor:        actor2,
			ItemReplyToItem:  "",
			ItemReplyToActor: "",
			Item:             objectBar,
			ItemContext:      "a",
			ItemType:         vocab.TypeObjectNote,
			Type:             model.TimelineTypeInbox,
			ItemVisibility:   vocab.VisibilityPublic,
			ID:               nextID,
			ItemReply:        true,
			ItemShare:        true,
			ItemPinned:       false,
			ItemMedia:        true,
			ItemTags:         true,
		},
	)
	s.NoError(err)

	list, err := s.repo.TimelineList(
		s.ctx,
		actor1,
		&model.Filters{},
		actor1,
		model.TimelineTypeInbox,
		flake.ID{},
		10,
	)
	s.NoError(err)

	s.Len(list, 2)

	list, err = s.repo.TimelineList(
		s.ctx,
		actor1,
		&model.Filters{},
		actor1,
		model.TimelineTypeInbox,
		flake.ID{},
		1,
	)
	s.NoError(err)

	s.Len(list, 1)

	err = s.repo.CollectionItemSave(
		s.ctx,
		&model.CollectionItem{
			Created: time.Now(),
			Key:     actor1,
			Type:    model.CollectionTypeBlocks,
			Element: actor2,
			Data:    nil,
		},
	)
	s.NoError(err)

	list, err = s.repo.TimelineList(
		s.ctx,
		actor1,
		&model.Filters{},
		actor1,
		model.TimelineTypeInbox,
		flake.ID{},
		10,
	)
	s.NoError(err)

	s.Len(list, 1)
}
