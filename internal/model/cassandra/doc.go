// Package cassandra provides persistent repository implementation using cassandra/scylla database
// # Repository structure
//
// Two main collection types are used:
//
// - time-partitioned
// - hash-partitioned
//
// Each collection has associated `generation` integer which determines how often it is partitioned.
//
// Once number of items/insert rate exceeds beyond what current `generation` could support within 100Mb
// limit per partition, new `pending generation` is introduced and migration starts.
//
// All data from previous `generation` is copied to the new `pending generation`, using more fine-grained
// partition subdivisions.
//
// While this process is in progress, all mutating operations must be replicated for both current and
// pending generations.
//
// Once migration is complete, previous generation can be range-deleted.
//
// Each table has 128-bit `genbucket` field which contains combined generation and bucket values.
//
// Note that all 128-bit values are stored as blobs to avoid uuid semantics assigning higher portion of 7th byte the
// most significance.
//
// ## Hash-partitioned collections
//
// By it's nature unordered collections, basically hash sets of elements.
// Generation splits 64-bit hash space into a number of buckets, using
//
// ```
// hash_bucket = H / (H_max/(2^generation_power_of_two))
// ```
//
// Where `H` is the 64-bit hash and `H_max` is 2^64-1.
//
// Meaning generation_power_of_two 0 would result in whole hash space being put in single bucket,
// while generation_power_of_two 2 would mean hash space split evenly between 4 buckets.
//
// Additionally, since hash-partitioned collections are used to lookup many entries for many objects, each generation
// must be kept unique across entire table. This is ensured by using following layout:
// - 42 bits of unix timestamp in millisecond resolution
// - 16 bits identifying one of 65535 possible shards
// - 6 bits encoding power of two
//
// ## Time-partitioned collections
//
// Time-ordered collections, currently used exclusively for timelines.
// Each element is having time-ordered 128-bit ID for compatability with pleroma flake ID implementation.
// Generation partitions 64-bit time portion of that 128-bit time-ordered ID into equal time intervals.
//
// ```
// hash_bucket = T / 2^generation
// ```
// where T is 64-bit time portion of time-ordered ID in millisecond resolution.
//
// For time-partitioned collections generation values are not required to be unique.
//
// Pinned statuses are implemented as timeline entries in the bucket of math.MaxUint64.
//
// ## Limitations
//
// For performance reasons, current partitioning scheme is inherently limited by 139 years of operation span and
// 65535 of maximum shards, but both values can be expanded via appropriate migrations in the future, increasing
// the width of genbucket blob values.
package cassandra
