package cassandra

import (
	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"
	"gitlab.eientei.org/eientei/teruyo/internal/model"
)

func (s *CassandraSuite) TestGenerationTimedGet() {
	repo := RepositoryType(0xff - 1)

	g, err := s.repo.GenerationTimedGet(
		s.ctx,
		repo,
		"bar",
		model.TimelineTypeInbox,
	)
	s.NoError(err)

	s.Nil(g)

	err = s.repo.GenerationUpdatePending(
		s.ctx,
		repo,
		"bar",
		int(model.TimelineTypeInbox),
		1,
	)
	s.NoError(err)

	g, err = s.repo.GenerationTimedGet(
		s.ctx,
		repo,
		"bar",
		model.TimelineTypeInbox,
	)
	s.NoError(err)

	s.Equal(uint64(0), g.Current)
	s.Equal(uint64(1), g.Pending)
	s.Equal(uint64(0), g.CurrentLastBucket)
	s.Equal(uint64(0), g.PendingLastBucket)

	err = s.repo.GenerationUpdateCurrent(
		s.ctx,
		repo,
		"bar",
		int(model.TimelineTypeInbox),
		1,
	)
	s.NoError(err)

	g, err = s.repo.GenerationTimedGet(
		s.ctx,
		repo,
		"bar",
		model.TimelineTypeInbox,
	)
	s.NoError(err)

	s.Equal(uint64(1), g.Current)
	s.Equal(uint64(1), g.Pending)
	s.Equal(uint64(0), g.CurrentLastBucket)
	s.Equal(uint64(0), g.PendingLastBucket)
}

func (s *CassandraSuite) TestGenerationHashedGet() {
	repo := RepositoryType(0xff - 2)

	gs, err := s.repo.GenerationHashedGet(
		s.ctx,
		repo,
		[]vocab.IRI{"bar"},
		[]model.CollectionType{model.CollectionTypeLikes},
	)
	s.NoError(err)

	s.Empty(gs)

	err = s.repo.GenerationUpdatePending(
		s.ctx,
		repo,
		"bar",
		int(model.CollectionTypeLikes),
		1,
	)
	s.NoError(err)

	gs, err = s.repo.GenerationHashedGet(
		s.ctx,
		repo,
		[]vocab.IRI{"bar"},
		[]model.CollectionType{model.CollectionTypeLikes},
	)
	s.NoError(err)

	s.EqualValues(
		[]*Generation{
			{
				Current:           uint64(0),
				Pending:           uint64(1),
				CurrentLastBucket: 0,
				PendingLastBucket: 0,
			},
		},
		gs,
	)

	err = s.repo.GenerationUpdateCurrent(
		s.ctx,
		repo,
		"bar",
		int(model.CollectionTypeLikes),
		1,
	)
	s.NoError(err)

	gs, err = s.repo.GenerationHashedGet(
		s.ctx,
		repo,
		[]vocab.IRI{"bar"},
		[]model.CollectionType{model.CollectionTypeLikes},
	)
	s.NoError(err)

	s.EqualValues(
		[]*Generation{
			{
				Current:           uint64(1),
				Pending:           uint64(1),
				CurrentLastBucket: 0,
				PendingLastBucket: 0,
			},
		},
		gs,
	)
}

func (s *CassandraSuite) TestGenerationHashedDistinctCurrent() {
	repo := RepositoryType(0xff - 3)

	gs, err := s.repo.GenerationHashedDistinctCurrent(
		s.ctx,
		repo,
		[]vocab.IRI{"bar", "baz"},
		[]model.CollectionType{model.CollectionTypeFollows, model.CollectionTypeFollowers},
	)
	s.NoError(err)

	s.Len(gs, 0)

	err = s.repo.GenerationUpdateCurrent(s.ctx, repo, "bar", int(model.CollectionTypeFollows), 1)
	s.NoError(err)

	err = s.repo.GenerationUpdateCurrent(s.ctx, repo, "baz", int(model.CollectionTypeFollowers), 2)
	s.NoError(err)

	gs, err = s.repo.GenerationHashedDistinctCurrent(
		s.ctx,
		repo,
		[]vocab.IRI{"bar", "baz"},
		[]model.CollectionType{model.CollectionTypeFollows, model.CollectionTypeFollowers},
	)
	s.NoError(err)

	s.EqualValues([]uint64{1, 2}, gs)
}
