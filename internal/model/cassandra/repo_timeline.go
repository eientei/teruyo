package cassandra

import (
	"context"
	"math"
	"time"

	"github.com/gocql/gocql"
	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"
	"gitlab.eientei.org/eientei/teruyo/internal/errcode"
	"gitlab.eientei.org/eientei/teruyo/internal/flake"
	"gitlab.eientei.org/eientei/teruyo/internal/model"
	"gitlab.eientei.org/eientei/teruyo/internal/persistence/cassandra"
	"gitlab.eientei.org/eientei/teruyo/internal/sorted"
)

func timelineFields(relation *model.TimelineItem, extra ...interface{}) []interface{} {
	return append(
		extra,
		&relation.Key,
		&relation.Type,
		(*FlakeID)(&relation.ID),
		&relation.Activity,
		&relation.ActivityActor,
		&relation.ActivityType,
		&relation.Item,
		&relation.ItemContext,
		&relation.Recipients,
		&relation.ItemVisibility,
		&relation.ItemActor,
		&relation.ItemReplyToItem,
		&relation.ItemReplyToActor,
		&relation.ItemType,
		&relation.ItemReply,
		&relation.ItemShare,
		&relation.ItemMedia,
		&relation.ItemTags,
		&relation.Created,
	)
}

const queryTimelineSave = `
insert into timelines(
  genbucket,
  key,
  type,
  id,
  activity,
  activity_actor,
  activity_type,
  item,
  item_context,
  item_recipients,
  item_visibility,
  item_actor,
  item_reply_to_item,
  item_reply_to_actor,
  item_type,
  item_reply,
  item_share,
  item_media,
  item_tags,
  created
) values (
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
)
`

const queryTimelineSaveNav = `
insert into timelines(
  genbucket,
  key,
  type,
  id,
  nav_type,
  nav_bucket
) values (
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
)
`

var errNoID = errcode.CodeInvalidState.New("ID not specified")

func (repo *repositoryImpl) TimelineSave(
	ctx context.Context,
	relation *model.TimelineItem,
) (err error) {
	defer repo.Trace(&ctx, &err)()

	b := repo.DB.NewBatchContext(ctx, gocql.LoggedBatch)

	err = repo.timelineSaveBatch(ctx, relation, relation.ItemPinned, b)
	if err != nil {
		return err
	}

	return repo.DB.ExecuteBatch(b)
}

/*
func (repo *repositoryImpl) timelineSaveBatchItem(
	ctx context.Context,
	activityType vocab.Type,
	activity, actor, recipient vocab.IRI,
	typ model.TimelineType,
	item *model.Item,
	pinned, share bool,
	b cassandra.Batch,
) (err error) {
	return repo.timelineSaveBatch(
		ctx,
		&model.TimelineItem{
			Created:          time.Now(),
			Key:              recipient,
			Activity:         activity,
			ActivityActor:    actor,
			ActivityType:     activityType,
			ItemActor:        item.Actor,
			ItemReplyToItem:  item.ReplyToItem,
			ItemReplyToActor: item.ReplyToActor,
			Item:             item.Item,
			ItemContext:      item.Context,
			ItemType:         item.Data.AsType(),
			Recipients:       item.Recipients,
			Type:             typ,
			ItemVisibility:   item.Visibility,
			ID:               item.ID,
			ItemReply:        item.ReplyToItem != "",
			ItemShare:        share,
			ItemPinned:       pinned,
			ItemMedia:        item.Media,
			ItemTags:         len(item.Tags) > 0,
		},
		pinned,
		b,
	)
}

*/

func (repo *repositoryImpl) timelineSaveBatch(
	ctx context.Context,
	relation *model.TimelineItem,
	pinned bool,
	b cassandra.Batch,
) (err error) {
	if relation.ID.IsZero() {
		return errNoID
	}

	if relation.Created.IsZero() {
		relation.Created = time.Now()
	}

	genbuckets, init, err := repo.GenerationTimeBuckets(
		ctx,
		RepositoryTypeTimelines,
		relation.Key,
		relation.Type,
		relation.ID.Time,
		true,
	)
	if err != nil {
		return err
	}

	if init {
		b.Query(
			queryGenerationTimedInit,
			RepositoryTypeTimelines,
			relation.Key,
			relation.Type,
			genbuckets[0].Generation,
			genbuckets[0].Generation,
			genbuckets[0].Bucket,
			genbuckets[0].Bucket,
		)
		b.Query(
			queryTimelineSaveNav,
			genbuckets[0],
			relation.Key,
			relation.Type,
			FlakeID{
				Time:     0,
				Instance: 0,
			},
			NavigationTypeFirst,
			0,
		)

		genbuckets[0].Last = genbuckets[0].Bucket
	}

	for idx, g := range genbuckets {
		b.Query(queryTimelineSave, timelineFields(relation, g)...)

		if pinned {
			b.Query(
				queryTimelineSave,
				timelineFields(
					relation,
					GenerationBucket{
						Generation: g.Generation,
						Bucket:     math.MaxUint64,
					},
				)...,
			)
		}

		if g.Bucket > g.Last {
			b.Query(
				queryTimelineSaveNav,
				g,
				relation.Key,
				relation.Type,
				FlakeID{
					Time:     0,
					Instance: 0,
				},
				NavigationTypeOlder,
				g.Last,
			)
			b.Query(
				queryTimelineSaveNav,
				GenerationBucket{
					Generation: g.Generation,
					Bucket:     g.Last,
				},
				relation.Key,
				relation.Type,
				FlakeID{
					Time:     math.MaxUint64,
					Instance: math.MaxUint64,
				},
				NavigationTypeNewer,
				g.Bucket,
			)

			if len(genbuckets) == 1 {
				b.Query(
					queryGenerationUpdateLastAll,
					g.Bucket,
					g.Bucket,
					RepositoryTypeTimelines,
					relation.Key,
					relation.Type,
				)
			} else {
				b.Query(
					queryGenerationUpdateLast[idx],
					g.Bucket,
					RepositoryTypeTimelines,
					relation.Key,
					relation.Type,
				)
			}
		}
	}

	return
}

const queryTimelineRemove = `
delete from timelines where
  genbucket = ? and
  key = ? and
  type = ? and
  id = ?
`

func (repo *repositoryImpl) TimelineRemove(
	ctx context.Context,
	key vocab.IRI,
	typ model.TimelineType,
	reference flake.ID,
) (err error) {
	defer repo.Trace(&ctx, &err)()

	b := repo.DB.NewBatchContext(ctx, gocql.LoggedBatch)

	err = repo.timelineRemoveBatch(ctx, key, typ, reference, true, true, b)
	if err != nil {
		return err
	}

	return repo.DB.ExecuteBatch(b)
}

func (repo *repositoryImpl) timelineRemoveBatch(
	ctx context.Context,
	key vocab.IRI,
	typ model.TimelineType,
	reference flake.ID,
	main, pinned bool,
	b cassandra.Batch,
) error {
	gs, _, err := repo.GenerationTimeBuckets(
		ctx,
		RepositoryTypeTimelines,
		key,
		typ,
		reference.Time,
		false,
	)
	if err != nil {
		return err
	}

	for _, g := range gs {
		if main {
			b.Query(
				queryTimelineRemove,
				g,
				key,
				typ,
				FlakeID(reference),
			)
		}

		if pinned {
			b.Query(
				queryTimelineRemove,
				GenerationBucket{
					Generation: g.Generation,
					Bucket:     math.MaxUint64,
				},
				key,
				typ,
				FlakeID(reference),
			)
		}
	}

	return nil
}

const queryTimelineList = `
select
  nav_type,
  nav_bucket,
  key,
  type,
  id,
  activity,
  activity_actor,
  activity_type,
  item,
  item_context,
  item_recipients,
  item_visibility,
  item_actor,
  item_reply_to_item,
  item_reply_to_actor,
  item_type,
  item_reply,
  item_media,
  item_tags,
  item_share,
  created
from timelines where
  genbucket = ? and
  key = ? and
  type = ? and
  id < ?
order by id desc
`

//nolint:gocognit
func (repo *repositoryImpl) TimelineList(
	ctx context.Context,
	authentication vocab.IRI,
	filters *model.Filters,
	key vocab.IRI,
	typ model.TimelineType,
	reference flake.ID,
	limit int,
) (relations []*model.TimelineItem, err error) {
	defer repo.Trace(&ctx, &err)()

	gs, err := repo.GenerationTimedGet(
		ctx,
		RepositoryTypeTimelines,
		key,
		typ,
	)
	if err != nil {
		return
	}

	if gs == nil {
		return nil, nil
	}

	if reference.IsZero() {
		reference = flake.ID{
			Time:     ((gs.CurrentLastBucket + 1) * gs.Current) - 1,
			Instance: math.MaxUint64,
		}
	}

	outbox := typ == model.TimelineTypeOutbox

	follower := authentication == key

	if outbox && !follower && authentication != "" {
		var match map[vocab.IRI]struct{}

		match, err = repo.CollectionItemContains(
			ctx,
			[]vocab.IRI{authentication},
			[]model.CollectionType{model.CollectionTypeFollows},
			[]vocab.IRI{key},
		)
		if err != nil {
			return nil, err
		}

		_, follower = match[key]
	}

	bucket := reference.Time / gs.Current

	q := repo.DB.QueryContext(ctx, queryTimelineList)

	defer q.Release()

	pinned := filters.Pinned != model.InclusionExclude

	if pinned {
		q.Bind(
			GenerationBucket{
				Generation: gs.Current,
				Bucket:     math.MaxUint64,
			},
			key,
			typ,
			FlakeID(reference),
		)
	} else {
		q.Bind(
			GenerationBucket{
				Generation: gs.Current,
				Bucket:     bucket,
			},
			key,
			typ,
			FlakeID(reference),
		)
	}

	iter := q.Iter()

	defer func() {
		nerr := iter.Close()
		if nerr != nil && err == nil {
			err = nerr
		}
	}()

loop:
	for len(relations) < limit {
		var (
			navtype   NavigationType
			navbucket uint64
		)

		item := &model.TimelineItem{}

		if !iter.Scan(timelineFields(item, &navtype, &navbucket)...) {
			err = iter.Close()
			if err != nil {
				return nil, err
			}

			if !pinned {
				break
			}

			if filters.Pinned == model.InclusionRequire {
				break
			}

			pinned = false

			q.Bind(
				GenerationBucket{
					Generation: gs.Current,
					Bucket:     bucket,
				},
				key,
				typ,
				FlakeID(reference),
			)

			iter = q.Iter()

			continue
		}

		item.ItemPinned = pinned

		switch navtype {
		case NavigationTypeData:
			if !filterMatch(filters, item) {
				continue
			}

			relations = append(relations, item)
		case NavigationTypeFirst:
			break loop
		case NavigationTypeOlder:
			err = iter.Close()
			if err != nil {
				return nil, err
			}

			q.Release()

			q = repo.DB.QueryContext(ctx, queryTimelineList)

			q.Bind(
				GenerationBucket{
					Generation: gs.Current,
					Bucket:     navbucket,
				},
				key,
				typ,
				FlakeID(reference),
			)

			iter = q.Iter()
		case NavigationTypeNewer:
			continue
		}

		if len(relations) >= limit {
			relations, err = repo.filterLists(
				ctx,
				authentication,
				outbox,
				follower,
				filters,
				relations,
			)
			if err != nil {
				return nil, err
			}

			if len(relations) >= limit {
				return
			}
		}
	}

	relations, err = repo.filterLists(
		ctx,
		authentication,
		outbox,
		follower,
		filters,
		relations,
	)
	if err != nil {
		return nil, err
	}

	return
}

func (repo *repositoryImpl) filterLists(
	ctx context.Context,
	authentication vocab.IRI,
	outbox, follower bool,
	filters *model.Filters,
	relations []*model.TimelineItem,
) (res []*model.TimelineItem, err error) {
	var types []model.CollectionType

	if !filters.Blocked {
		types = append(types, model.CollectionTypeBlocks)
	}

	if !filters.Muted {
		types = append(types, model.CollectionTypeMutes)
	}

	if len(types) == 0 {
		return relations, nil
	}

	var targets sorted.IRISet

	for _, item := range relations {
		if item.ItemContext != "" {
			targets.Add(item.ItemContext)
		}

		if item.ActivityActor != "" {
			targets.Add(item.ActivityActor)
		}

		if item.ItemActor != "" {
			targets.Add(item.ItemActor)
		}
	}

	skip, err := repo.CollectionItemContains(
		ctx,
		[]vocab.IRI{authentication},
		types,
		targets,
	)
	if err != nil {
		return nil, err
	}

	for _, item := range relations {
		if outbox {
			if (item.ItemVisibility == vocab.VisibilityPrivate && !follower) ||
				item.ItemVisibility == vocab.VisibilityDirect && !item.Recipients.Contains(authentication) {
				continue
			}
		}

		if _, ok := skip[item.ItemContext]; ok {
			continue
		}

		if _, ok := skip[item.ActivityActor]; ok {
			continue
		}

		if _, ok := skip[item.ItemActor]; ok {
			continue
		}

		res = append(res, item)
	}

	return
}

func filterMatch(
	filters *model.Filters,
	item *model.TimelineItem,
) bool {
	if item.ItemShare && filters.Share == model.InclusionExclude ||
		!item.ItemShare && filters.Share == model.InclusionRequire {
		return false
	}

	if item.ItemReply && filters.Reply == model.InclusionExclude ||
		!item.ItemReply && filters.Reply == model.InclusionRequire {
		return false
	}

	if item.ItemMedia && filters.Media == model.InclusionExclude ||
		!item.ItemMedia && filters.Media == model.InclusionRequire {
		return false
	}

	if item.ItemTags && filters.Tags == model.InclusionExclude ||
		!item.ItemTags && filters.Tags == model.InclusionRequire {
		return false
	}

	return true
}
