package cassandra

import (
	"context"
	"errors"

	"github.com/gocql/gocql"
	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"
	"gitlab.eientei.org/eientei/teruyo/internal/model"
	"gitlab.eientei.org/eientei/teruyo/internal/sorted"
)

const queryActorSaveLocal = `
insert into actors(
  genbucket,
  key,
  actor,
  data,
  id,
  email,
  username,
  password_hash,
  private_key,
  settings,
  attributes,
  followers_count,
  following_count,
  confirmed,
  locked,
  created,
  updated,
  posted,
  seen
) values (
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
)
`

const queryActorSaveFederated = `
insert into actors(
  genbucket,
  key,
  actor,
  data,
  id,
  followers_count,
  following_count,
  created,
  updated
) values (
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
)
`

func actorFieldsLocal(ctx context.Context, relation *model.Actor, extra ...interface{}) []interface{} {
	return append(
		extra,
		&relation.Actor,
		VocabItem{
			Item:    &relation.Data,
			Context: ctx,
		},
		(*FlakeID)(&relation.ID),
		&relation.Email,
		&relation.Username,
		&relation.PasswordHash,
		&relation.PrivateKey,
		&relation.Settings,
		&relation.Attributes,
		&relation.FollowersCount,
		&relation.FollowingCount,
		&relation.Confirmed,
		&relation.Locked,
		&relation.Created,
		&relation.Updated,
		&relation.Posted,
		&relation.Seen,
	)
}

func actorFieldsFederated(ctx context.Context, relation *model.Actor, extra ...interface{}) []interface{} {
	return append(
		extra,
		&relation.Actor,
		VocabItem{
			Item:    &relation.Data,
			Context: ctx,
		},
		(*FlakeID)(&relation.ID),
		&relation.FollowersCount,
		&relation.FollowingCount,
		&relation.Created,
		&relation.Updated,
	)
}

func (repo *repositoryImpl) ActorSave(
	ctx context.Context,
	actor *model.Actor,
) (err error) {
	defer repo.Trace(&ctx, &err)()

	key := actor.Actor.Domain()

	b := repo.DB.NewBatchContext(ctx, gocql.LoggedBatch)

	gs, init, err := repo.GenerationHashBuckets(
		ctx,
		RepositoryTypeActors,
		[]vocab.IRI{key},
		[]model.CollectionType{model.CollectionTypeActors},
		[]vocab.IRI{actor.Actor},
		true,
	)
	if err != nil {
		return err
	}

	if len(gs) == 0 {
		return nil
	}

	if init {
		b.Query(
			queryGenerationHashedInit,
			RepositoryTypeActors,
			key,
			model.CollectionTypeActors,
			gs[0].Generation,
			gs[0].Generation,
		)
	}

	for _, g := range gs {
		if actor.Actor.IsLocal() {
			b.Query(queryActorSaveLocal, actorFieldsLocal(ctx, actor, g, key)...)
		} else {
			b.Query(queryActorSaveFederated, actorFieldsFederated(ctx, actor, g, key)...)
		}
	}

	err = repo.DB.ExecuteBatch(b)
	if err != nil {
		return err
	}

	return nil
}

const queryActorGetFederatedByID = `
select
  actor,
  data,
  id,
  followers_count,
  following_count,
  created,
  updated
from actors where
  genbucket in ? and
  key in ? and
  actor in ?
`

func (repo *repositoryImpl) ActorGetFederatedByID(
	ctx context.Context,
	actorID vocab.IRI,
) (*model.Actor, error) {
	actors, err := repo.ActorGetFederatedByIDs(ctx, actorID)
	if errors.Is(err, gocql.ErrNotFound) {
		err = nil
	}

	if err != nil {
		return nil, err
	}

	if len(actors) > 0 {
		return actors[0], nil
	}

	return nil, nil
}

func (repo *repositoryImpl) ActorGetFederatedByIDs(
	ctx context.Context,
	actorIDs ...vocab.IRI,
) (actors []*model.Actor, err error) {
	defer repo.Trace(&ctx, &err)()

	var keys sorted.IRISet

	var elements sorted.IRISet

	for _, actorID := range actorIDs {
		keys.Add(actorID.Domain())
		elements.Add(actorID)
	}

	genbuckets, _, err := repo.GenerationHashBuckets(
		ctx,
		RepositoryTypeActors,
		keys,
		[]model.CollectionType{model.CollectionTypeActors},
		elements,
		false,
	)

	if err != nil {
		return nil, err
	}

	if len(genbuckets) == 0 {
		return nil, nil
	}

	q := repo.DB.QueryContext(
		ctx,
		queryActorGetFederatedByID,
		genbuckets,
		keys,
		elements,
	)

	defer q.Release()

	scan := q.Iter().Scanner()

	for scan.Next() {
		it := &model.Actor{}

		err = scan.Scan(actorFieldsFederated(ctx, it)...)
		if err != nil {
			_ = scan.Err()

			return nil, err
		}

		actors = append(actors, it)
	}

	err = scan.Err()
	if err != nil {
		return nil, err
	}

	return
}

const queryActorGetLocalByID = `
select
  actor,
  data,
  id,
  email,
  username,
  password_hash,
  private_key,
  settings,
  attributes,
  followers_count,
  following_count,
  confirmed,
  locked,
  created,
  updated,
  posted,
  seen
from actors where
  genbucket in ? and
  key in ? and
  actor in ?
`

func (repo *repositoryImpl) ActorGetLocalByID(
	ctx context.Context,
	actorID vocab.IRI,
) (*model.Actor, error) {
	actors, err := repo.ActorGetLocalByIDs(ctx, actorID)
	if errors.Is(err, gocql.ErrNotFound) {
		err = nil
	}

	if err != nil {
		return nil, err
	}

	if len(actors) > 0 {
		return actors[0], nil
	}

	return nil, nil
}

func (repo *repositoryImpl) ActorGetLocalByIDs(
	ctx context.Context,
	actorIDs ...vocab.IRI,
) (actors []*model.Actor, err error) {
	defer repo.Trace(&ctx, &err)()

	var keys sorted.IRISet

	var elements sorted.IRISet

	for _, actorID := range actorIDs {
		keys.Add(actorID.Domain())
		elements.Add(actorID)
	}

	genbuckets, _, err := repo.GenerationHashBuckets(
		ctx,
		RepositoryTypeActors,
		keys,
		[]model.CollectionType{model.CollectionTypeActors},
		elements,
		false,
	)

	if err != nil {
		return nil, err
	}

	if len(genbuckets) == 0 {
		return nil, nil
	}

	q := repo.DB.QueryContext(
		ctx,
		queryActorGetLocalByID,
		genbuckets,
		keys,
		elements,
	)

	defer q.Release()

	scan := q.Iter().Scanner()

	for scan.Next() {
		it := &model.Actor{}

		err = scan.Scan(actorFieldsLocal(ctx, it)...)
		if err != nil {
			_ = scan.Err()

			return nil, err
		}

		actors = append(actors, it)
	}

	err = scan.Err()
	if err != nil {
		return nil, err
	}

	return
}

//nolint:revive // temporary
func (repo *repositoryImpl) ActorUpdateFollowers(
	ctx context.Context,
	actorID vocab.IRI,
	followers int,
) error {
	return nil
}

//nolint:revive // temporary
func (repo *repositoryImpl) ActorUpdateFollowing(
	ctx context.Context,
	actorID vocab.IRI,
	following int,
) error {
	return nil
}

//nolint:revive // temporary
func (repo *repositoryImpl) ActorUpdateSettings(
	ctx context.Context,
	actorID vocab.IRI,
	settings map[string]interface{},
) error {
	return nil
}

//nolint:revive // temporary
func (repo *repositoryImpl) ActorUpdateAttributes(
	ctx context.Context,
	actorID vocab.IRI,
	attributes map[string]interface{},
) error {
	return nil
}
