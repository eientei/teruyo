package cassandra

import (
	"context"
	"testing"
	"time"

	"gitlab.eientei.org/eientei/teruyo/internal/persistence/cassandra"

	"gitlab.eientei.org/eientei/teruyo/internal/tracer"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/propagation"

	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"

	"github.com/gocql/gocql"
	"github.com/stretchr/testify/suite"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
	"gitlab.eientei.org/eientei/teruyo/internal/flake"
)

const (
	objectBar = vocab.DIDAPNodeLocal + "/objects/bar"
	objectBaz = vocab.DIDAPNodeLocal + "/objects/baz"
)

type CassandraSuite struct {
	suite.Suite
	suitectx       context.Context
	suitecancel    context.CancelFunc
	ctx            context.Context
	cancel         context.CancelFunc
	tracerShutdown func()
	scylla         testcontainers.Container
	cluster        *gocql.ClusterConfig
	config         *Config
	repo           *repositoryImpl
}

func (s *CassandraSuite) SetupSuite() {
	s.suitectx, s.suitecancel = context.WithCancel(context.Background())
	req := testcontainers.ContainerRequest{
		FromDockerfile: testcontainers.FromDockerfile{},
		Image:          "scylladb/scylla:latest",
		ExposedPorts:   []string{"9042/tcp"},
		Cmd:            []string{"--skip-wait-for-gossip-to-settle", "0"},
		WaitingFor:     wait.ForListeningPort("9042/tcp"),
	}

	var err error

	s.scylla, err = testcontainers.GenericContainer(s.suitectx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	})
	s.NoError(err)

	host, err := s.scylla.ContainerIP(s.suitectx)
	s.NoError(err)

	s.cluster = gocql.NewCluster(host)
	s.cluster.Keyspace = "teruyo_test"

	globalTracer, tracerShutdown, err := tracer.New("teruyo-tests")
	if err != nil {
		panic(err)
	}

	s.tracerShutdown = tracerShutdown

	otel.SetTracerProvider(globalTracer)
	otel.SetTextMapPropagator(
		propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}, propagation.Baggage{}),
	)

	_, cs := globalTracer.Tracer("foo").Start(context.Background(), "bar")

	cs.End()

	s.config = &Config{
		Generator:                   flake.New(1),
		ClusterConfig:               *s.cluster,
		TracerProvider:              globalTracer,
		Replication:                 "",
		InitialGenerationHash:       1,
		InitialGenerationTime:       time.Hour * 24 * 30,
		UpgradeBatchSizeCollections: 100,
		UpgradeBatchSizeItems:       100,
		UpgradeBatchSizeTimelines:   100,
	}

	repo, err := New(s.config)
	s.NoError(err)

	s.repo, _ = repo.(*repositoryImpl)

	err = cassandra.DropKeyspace(s.suitectx, s.cluster, s.cluster.Keyspace)
	s.NoError(err)

	err = s.repo.Migrate(s.suitectx)
	s.NoError(err)
}

func (s *CassandraSuite) SetupTest() {
	s.ctx, s.cancel = context.WithCancel(s.suitectx)
}

func (s *CassandraSuite) TearDownTest() {
	s.cancel()
}

func (s *CassandraSuite) TearDownSuite() {
	err := s.scylla.Terminate(s.suitectx)
	s.NoError(err)

	s.tracerShutdown()

	s.suitecancel()
}

func TestExampleTestSuite(t *testing.T) {
	suite.Run(t, &CassandraSuite{})
}
