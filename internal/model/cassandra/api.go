package cassandra

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"time"

	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"

	"gitlab.eientei.org/eientei/teruyo/internal/byteops"

	"github.com/gocql/gocql"
	"gitlab.eientei.org/eientei/teruyo/internal/flake"
	"gitlab.eientei.org/eientei/teruyo/internal/model"
	"gitlab.eientei.org/eientei/teruyo/internal/model/cassandra/migration"
	"gitlab.eientei.org/eientei/teruyo/internal/persistence"
	"gitlab.eientei.org/eientei/teruyo/internal/persistence/cassandra"
	"go.opentelemetry.io/otel/trace"
)

// RepositoryType enum
type RepositoryType byte

// Repository types
const (
	RepositoryTypeTimelines   = 1
	RepositoryTypeItems       = 3
	RepositoryTypeCollections = 2
	RepositoryTypeActors      = 4
)

// NavigationType aids navigation in time-bucketed timeline collection
type NavigationType int32

const (
	// NavigationTypeData (zero-value default) indicates data entry, not a navigation aid
	NavigationTypeData NavigationType = 0
	// NavigationTypeOlder indicates older bucket id
	NavigationTypeOlder NavigationType = 1
	// NavigationTypeNewer indicates newer bucket id
	NavigationTypeNewer NavigationType = 2
	// NavigationTypeFirst indicates there is no older buckets
	NavigationTypeFirst NavigationType = 3
)

// Generation represents the subdivision of ID space (buckets).
// When new pending generation is added, all entries from previous generation must be gradually migrated and while
// migration is in progress, all mutation must be duplicated for current and pending generations.
//
// for hashed collections:
// Generation N means entire hash space is split between 2^N buckets
// for timed collections:
// Generation N means time since unix epoch is split between periods of N milliseconds
type Generation struct {
	Current           uint64
	Pending           uint64
	CurrentLastBucket uint64
	PendingLastBucket uint64
}

// GenerationBucket 128-bit representation of generation with bucket
type GenerationBucket struct {
	Generation uint64
	Bucket     uint64
	Last       uint64
}

// MarshalCQL implementation
func (v GenerationBucket) MarshalCQL(_ gocql.TypeInfo) ([]byte, error) {
	return byteops.UInt128Marshal(v.Generation, v.Bucket), nil
}

// UnmarshalCQL implementation
func (v *GenerationBucket) UnmarshalCQL(_ gocql.TypeInfo, bs []byte) (err error) {
	v.Generation, v.Bucket, err = byteops.UInt128Unmarshal(bs)

	return
}

// FlakeID wrapper
type FlakeID flake.ID

// MarshalCQL implementation
func (v FlakeID) MarshalCQL(_ gocql.TypeInfo) ([]byte, error) {
	return byteops.UInt128Marshal(v.Time, v.Instance), nil
}

// UnmarshalCQL implementation
func (v *FlakeID) UnmarshalCQL(_ gocql.TypeInfo, bs []byte) (err error) {
	v.Time, v.Instance, err = byteops.UInt128Unmarshal(bs)

	return
}

// VocabItem wrapper
type VocabItem struct {
	Item    *vocab.Item
	Context context.Context
}

// MarshalCQL implementation
func (v VocabItem) MarshalCQL(_ gocql.TypeInfo) ([]byte, error) {
	return vocab.Marshal(v.Context, *v.Item)
}

// UnmarshalCQL implementation
func (v VocabItem) UnmarshalCQL(_ gocql.TypeInfo, bs []byte) (err error) {
	*v.Item, err = vocab.Unmarshal(v.Context, bs)

	return
}

// ReactionList wrapper
type ReactionList []*model.Reaction

func marshalSize(buf *bytes.Buffer, l int) {
	_ = buf.WriteByte(byte(l >> 24))
	_ = buf.WriteByte(byte(l >> 16))
	_ = buf.WriteByte(byte(l >> 8))
	_ = buf.WriteByte(byte(l))
}

func unmarshalSize(bs []byte) (r int, buf []byte, err error) {
	if len(bs) < 4 {
		return 0, nil, io.EOF
	}

	r = int(bs[0])<<24 | int(bs[1])<<16 | int(bs[2])<<8 | int(bs[3])

	return r, bs[4:], nil
}

// MarshalCQL implementation
func (v ReactionList) MarshalCQL(_ gocql.TypeInfo) ([]byte, error) {
	buf := bytes.Buffer{}

	marshalSize(&buf, len(v))

	for _, el := range v {
		bs, err := json.Marshal(el)
		if err != nil {
			return nil, err
		}

		marshalSize(&buf, len(bs))

		_, _ = buf.Write(bs)
	}

	return buf.Bytes(), nil
}

// UnmarshalCQL implementation
func (v *ReactionList) UnmarshalCQL(_ gocql.TypeInfo, bs []byte) (err error) {
	s, bs, err := unmarshalSize(bs)
	if err != nil {
		return err
	}

	if *v == nil {
		*v = make(ReactionList, 0, s)
	} else {
		*v = (*v)[:0]
	}

	var l int

	var data []byte

	for i := 0; i < s; i++ {
		l, bs, err = unmarshalSize(bs)
		if err != nil {
			return err
		}

		data, bs = bs[:l], bs[l:]

		el := &model.Reaction{}

		err = json.Unmarshal(data, el)
		if err != nil {
			return err
		}

		*v = append(*v, el)
	}

	return
}

// GenerationTarget combined GenerationBucket with hashed target
type GenerationTarget struct {
	Target           string
	GenerationBucket GenerationBucket
}

// Repository combined repository
type Repository interface {
	model.ActorRepository
	model.ItemRepository
	model.CollectionRepository
	model.TimelineRepository
	Migrate(ctx context.Context) error
}

// Config repository config
type Config struct {
	Generator                   flake.Generator
	ClusterConfig               gocql.ClusterConfig
	TracerProvider              trace.TracerProvider
	Replication                 string
	InitialGenerationHash       uint64
	InitialGenerationTime       time.Duration
	UpgradeBatchSizeCollections int
	UpgradeBatchSizeItems       int
	UpgradeBatchSizeTimelines   int
}

type repositoryImpl struct {
	persistence.Tracer
	DB cassandra.Session
	Config
	InitialGenerationTime uint64
	timestamp             uint64
	machineID             uint64
}

// New returns new Repository instance
func New(config *Config) (Repository, error) {
	repo := &repositoryImpl{
		Tracer:                *persistence.NewTracerDB("cassandra", config.TracerProvider),
		Config:                *config,
		InitialGenerationTime: uint64(config.InitialGenerationTime.Milliseconds()),
		machineID:             uint64(config.Generator.MachineID()) << 6,
	}

	return repo, nil
}

// Migrate performs repository migration
func (repo *repositoryImpl) Migrate(ctx context.Context) error {
	driver, err := migration.NewSource()
	if err != nil {
		return err
	}

	repo.DB, err = cassandra.Migrate(
		ctx,
		&repo.ClusterConfig,
		"cassandra",
		repo.ClusterConfig.Keyspace,
		repo.Replication,
		driver,
		repo.TracerProvider,
	)
	if err != nil {
		return err
	}

	return nil
}
