package cassandra

import (
	"context"
	"math"

	"github.com/spaolacci/murmur3"
	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"
	"gitlab.eientei.org/eientei/teruyo/internal/model"
)

// GenerationHashBuckets returns hash-based buckets
func (repo *repositoryImpl) GenerationHashBuckets(
	ctx context.Context,
	repository RepositoryType,
	keys []vocab.IRI,
	types []model.CollectionType,
	elements []vocab.IRI,
	coalesce bool,
) (buckets []GenerationBucket, init bool, err error) {
	gs, err := repo.GenerationHashedGet(
		ctx,
		repository,
		keys,
		types,
	)
	if err != nil {
		return
	}

	if len(gs) == 0 && coalesce {
		g := repo.NextHashedGeneration(repo.InitialGenerationHash)

		gs = []*Generation{
			{
				Current: g,
				Pending: g,
			},
		}

		init = true
	}

	for _, g := range gs {
		for _, target := range elements {
			h := murmur3.Sum64(([]byte)(target))

			buckets = append(buckets, GenerationBucket{
				Generation: g.Current,
				Bucket:     h / (math.MaxUint64 / uint64(1<<(g.Current&0x3f))),
				Last:       h,
			})

			if g.Current != g.Pending {
				buckets = append(buckets, GenerationBucket{
					Generation: g.Pending,
					Bucket:     h / (math.MaxUint64 / uint64(1<<(g.Pending&0x3f))),
					Last:       h,
				})
			}
		}
	}

	return
}

// GenerationTimeBuckets returns time-based buckets
func (repo *repositoryImpl) GenerationTimeBuckets(
	ctx context.Context,
	repository RepositoryType,
	key vocab.IRI,
	typ model.TimelineType,
	target uint64,
	coalesce bool,
) (genbuckets []GenerationBucket, init bool, err error) {
	gs, err := repo.GenerationTimedGet(
		ctx,
		repository,
		key,
		typ,
	)
	if err != nil {
		return
	}

	if gs == nil {
		if !coalesce {
			return nil, false, nil
		}

		b := target / repo.InitialGenerationTime

		return []GenerationBucket{
			{
				Generation: repo.InitialGenerationTime,
				Bucket:     b,
				Last:       b,
			},
		}, true, nil
	}

	genbuckets = []GenerationBucket{
		{
			Generation: gs.Current,
			Bucket:     target / gs.Current,
			Last:       gs.CurrentLastBucket,
		},
	}

	if gs.Current != gs.Pending {
		genbuckets = append(genbuckets, GenerationBucket{
			Generation: gs.Pending,
			Bucket:     target / gs.Pending,
			Last:       gs.PendingLastBucket,
		})
	}

	return
}

// EnumerateHashBuckets enumerates all hash buckets for every distinct generation of supplied keys/types
func (repo *repositoryImpl) EnumerateHashBuckets(
	ctx context.Context,
	repository RepositoryType,
	keys []vocab.IRI,
	types []model.CollectionType,
) (buckets []GenerationBucket, err error) {
	gs, err := repo.GenerationHashedDistinctCurrent(
		ctx,
		repository,
		keys,
		types,
	)
	if err != nil {
		return
	}

	for _, g := range gs {
		for i := uint64(0); i < 1<<(g&0x3f); i++ {
			buckets = append(buckets, GenerationBucket{
				Generation: g,
				Bucket:     i,
			})
		}
	}

	return
}
