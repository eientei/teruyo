// Package migration provides migration files for cassandra/scylla database
package migration

import (
	"embed"
	"net/http"

	"github.com/golang-migrate/migrate/v4/source"
	"github.com/golang-migrate/migrate/v4/source/httpfs"
)

//go:embed *.cql
var fs embed.FS

// NewSource builds migrations source
func NewSource() (source.Driver, error) {
	return httpfs.New(http.FS(fs), ".")
}
