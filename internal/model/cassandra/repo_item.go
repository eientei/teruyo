package cassandra

import (
	"context"
	"errors"

	"github.com/gocql/gocql"
	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"
	"gitlab.eientei.org/eientei/teruyo/internal/model"
	"gitlab.eientei.org/eientei/teruyo/internal/sorted"
)

func itemFields(ctx context.Context, relation *model.Item, extra ...interface{}) []interface{} {
	return append(
		extra,
		&relation.Item,
		VocabItem{
			Item:    &relation.Data,
			Context: ctx,
		},
		(*FlakeID)(&relation.ID),
		&relation.Context,
		&relation.List,
		&relation.Visibility,
		&relation.Actor,
		&relation.ReplyToItem,
		&relation.ReplyToActor,
		&relation.Replies,
		&relation.Shares,
		&relation.Likes,
		&relation.Reactions,
		&relation.Sensitive,
		&relation.Pinned,
		&relation.Media,
		&relation.Tags,
		&relation.Recipients,
		&relation.Created,
		&relation.Updated,
	)
}

const queryItemUpdateReplies = `
update items set
  replies = ?
where
  genbucket in ? and
  key = ? and
  item = ?
`

const queryItemUpdateShares = `
update items set
  shares = ?
where
  genbucket in ? and
  key = ? and
  item = ?
`

const queryItemUpdateLikes = `
update items set
  likes = ?
where
  genbucket in ? and
  key = ? and
  item = ?
`

const queryItemUpdateReactions = `
update items set
  reactions = ?
where
  genbucket in ? and
  key = ? and
  item = ?
`

var queryItemUpdate = map[model.CollectionType]string{
	model.CollectionTypeReplies:   queryItemUpdateReplies,
	model.CollectionTypeShares:    queryItemUpdateShares,
	model.CollectionTypeLikes:     queryItemUpdateLikes,
	model.CollectionTypeReactions: queryItemUpdateReactions,
}

func (repo *repositoryImpl) itemUpdate(
	ctx context.Context,
	collection model.CollectionType,
	genbuckets []GenerationBucket,
	key vocab.IRI,
	itemID vocab.IRI,
) error {
	counts, err := repo.CollectionItemCountBySources(
		ctx,
		[]vocab.IRI{itemID},
		[]model.CollectionType{collection},
	)
	if err != nil {
		return err
	}

	liked := counts[model.CollectionTypeSource{Key: itemID, Type: collection}]

	q := repo.DB.QueryContext(
		ctx,
		queryItemUpdate[collection],
		liked,
		genbuckets,
		key,
		itemID,
	)

	defer q.Release()

	return q.Exec()
}

/*
func (repo *repositoryImpl) itemCollectionAdd(
	ctx context.Context,
	collection model.CollectionType,
	bidirectional bool,
	itemID, actorID vocab.IRI,
	data vocab.Item,
) (err error) {
	defer repo.Trace(&ctx, &err)()

	key := itemID.Domain()

	genbuckets, _, err := repo.GenerationHashBuckets(
		ctx,
		RepositoryTypeItems,
		[]vocab.IRI{key},
		[]model.CollectionType{model.CollectionTypeItems},
		[]vocab.IRI{itemID},
		false,
	)
	if err != nil {
		return err
	}

	if len(genbuckets) == 0 {
		return nil
	}

	if bidirectional {
		b := repo.DB.NewBatchContext(ctx, gocql.LoggedBatch)

		err = repo.collectionItemSaveBatch(
			ctx,
			&model.CollectionItem{
				Created: time.Now(),
				Key:     itemID,
				Type:    collection,
				Element: actorID,
				Data:    data,
			},
			b,
		)
		if err != nil {
			return err
		}

		err = repo.collectionItemSaveBatch(
			ctx,
			&model.CollectionItem{
				Created: time.Now(),
				Key:     actorID,
				Type:    collection,
				Element: itemID,
				Data:    data,
			},
			b,
		)
		if err != nil {
			return err
		}

		err = repo.DB.ExecuteBatch(b)
		if err != nil {
			return err
		}
	} else {
		err = repo.CollectionItemSave(
			ctx,
			&model.CollectionItem{
				Created: time.Now(),
				Key:     itemID,
				Type:    collection,
				Element: actorID,
				Data:    data,
			},
		)
		if err != nil {
			return err
		}
	}

	return repo.itemUpdate(ctx, collection, genbuckets, key, itemID)
}

func (repo *repositoryImpl) itemCollectionRemove(
	ctx context.Context,
	collection model.CollectionType,
	bidirectional bool,
	itemID, actorID vocab.IRI,
) (err error) {
	defer repo.Trace(&ctx, &err)()

	key := itemID.Domain()

	genbuckets, _, err := repo.GenerationHashBuckets(
		ctx,
		RepositoryTypeItems,
		[]vocab.IRI{key},
		[]model.CollectionType{model.CollectionTypeItems},
		[]vocab.IRI{itemID},
		false,
	)
	if err != nil {
		return err
	}

	if len(genbuckets) == 0 {
		return nil
	}

	if bidirectional {
		b := repo.DB.NewBatchContext(ctx, gocql.LoggedBatch)

		err = repo.collectionItemRemoveBatch(
			ctx,
			itemID,
			collection,
			actorID,
			b,
		)
		if err != nil {
			return err
		}

		err = repo.collectionItemRemoveBatch(
			ctx,
			actorID,
			collection,
			itemID,
			b,
		)
		if err != nil {
			return err
		}

		err = repo.DB.ExecuteBatch(b)
		if err != nil {
			return err
		}
	} else {
		err = repo.CollectionItemRemove(ctx, itemID, collection, actorID)
		if err != nil {
			return err
		}
	}

	return repo.itemUpdate(ctx, collection, genbuckets, key, itemID)
}

*/

const queryItemSave = `
insert into items(
  genbucket,
  key,
  item,
  data,
  id,
  context,
  list,
  visibility,
  actor,
  reply_to_item,
  reply_to_actor,
  replies,
  shares,
  likes,
  reactions,
  sensitive,
  pinned,
  media,
  tags,
  recipients,
  created,
  updated
) values (
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
)
`

/*
func (repo *repositoryImpl) itemUpdateCounters(ctx context.Context, item *model.Item) error {
	counts, err := repo.CollectionItemCountBySources(
		ctx,
		[]vocab.IRI{item.Item},
		[]model.CollectionType{
			model.CollectionTypeReplies,
			model.CollectionTypeShares,
			model.CollectionTypeLikes,
			model.CollectionTypeReactions,
		},
	)
	if err != nil {
		return err
	}

	item.Replies = counts[model.CollectionTypeSource{Key: item.Item, Type: model.CollectionTypeReplies}]
	item.Shares = counts[model.CollectionTypeSource{Key: item.Item, Type: model.CollectionTypeShares}]
	item.Likes = counts[model.CollectionTypeSource{Key: item.Item, Type: model.CollectionTypeLikes}]
	item.Reactions = counts[model.CollectionTypeSource{Key: item.Item, Type: model.CollectionTypeReactions}]

	return nil
}

*/

func (repo *repositoryImpl) ItemSave(
	ctx context.Context,
	item *model.Item,
) (err error) {
	defer repo.Trace(&ctx, &err)()

	key := item.Item.Domain()

	b := repo.DB.NewBatchContext(ctx, gocql.LoggedBatch)

	gs, init, err := repo.GenerationHashBuckets(
		ctx,
		RepositoryTypeItems,
		[]vocab.IRI{key},
		[]model.CollectionType{model.CollectionTypeItems},
		[]vocab.IRI{item.Item},
		true,
	)
	if err != nil {
		return err
	}

	if len(gs) == 0 {
		return nil
	}

	if init {
		b.Query(
			queryGenerationHashedInit,
			RepositoryTypeItems,
			key,
			model.CollectionTypeItems,
			gs[0].Generation,
			gs[0].Generation,
		)
	}

	for _, g := range gs {
		b.Query(queryItemSave, itemFields(ctx, item, g, key)...)
	}

	err = repo.DB.ExecuteBatch(b)
	if err != nil {
		return err
	}

	return nil
}

const queryItemGetByID = `
select
  item,
  data,
  id,
  context,
  list,
  visibility,
  actor,
  reply_to_item,
  reply_to_actor,
  replies,
  shares,
  likes,
  reactions,
  sensitive,
  pinned,
  media,
  tags,
  recipients,
  created,
  updated
from items where
  genbucket in ? and
  key in ? and
  item in ?
`

func (repo *repositoryImpl) ItemGetByID(
	ctx context.Context,
	itemID vocab.IRI,
) (item *model.Item, err error) {
	items, err := repo.ItemGetByIDs(ctx, itemID)
	if errors.Is(err, gocql.ErrNotFound) {
		err = nil
	}

	if err != nil {
		return nil, err
	}

	if len(items) > 0 {
		return items[0], nil
	}

	return nil, nil
}

func (repo *repositoryImpl) ItemGetByIDs(
	ctx context.Context,
	itemIDs ...vocab.IRI,
) (items []*model.Item, err error) {
	defer repo.Trace(&ctx, &err)()

	var keys sorted.IRISet

	var elements sorted.IRISet

	for _, itemID := range itemIDs {
		keys.Add(itemID.Domain())
		elements.Add(itemID)
	}

	genbuckets, _, err := repo.GenerationHashBuckets(
		ctx,
		RepositoryTypeItems,
		keys,
		[]model.CollectionType{model.CollectionTypeItems},
		elements,
		false,
	)
	if err != nil {
		return nil, err
	}

	if len(genbuckets) == 0 {
		return nil, nil
	}

	q := repo.DB.QueryContext(
		ctx,
		queryItemGetByID,
		genbuckets,
		keys,
		elements,
	)

	defer q.Release()

	scan := q.Iter().Scanner()

	for scan.Next() {
		it := &model.Item{}

		err = scan.Scan(itemFields(ctx, it)...)
		if err != nil {
			_ = scan.Err()

			return nil, err
		}

		items = append(items, it)
	}

	err = scan.Err()
	if err != nil {
		return nil, err
	}

	return
}

func (repo *repositoryImpl) ItemUpdateReplies(
	ctx context.Context,
	itemID vocab.IRI,
) (err error) {
	defer repo.Trace(&ctx, &err)()

	key := itemID.Domain()

	genbuckets, _, err := repo.GenerationHashBuckets(
		ctx,
		RepositoryTypeItems,
		[]vocab.IRI{key},
		[]model.CollectionType{model.CollectionTypeItems},
		[]vocab.IRI{itemID},
		false,
	)
	if err != nil {
		return err
	}

	if len(genbuckets) == 0 {
		return nil
	}

	return repo.itemUpdate(ctx, model.CollectionTypeReplies, genbuckets, key, itemID)
}

//nolint:revive // temporary
func (repo *repositoryImpl) ItemList(
	ctx context.Context,
	authentication *model.Actor,
	filters *model.Filters,
	typ model.TimelineType,
	actor, reference vocab.IRI,
	limit int,
) (relations []*model.Item, err error) {
	return nil, nil
}
