package cassandra

import (
	"context"
	"math"
	"time"

	"gitlab.eientei.org/eientei/teruyo/internal/persistence/cassandra"

	"github.com/gocql/gocql"
	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"
	"gitlab.eientei.org/eientei/teruyo/internal/model"
)

const queryCollectionItemSave = `
insert into collections(
  genbucket,
  hash,
  key,
  type,
  element,
  data,
  created
) values (
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
)
`

func collectionFields(ctx context.Context, collection *model.CollectionItem, extra ...interface{}) []interface{} {
	return append(
		extra,
		&collection.Key,
		&collection.Type,
		&collection.Element,
		VocabItem{
			Item:    &collection.Data,
			Context: ctx,
		},
		&collection.Created,
	)
}

func (repo *repositoryImpl) CollectionItemSave(
	ctx context.Context,
	collection *model.CollectionItem,
) (err error) {
	defer repo.Trace(&ctx, &err)()

	b := repo.DB.NewBatchContext(ctx, gocql.LoggedBatch)

	err = repo.collectionItemSaveBatch(ctx, collection, b)
	if err != nil {
		return err
	}

	return repo.DB.ExecuteBatch(b)
}

func (repo *repositoryImpl) collectionItemSaveBatch(
	ctx context.Context,
	collection *model.CollectionItem,
	b cassandra.Batch,
) (err error) {
	if collection.Created.IsZero() {
		collection.Created = time.Now()
	}

	generations, init, err := repo.GenerationHashBuckets(
		ctx,
		RepositoryTypeCollections,
		[]vocab.IRI{collection.Key},
		[]model.CollectionType{collection.Type},
		[]vocab.IRI{collection.Element},
		true,
	)
	if err != nil {
		return err
	}

	for _, g := range generations {
		b.Query(
			queryCollectionItemSave,
			collectionFields(ctx, collection, &g, g.Last)...,
		)
	}

	if init {
		b.Query(
			queryGenerationHashedInit,
			RepositoryTypeCollections,
			collection.Key,
			collection.Type,
			generations[0].Generation,
			generations[0].Generation,
		)
	}

	return nil
}

const queryCollectionItemRemove = `
delete from collections where
  genbucket in ? and
  key = ? and
  type = ? and
  element in ?
`

func (repo *repositoryImpl) CollectionItemRemove(
	ctx context.Context,
	key vocab.IRI,
	typ model.CollectionType,
	elements ...vocab.IRI,
) (err error) {
	defer repo.Trace(&ctx, &err)()

	genbuckets, _, err := repo.GenerationHashBuckets(
		ctx,
		RepositoryTypeCollections,
		[]vocab.IRI{key},
		[]model.CollectionType{typ},
		elements,
		false,
	)
	if err != nil {
		return err
	}

	q := repo.DB.QueryContext(
		ctx,
		queryCollectionItemRemove,
		genbuckets,
		key,
		typ,
		elements,
	)

	defer q.Release()

	return q.Exec()
}

/*
func (repo *repositoryImpl) collectionItemRemoveBatch(
	ctx context.Context,
	key vocab.IRI,
	typ model.CollectionType,
	element vocab.IRI,
	b cassandra.Batch,
) (err error) {
	genbuckets, _, err := repo.GenerationHashBuckets(
		ctx,
		RepositoryTypeCollections,
		[]vocab.IRI{key},
		[]model.CollectionType{typ},
		[]vocab.IRI{element},
		false,
	)
	if err != nil {
		return err
	}

	b.Query(
		queryCollectionItemRemove,
		genbuckets,
		key,
		typ,
		element,
	)

	return nil
}

*/

const queryCollectionItemList = `
select
  key,
  type,
  element,
  data,
  created
from collections where
  genbucket in ? and
  key in ? and
  type in ?
`

func (repo *repositoryImpl) CollectionItemList(
	ctx context.Context,
	keys []vocab.IRI,
	types []model.CollectionType,
) (collections []*model.CollectionItem, err error) {
	defer repo.Trace(&ctx, &err)()

	genbuckets, err := repo.EnumerateHashBuckets(ctx, RepositoryTypeCollections, keys, types)
	if err != nil {
		return nil, err
	}

	q := repo.DB.QueryContext(
		ctx,
		queryCollectionItemList,
		genbuckets,
		keys,
		types,
	)

	defer q.Release()

	scan := q.Iter().Scanner()

	for scan.Next() {
		collection := &model.CollectionItem{}

		err = scan.Scan(collectionFields(ctx, collection)...)
		if err != nil {
			_ = scan.Err()

			return nil, err
		}

		collections = append(collections, collection)
	}

	err = scan.Err()
	if err != nil {
		return nil, err
	}

	return
}

const queryCollectionItemContains = `
select
  element
from collections where
  genbucket in ? and
  key in ? and
  type in ? and
  element in ?
`

func (repo *repositoryImpl) CollectionItemContains(
	ctx context.Context,
	keys []vocab.IRI,
	types []model.CollectionType,
	elements []vocab.IRI,
) (foundtargets map[vocab.IRI]struct{}, err error) {
	defer repo.Trace(&ctx, &err)()

	genbuckets, _, err := repo.GenerationHashBuckets(ctx, RepositoryTypeCollections, keys, types, elements, false)
	if err != nil {
		return nil, err
	}

	q := repo.DB.QueryContext(
		ctx,
		queryCollectionItemContains,
		genbuckets,
		keys,
		types,
		elements,
	)

	defer q.Release()

	scan := q.Iter().Scanner()

	foundtargets = make(map[vocab.IRI]struct{})

	for scan.Next() {
		var match vocab.IRI

		err = scan.Scan(
			&match,
		)
		if err != nil {
			_ = scan.Err()

			return nil, err
		}

		foundtargets[match] = struct{}{}
	}

	err = scan.Err()
	if err != nil {
		return nil, err
	}

	return
}

const queryCollectionItemCountBySource = `
select
  key,
  type,
  count(*)
from collections where
  genbucket in ? and
  key in ? and
  type in ?
group by genbucket, key, type
`

func (repo *repositoryImpl) CollectionItemCountBySources(
	ctx context.Context,
	keys []vocab.IRI,
	types []model.CollectionType,
) (total map[model.CollectionTypeSource]int64, err error) {
	defer repo.Trace(&ctx, &err)()

	genbuckets, err := repo.EnumerateHashBuckets(ctx, RepositoryTypeCollections, keys, types)
	if err != nil {
		return nil, err
	}

	total = make(map[model.CollectionTypeSource]int64)

	q := repo.DB.QueryContext(
		ctx,
		queryCollectionItemCountBySource,
		genbuckets,
		keys,
		types,
	)

	defer q.Release()

	scan := q.Iter().Scanner()

	for scan.Next() {
		var key model.CollectionTypeSource

		var count int64

		err = scan.Scan(
			&key.Key,
			&key.Type,
			&count,
		)
		if err != nil {
			_ = scan.Err()

			return nil, err
		}

		total[model.CollectionTypeSource{
			Key:  key.Key,
			Type: key.Type,
		}] += count
	}

	err = scan.Err()
	if err != nil {
		return nil, err
	}

	return
}

const queryCollectionItemListHash = `
select
  genbucket,
  hash,
  key,
  type,
  element,
  data,
  created
from collections where
  genbucket in ? and
  key in ? and
  type in ?
`

const queryCollectionItemRemoveRange = `
delete from collections where
  genbucket in ? and
  key = ? and
  type = ?
`

func (repo *repositoryImpl) CollectionItemUpgradeGeneration(
	ctx context.Context,
	key vocab.IRI,
	typ model.CollectionType,
) (err error) {
	defer repo.Trace(&ctx, &err)()

	gs, err := repo.GenerationHashedGet(
		ctx,
		RepositoryTypeCollections,
		[]vocab.IRI{key},
		[]model.CollectionType{typ},
	)
	if err != nil {
		return
	}

	if len(gs) == 0 {
		return nil
	}

	g := gs[0]

	var nextgen uint64

	if g.Current != g.Pending {
		nextgen = g.Pending
	} else {
		nextgen = repo.NextHashedGeneration((g.Current & 0x3f) + 1)
		err = repo.GenerationUpdatePending(ctx, RepositoryTypeCollections, string(key), int(typ), nextgen)
		if err != nil {
			return err
		}
	}

	return repo.CollectionItemMigrateGeneration(ctx, key, typ, g.Current, nextgen)
}

func (repo *repositoryImpl) CollectionItemMigrateGeneration(
	ctx context.Context,
	key vocab.IRI,
	typ model.CollectionType,
	current, next uint64,
) (err error) {
	var buckets []GenerationBucket

	for i := 0; i < 1<<(current&0x3f); i++ {
		buckets = append(buckets, GenerationBucket{
			Generation: current,
			Bucket:     uint64(i),
		})
	}

	q := repo.DB.QueryContext(
		ctx,
		queryCollectionItemListHash,
		buckets,
		[]vocab.IRI{key},
		[]model.CollectionType{typ},
	)

	defer q.Release()

	scan := q.Iter().Scanner()

	b := repo.DB.NewBatchContext(ctx, gocql.UnloggedBatch)

	var lastb GenerationBucket

	for scan.Next() {
		var lb GenerationBucket

		collection := &model.CollectionItem{}

		var hash uint64

		err = scan.Scan(collectionFields(ctx, collection, &lb, &hash)...)
		if err != nil {
			_ = scan.Err()

			return err
		}

		if lastb != lb || len(b.BatchEntries()) > repo.UpgradeBatchSizeCollections {
			lastb = lb

			if len(b.BatchEntries()) > 0 {
				err = repo.DB.ExecuteBatch(b)
				if err != nil {
					return err
				}

				b = repo.DB.NewBatch(gocql.UnloggedBatch)
			}
		}

		b.Query(
			queryCollectionItemSave,
			collectionFields(
				ctx,
				collection,
				GenerationBucket{
					Generation: next,
					Bucket:     hash / (math.MaxUint64 / uint64(1<<(next&0x3f))),
				},
				hash,
			)...,
		)
	}

	err = scan.Err()
	if err != nil {
		return err
	}

	if len(b.BatchEntries()) > 0 {
		err = repo.DB.ExecuteBatch(b)
		if err != nil {
			return err
		}
	}

	err = repo.GenerationUpdateCurrent(ctx, RepositoryTypeCollections, string(key), int(typ), next)
	if err != nil {
		return err
	}

	qd := repo.DB.QueryContext(
		ctx,
		queryCollectionItemRemoveRange,
		buckets,
		key,
		typ,
	)

	defer qd.Release()

	err = qd.Exec()

	return
}
