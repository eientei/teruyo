package cassandra

import (
	"time"

	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"
	"gitlab.eientei.org/eientei/teruyo/internal/flake"
	"gitlab.eientei.org/eientei/teruyo/internal/model"
)

func (s *CassandraSuite) TestItemSave() {
	actor1 := vocab.LocalActor("TestItemSave1")
	actor2 := vocab.LocalActor("TestItemSave2")
	actor3 := vocab.LocalActor("TestItemSave3")

	item1 := model.Item{
		Data: &vocab.Object{
			Content: vocab.ValueString("test"),
			ID:      vocab.LocalObject("bar"),
			Type:    vocab.TypeObjectNote,
		},
		Created:      time.Now().UTC().Truncate(time.Millisecond),
		Updated:      time.Now().UTC().Truncate(time.Millisecond),
		Actor:        actor1,
		ReplyToItem:  "",
		ReplyToActor: "",
		Item:         vocab.LocalObject("bar"),
		Context:      vocab.LocalContext("baz"),
		List:         "",
		Recipients:   []vocab.IRI{actor2},
		Tags:         []string{"TestItemSave1", "TestItemSave2"},
		Replies:      0,
		Shares:       0,
		Likes:        0,
		Reactions:    0,
		ID:           flake.ID{},
		Visibility:   vocab.VisibilityPublic,
		Pinned:       true,
		Media:        false,
	}

	item2 := item1
	item2.Data.(*vocab.Object).Content = vocab.ValueString("edited")
	item2.Tags = []string{"TestItemSave1", "TestItemSave3"}
	item2.Recipients = []vocab.IRI{actor3}
	item2.Pinned = false

	item3 := item2
	item3.ReplyToItem = item2.Item
	item3.ReplyToActor = item2.Actor
	item3.Item = vocab.LocalObject("quz")

	err := s.repo.ItemSave(s.ctx, &item1)
	s.NoError(err)

	load1, err := s.repo.ItemGetByID(s.ctx, item1.Item)
	s.NoError(err)

	s.EqualValues(&item1, load1)

	err = s.repo.ItemSave(s.ctx, &item2)
	s.NoError(err)

	load2, err := s.repo.ItemGetByID(s.ctx, item2.Item)
	s.NoError(err)

	s.EqualValues(&item2, load2)

	err = s.repo.ItemSave(s.ctx, &item3)
	s.NoError(err)

	load3, err := s.repo.ItemGetByID(s.ctx, item3.Item)
	s.NoError(err)

	s.EqualValues(&item3, load3)
}
