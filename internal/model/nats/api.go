// Package nats provides queue implementation using nats server
package nats

import (
	"context"
	"sync"

	"github.com/nats-io/nats.go/jetstream"
	"gitlab.eientei.org/eientei/teruyo/internal/model"
)

type repositoryImpl struct {
	Config
	streams    map[string]jetstream.Stream
	consumers  map[string]jetstream.Consumer
	streamsM   sync.RWMutex
	consumersM sync.RWMutex
}

// Config for queue repository
type Config struct {
	JetStream jetstream.JetStream
}

// New returns new queue repository instance
func New(config *Config) (model.QueueRepository, error) {
	return &repositoryImpl{
		Config:    *config,
		streams:   make(map[string]jetstream.Stream),
		consumers: make(map[string]jetstream.Consumer),
	}, nil
}

func (repo *repositoryImpl) stream(ctx context.Context, topic string) (s jetstream.Stream, err error) {
	var ok bool

	repo.streamsM.RLock()

	s, ok = repo.streams[topic]

	repo.streamsM.RUnlock()

	if ok {
		return
	}

	repo.streamsM.Lock()
	defer repo.streamsM.Unlock()

	s, ok = repo.streams[topic]
	if ok {
		return
	}

	s, err = repo.JetStream.CreateStream(
		ctx,
		jetstream.StreamConfig{
			Name:      topic,
			Retention: jetstream.WorkQueuePolicy,
			Subjects:  []string{topic + ".>"},
		},
	)
	if err != nil {
		return
	}

	repo.streams[topic] = s

	return
}

func (repo *repositoryImpl) consumer(ctx context.Context, topic string) (cons jetstream.Consumer, err error) {
	var ok bool

	repo.consumersM.RLock()

	cons, ok = repo.consumers[topic]

	repo.consumersM.RUnlock()

	if ok {
		return
	}

	repo.consumersM.Lock()
	defer repo.consumersM.Unlock()

	cons, ok = repo.consumers[topic]
	if ok {
		return
	}

	stream, err := repo.stream(ctx, topic)
	if err != nil {
		return
	}

	cons, err = stream.CreateOrUpdateConsumer(ctx, jetstream.ConsumerConfig{
		Name: topic,
	})
	if err != nil {
		return
	}

	repo.consumers[topic] = cons

	return
}

func (repo *repositoryImpl) Enqueue(ctx context.Context, topic, partition string, data []byte) error {
	_, err := repo.JetStream.Publish(
		ctx,
		topic+"."+partition,
		data,
	)

	return err
}

func (repo *repositoryImpl) Dequeue(ctx context.Context, topic string) (model.QueuedItem, error) {
	cons, err := repo.consumer(ctx, topic)
	if err != nil {
		return nil, err
	}

	return cons.Next()
}
