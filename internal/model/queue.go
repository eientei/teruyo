package model

import (
	"context"
	"time"
)

// well-known queues
const (
	QueueFederationIncoming = "federation_incoming"
	QueueFederationOutgoing = "federation_outgoing"
)

// QueueRepository provides optionally partitioned topic-based queue
type QueueRepository interface {
	Enqueue(ctx context.Context, topic, partition string, data []byte) error
	Dequeue(ctx context.Context, topic string) (QueuedItem, error)
}

// QueuedItem abstraction for dequeued item
type QueuedItem interface {
	// Data returns the message body
	Data() []byte
	// Ack acknowledges a message
	// This tells the server that the message was successfully processed and it can move on to the next message
	Ack() error
	// DoubleAck acknowledges a message and waits for ack from server
	DoubleAck(context.Context) error
	// Nak negatively acknowledges a message
	// This tells the server to redeliver the message
	Nak() error
	// NakWithDelay negatively acknowledges a message
	// This tells the server to redeliver the message
	// after the given `delay` duration
	NakWithDelay(delay time.Duration) error
	// InProgress tells the server that this message is being worked on
	// It resets the redelivery timer on the server
	InProgress() error
	// Term tells the server to not redeliver this message, regardless of the value of nats.MaxDeliver
	Term() error
}
