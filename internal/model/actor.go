package model

import (
	"context"
	"time"

	"gitlab.eientei.org/eientei/teruyo/internal/flake"

	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"
)

// ActorRepository stores actors and actor metadata
type ActorRepository interface {
	ActorSave(ctx context.Context, actor *Actor) error
	ActorGetFederatedByID(ctx context.Context, actorID vocab.IRI) (*Actor, error)
	ActorGetFederatedByIDs(ctx context.Context, actorID ...vocab.IRI) ([]*Actor, error)
	ActorGetLocalByID(ctx context.Context, actorID vocab.IRI) (*Actor, error)
	ActorGetLocalByIDs(ctx context.Context, actorID ...vocab.IRI) ([]*Actor, error)
	ActorUpdateFollowers(ctx context.Context, actorID vocab.IRI, followers int) error
	ActorUpdateFollowing(ctx context.Context, actorID vocab.IRI, following int) error
	ActorUpdateSettings(ctx context.Context, actorID vocab.IRI, settings map[string]interface{}) error
	ActorUpdateAttributes(ctx context.Context, actorID vocab.IRI, attributes map[string]interface{}) error
}

// Actor model
type Actor struct {
	Attributes     map[string]string
	Settings       map[string]string
	Data           vocab.Item
	Created        time.Time
	Updated        time.Time
	Posted         time.Time
	Seen           time.Time
	Fetched        time.Time
	Actor          vocab.IRI
	Email          string
	Username       string
	PasswordHash   string
	PrivateKey     []byte
	ID             flake.ID
	FollowersCount int
	FollowingCount int
	Confirmed      bool
	Locked         bool
}
