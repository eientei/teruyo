package sorted

import "sort"

// StringSet implements sorted set for slices of string without using generics
type StringSet []string

// Add element to the set
func (set *StringSet) Add(els ...string) (newlen int) {
	for _, el := range els {
		idx := sort.Search(len(*set), func(i int) bool {
			return (*set)[i] >= el
		})
		if idx == len(*set) {
			*set = append(*set, el)

			continue
		}

		if (*set)[idx] == el {
			continue
		}

		*set = append((*set)[:idx], append([]string{el}, (*set)[idx:]...)...)
	}

	newlen = len(*set)

	return
}

// Contains returns true if set contains given element
func (set *StringSet) Contains(el string) bool {
	if set == nil {
		return false
	}

	idx := sort.Search(len(*set), func(i int) bool {
		return (*set)[i] >= el
	})

	if idx == len(*set) {
		return false
	}

	return (*set)[idx] == el
}

// Remove element from the set
func (set *StringSet) Remove(els ...string) (newlen int) {
	for _, el := range els {
		idx := sort.Search(len(*set), func(i int) bool {
			return (*set)[i] >= el
		})
		if idx == len(*set) {
			continue
		}

		if (*set)[idx] != el {
			continue
		}

		*set = append((*set)[:idx], (*set)[idx+1:]...)
	}

	newlen = len(*set)

	return
}
