// Package sorted provides generic sorted set collection
package sorted

import (
	"sort"

	"golang.org/x/exp/constraints"
)

// Set implements sorted set for slices of ordered types
type Set[T constraints.Ordered] []T

// Add element to the set
func (set *Set[T]) Add(els ...T) (newlen int) {
	for _, el := range els {
		idx := sort.Search(len(*set), func(i int) bool {
			return (*set)[i] >= el
		})
		if idx == len(*set) {
			*set = append(*set, el)

			continue
		}

		if (*set)[idx] == el {
			continue
		}

		*set = append((*set)[:idx], append([]T{el}, (*set)[idx:]...)...)
	}

	newlen = len(*set)

	return
}

// Contains returns true if set contains given element
func (set *Set[T]) Contains(el T) bool {
	if set == nil {
		return false
	}

	idx := sort.Search(len(*set), func(i int) bool {
		return (*set)[i] >= el
	})

	if idx == len(*set) {
		return false
	}

	return (*set)[idx] == el
}

// Remove element from the set
func (set *Set[T]) Remove(els ...T) (newlen int) {
	for _, el := range els {
		idx := sort.Search(len(*set), func(i int) bool {
			return (*set)[i] >= el
		})
		if idx == len(*set) {
			continue
		}

		if (*set)[idx] != el {
			continue
		}

		*set = append((*set)[:idx], (*set)[idx+1:]...)
	}

	newlen = len(*set)

	return
}

// SetComparator implements sorted set with provided comparator
type SetComparator[T any] struct {
	Comparator func(a, b T) int // should return positive if a >= b, 0 if a == b, negative if a < b
	Data       []T
}

// Add element to the set
func (set *SetComparator[T]) Add(els ...T) {
	for _, el := range els {
		idx := sort.Search(len(set.Data), func(i int) bool {
			return set.Comparator(set.Data[i], el) > 0
		})
		if idx == len(set.Data) {
			set.Data = append(set.Data, el)

			continue
		}

		if set.Comparator(set.Data[idx], el) == 0 {
			continue
		}

		set.Data = append(set.Data[:idx], append([]T{el}, set.Data[idx:]...)...)
	}
}

// Contains returns true if set contains given element
func (set *SetComparator[T]) Contains(el T) bool {
	idx := sort.Search(len(set.Data), func(i int) bool {
		return set.Comparator(set.Data[i], el) > 0
	})

	if idx == len(set.Data) {
		return false
	}

	return set.Comparator(set.Data[idx], el) == 0
}

// Remove element from the set
func (set *SetComparator[T]) Remove(els ...T) {
	for _, el := range els {
		idx := sort.Search(len(set.Data), func(i int) bool {
			return set.Comparator(set.Data[i], el) > 0
		})
		if idx == len(set.Data) {
			continue
		}

		if set.Comparator(set.Data[idx], el) != 0 {
			continue
		}

		set.Data = append(set.Data[:idx], set.Data[idx+1:]...)
	}
}
