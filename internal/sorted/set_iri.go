package sorted

import (
	"sort"

	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"
)

// IRISet implements sorted set for slices of vocab.IRI without using generics
type IRISet []vocab.IRI

// Add element to the set
func (set *IRISet) Add(els ...vocab.IRI) (newlen int) {
	for _, el := range els {
		idx := sort.Search(len(*set), func(i int) bool {
			return (*set)[i] >= el
		})
		if idx == len(*set) {
			*set = append(*set, el)

			continue
		}

		if (*set)[idx] == el {
			continue
		}

		*set = append((*set)[:idx], append([]vocab.IRI{el}, (*set)[idx:]...)...)
	}

	newlen = len(*set)

	return
}

// Contains returns true if set contains given element
func (set *IRISet) Contains(el vocab.IRI) bool {
	if set == nil {
		return false
	}

	idx := sort.Search(len(*set), func(i int) bool {
		return (*set)[i] >= el
	})

	if idx == len(*set) {
		return false
	}

	return (*set)[idx] == el
}

// Remove element from the set
func (set *IRISet) Remove(els ...vocab.IRI) (newlen int) {
	for _, el := range els {
		idx := sort.Search(len(*set), func(i int) bool {
			return (*set)[i] >= el
		})
		if idx == len(*set) {
			continue
		}

		if (*set)[idx] != el {
			continue
		}

		*set = append((*set)[:idx], (*set)[idx+1:]...)
	}

	newlen = len(*set)

	return
}
