package processor

import (
	"context"
	"fmt"
	"sync"
	"sync/atomic"

	"gitlab.eientei.org/eientei/teruyo/internal/errcode"
)

var (
	errNilConfig = errcode.CodeInvalidData.New("nil registry config")
)

type controlEvent int32

const (
	controlEventPause  = 0
	controlEventResume = 1
)

// RegistryConfig provides registry configuration
type RegistryConfig struct {
	Context     context.Context      // context each processor will be bound with
	Generators  map[string]Generator // generators for each processor type
	ScaleConfig map[string]int       // number of processor replicas, 1 for every generator if not specified
}

// Registry holds the instantiated Processors and manges their invocation
type Registry struct {
	RegistryConfig
	cancel      context.CancelFunc
	processors  map[string][]*processor
	processorsM sync.RWMutex
}

// NewRegistry returns new registry instance scaled to the parameters specified
func NewRegistry(ctx context.Context, config *RegistryConfig) (*Registry, error) {
	if config == nil {
		return nil, errNilConfig
	}

	if config.ScaleConfig == nil {
		config.ScaleConfig = make(map[string]int)

		for k := range config.Generators {
			config.ScaleConfig[k] = 1
		}
	}

	if config.Context == nil {
		config.Context = ctx
	}

	reg := &Registry{
		RegistryConfig: *config,
		processorsM:    sync.RWMutex{},
		processors:     make(map[string][]*processor),
	}

	for k, v := range config.ScaleConfig {
		err := reg.Scale(ctx, k, v)
		if err != nil {
			return nil, err
		}
	}

	reg.Context, reg.cancel = context.WithCancel(reg.Context)

	go func() {
		<-reg.Context.Done()

		_ = reg.Close()
	}()

	return reg, nil
}

// Close shuts down all processors
func (r *Registry) Close() error {
	var await []*processor

	r.processorsM.Lock()

	for _, ps := range r.processors {
		for _, p := range ps {
			p.cancel()

			await = append(await, p)
		}
	}

	r.processors = nil

	r.processorsM.Unlock()

	for _, p := range await {
		_ = p.await(context.Background())
	}

	r.cancel()

	return nil
}

// Scale changes number of processors to the desired workers count
func (r *Registry) Scale(ctx context.Context, processorType string, workers int) error {
	r.processorsM.Lock()

	if r.processors == nil {
		return nil
	}

	ps := r.processors[processorType]
	if workers == len(ps) {
		r.processorsM.Unlock()

		return nil
	}

	g, ok := r.Generators[processorType]
	if !ok {
		r.processorsM.Unlock()

		return fmt.Errorf("%w: %s", errGeneratorMissing, processorType)
	}

	var newprocs []Processor

	var await []*processor

	if workers > len(ps) {
		for i := 0; i < workers-len(ps); i++ {
			pinst, err := g.CreateProcessor(ctx)
			if err != nil {
				r.processorsM.Unlock()

				return err
			}

			newprocs = append(newprocs, pinst)
		}

		for _, pinst := range newprocs {
			pctx, pcancel := context.WithCancel(r.Context)

			p := &processor{
				Processor: pinst,
				ctx:       pctx,
				cancel:    pcancel,
				control:   make(chan controlEvent, 1),
				state:     controlEventResume,
			}

			r.processors[processorType] = append(r.processors[processorType], p)

			go p.work(processorType, r)
		}
	} else {
		for i := len(ps) - 1; i > workers-1; i-- {
			ps[i].cancel()

			await = append(await, ps[i])
		}
	}

	r.processorsM.Unlock()

	for _, p := range await {
		err := p.await(ctx)
		if err != nil {
			return err
		}
	}

	return nil
}

// Pause processing for given processor type. Current work unit on each processor instance may still get processed.
func (r *Registry) Pause(processorType string) error {
	r.processorsM.RLock()
	defer r.processorsM.RUnlock()

	if r.processors == nil {
		return nil
	}

	ps, ok := r.processors[processorType]
	if !ok {
		return nil
	}

	for _, p := range ps {
		atomic.StoreInt32(&p.state, controlEventPause)
	}

	return nil
}

// Resume processing for given processor type
func (r *Registry) Resume(processorType string) error {
	r.processorsM.RLock()
	defer r.processorsM.RUnlock()

	if r.processors == nil {
		return nil
	}

	ps, ok := r.processors[processorType]
	if !ok {
		return nil
	}

	for _, p := range ps {
		atomic.StoreInt32(&p.state, controlEventResume)

		select {
		case p.control <- controlEventResume: // try to wake up paused worker
		default: // do not block if any work is under way
		}
	}

	return nil
}

func (r *Registry) remove(processorType string, torem *processor) {
	r.processorsM.Lock()
	defer r.processorsM.Unlock()

	defer close(torem.control)

	if r.processors == nil {
		return
	}

	ps := r.processors[processorType]

	for idx, p := range ps {
		if p == torem {
			r.processors[processorType] = append(ps[:idx], ps[idx+1:]...)

			break
		}
	}

	if len(r.processors[processorType]) == 0 {
		delete(r.processors, processorType)
	}
}
