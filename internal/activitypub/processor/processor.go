package processor

import (
	"context"
	"encoding/binary"
	"encoding/hex"
	"sync/atomic"
	"unsafe"

	"github.com/rs/zerolog/log"
)

type processor struct {
	Processor
	ctx     context.Context
	cancel  context.CancelFunc
	control chan controlEvent
	state   int32
}

func (p *processor) work(name string, reg *Registry) {
	bs := make([]byte, 8)
	binary.BigEndian.PutUint64(bs, uint64(uintptr(unsafe.Pointer(p))))
	instance := hex.EncodeToString(bs)

	l := log.With().
		Str("processor", name).
		Str("instance", instance).
		Logger()

loop:
	for {
		switch atomic.LoadInt32(&p.state) {
		case controlEventPause:
			select {
			case <-p.ctx.Done():
				break loop
			case ns := <-p.control:
				atomic.StoreInt32(&p.state, int32(ns))
			}
		case controlEventResume:
			select {
			case <-p.ctx.Done():
				break loop
			case ns := <-p.control:
				atomic.StoreInt32(&p.state, int32(ns))
			default:
				err := p.Processor.Work(p.ctx)
				if err != nil {
					l.Error().Err(err).Msg("error processing work")
				}
			}
		}
	}

	err := p.Processor.Close()
	if err != nil {
		l.Error().Err(err).Msg("error closing processor")
	}

	reg.remove(name, p)
}

func (p *processor) await(ctx context.Context) error {
	waiting := true

	for waiting {
		select {
		case _, waiting = <-p.control:
		case <-ctx.Done():
			return ctx.Err()
		}
	}

	return nil
}
