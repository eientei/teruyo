// Package processor provides generalized processor interface for background workers
package processor

import (
	"context"

	"gitlab.eientei.org/eientei/teruyo/internal/errcode"
)

var (
	errGeneratorMissing = errcode.CodeInvalidState.New("processor type lacks generator definition")
)

// Processor is intended to do one small thing with no internal concurrency implied, parallelism should be handled
// by providing multiple instances of the same processor. Each processor should not not perform any activity unless
// Work() is called, allowing to pause the processors if needed.
type Processor interface {
	// Close must free all allocated resources
	Close() error
	// Work does the unit of work for this processor, which will be called in a loop so long processor is not paused.
	// it is allowed to take long time, blocking if there is no work available, but must respect context bounds,
	// Errors returned are not considered fatal.
	Work(ctx context.Context) error
}

// Generator returns new initialized processor instance
type Generator interface {
	CreateProcessor(ctx context.Context) (Processor, error)
}

// GeneratorFunc wrapper for function satisfying CreateProcessor signature
type GeneratorFunc func(ctx context.Context) (Processor, error)

// CreateProcessor implementation
func (g GeneratorFunc) CreateProcessor(ctx context.Context) (Processor, error) {
	return g(ctx)
}
