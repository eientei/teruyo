package incoming

import (
	"context"
	"time"

	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"
	"gitlab.eientei.org/eientei/teruyo/internal/model"
)

func (processor *processorImpl) resolveActor(
	ctx context.Context,
	item vocab.Item,
) (res *model.Actor, retry bool, err error) {
	var remote vocab.Item

	switch {
	case item.AsType() != "" && item.AsType().Root() == vocab.TypeActor:
		remote = item
	case item.AsIRI() != "":
		res, err = processor.ActorRepository.ActorGetFederatedByID(ctx, item.AsIRI())
		if err != nil {
			return res, true, err
		}

		if item.AsIRI().IsLocal() {
			return
		}

		if res == nil || time.Since(res.Fetched) > processor.ActorUpdatePeriod {
			remote, retry, err = processor.Fetcher.Fetch(ctx, item.AsIRI())
			if err != nil {
				return
			}
		} else {
			return
		}
	default:
		return nil, false, errUnresolvableItem
	}

	actor, _ := remote.(*vocab.Actor)
	if actor == nil {
		return nil, false, errUnresolvableItem
	}

	return processor.processActor(ctx, actor)
}

func (processor *processorImpl) processActor(
	ctx context.Context,
	actor *vocab.Actor,
) (res *model.Actor, retry bool, err error) {
	res, err = processor.ActorRepository.ActorGetFederatedByID(ctx, actor.AsIRI())
	if err != nil {
		return nil, true, err
	}

	if res == nil {
		res = &model.Actor{
			Data:    actor,
			Created: time.Now(),
			Updated: time.Now(),
			Fetched: time.Now(),
			Actor:   actor.AsIRI(),
		}
	} else {
		res.Data.Merge(actor)
		res.Updated = time.Now()
		res.Fetched = time.Now()
	}

	err = processor.ActorRepository.ActorSave(ctx, res)
	if err != nil {
		return nil, true, err
	}

	return
}
