package incoming

import (
	"context"
	"strings"
	"time"

	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"
	"gitlab.eientei.org/eientei/teruyo/internal/model"
	"gitlab.eientei.org/eientei/teruyo/internal/sorted"
)

func (processor *processorImpl) resolveObject(
	ctx context.Context,
	actor *model.Actor,
	item vocab.Item,
) (res *model.Item, retry bool, err error) {
	var remote vocab.Item

	switch {
	case item.AsType() != "" && item.AsType().Root() == vocab.TypeObject:
		remote = item
	case item.AsIRI() != "":
		res, err = processor.ItemRepository.ItemGetByID(ctx, item.AsIRI())
		if err != nil {
			return res, true, err
		}

		if res != nil {
			return
		}

		if item.AsIRI().IsLocal() {
			return
		}

		remote, retry, err = processor.Fetcher.Fetch(ctx, item.AsIRI())
		if err != nil {
			return
		}
	default:
		return nil, false, errUnresolvableItem
	}

	obj, _ := remote.(*vocab.Object)
	if obj == nil {
		return nil, false, errUnresolvableItem
	}

	if actor == nil {
		actor, retry, err = processor.resolveActor(ctx, obj.AttributedTo)
		if err != nil {
			return
		}
	}

	return processor.processObject(ctx, actor, obj)
}

func (processor *processorImpl) processRecipients(
	tos ...vocab.Item,
) (recipients sorted.IRISet) {
	for _, raw := range tos {
		to, _ := raw.(vocab.Items)

		for _, r := range to {
			recipients.Add(r.AsIRI())
		}

		if raw.AsIRI() != "" {
			recipients.Add(raw.AsIRI())
		}
	}

	return
}

func (processor *processorImpl) processObject(
	ctx context.Context,
	actor *model.Actor,
	obj *vocab.Object,
) (res *model.Item, retry bool, err error) {
	res, err = processor.ItemRepository.ItemGetByID(ctx, obj.ID)
	if err != nil {
		return nil, true, err
	}

	if res == nil {
		res = &model.Item{
			Created: time.Now(),
			Updated: time.Now(),
			Data:    obj,
			ID:      processor.Generator.Next(),
			Actor:   obj.AttributedTo.AsIRI(),
		}
	} else {
		res.Updated = time.Now()
		res.Data.Merge(obj)
	}

	if actor.Actor != obj.AttributedTo.AsIRI() {
		return nil, false, errUnresolvableItem
	}

	localctx := vocab.WithConfig(ctx, &processor.vocabConfigLocal)

	recipients := processor.processRecipients(obj.Audience, obj.To, obj.BTo, obj.CC, obj.BCC)

	if obj.InReplyTo != nil {
		res.ReplyToItem = obj.InReplyTo.AsIRI()

		var rep *model.Item

		rep, err = processor.ItemRepository.ItemGetByID(ctx, obj.InReplyTo.AsIRI())
		if err != nil {
			return nil, true, err
		}

		if rep == nil {
			rep, retry, err = processor.resolveObject(ctx, nil, obj.InReplyTo.AsIRI())
			if err != nil {
				return
			}
		}

		if rep != nil {
			res.ReplyToActor = rep.Actor

			err = processor.CollectionRepository.CollectionItemSave(
				localctx,
				&model.CollectionItem{
					Data:    nil,
					Created: time.Now(),
					Key:     rep.Item,
					Element: res.Item,
					Type:    model.CollectionTypeReplies,
				},
			)
			if err != nil {
				return nil, true, err
			}

			err = processor.ItemRepository.ItemUpdateReplies(ctx, rep.Item)
			if err != nil {
				return nil, true, err
			}

			if res.ReplyToActor.IsLocal() {
				recipients.Add(res.ReplyToActor)
			}
		}
	}

	if obj.AttributedTo != nil {
		res.Actor = obj.AttributedTo.AsIRI()
	}

	if obj.Context != nil {
		res.Context = obj.Context.AsIRI()
	}

	tags, _ := obj.Tag.(vocab.Items)

	var hashtags sorted.StringSet

	for _, t := range tags {
		if t.AsType() == vocab.TypeLinkHashtag {
			l, _ := t.(*vocab.Link)

			hashtags.Add(strings.TrimPrefix(l.Name.String(""), "#"))
		}
	}

	res.Tags = hashtags

	attachments, _ := obj.Attachment.(vocab.Items)

	res.Media = len(attachments) > 0

	if obj.ListMessage != nil {
		res.List = obj.ListMessage.AsIRI()
	}

	if obj.Sensitive != nil {
		res.Sensitive = obj.Sensitive.AsBool()
	}

	res.Visibility = obj.Visibility()

	var newrecipients sorted.IRISet

	newrecipients.Add(recipients...)
	newrecipients.Remove(res.Recipients...)

	res.Recipients = recipients

	err = processor.ItemRepository.ItemSave(localctx, res)
	if err != nil {
		return nil, true, err
	}

	return
}

func (processor *processorImpl) deliver(
	localctx context.Context,
	actor vocab.IRI,
	recipients sorted.IRISet,
	tl *model.TimelineItem,
) (err error) {
	var followers, direct sorted.IRISet

	for _, r := range recipients {
		if r.IsCollection(model.CollectionTypeFollowers.String()) {
			followers.Add(r.Parent())
		}
	}

	if len(followers) > 0 {
		var allfollowers []*model.CollectionItem

		allfollowers, err = processor.CollectionRepository.CollectionItemList(
			localctx,
			followers,
			[]model.CollectionType{model.CollectionTypeFollowers},
		)
		if err != nil {
			return
		}

		for _, f := range allfollowers {
			if !f.Element.IsLocal() {
				continue
			}

			recipients.Add(f.Element)
		}
	}

	for _, r := range recipients {
		if !r.IsLocal() {
			continue
		}

		tl.Key = r.AsIRI()

		direct.Add(r.AsIRI())

		err = processor.TimelineRepository.TimelineSave(localctx, tl)
		if err != nil {
			return err
		}
	}

	tl.Key = actor
	tl.Type = model.TimelineTypeOutbox
	tl.Recipients = direct

	err = processor.TimelineRepository.TimelineSave(localctx, tl)
	if err != nil {
		return err
	}

	return nil
}
