package incoming

import (
	"context"

	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"
	"gitlab.eientei.org/eientei/teruyo/internal/model"
)

func (processor *processorImpl) processActivity(
	ctx context.Context,
	activity *vocab.Activity,
) (retry bool, err error) {
	actor, retry, err := processor.resolveActor(ctx, activity.Actor)
	if err != nil {
		return
	}

	switch activity.AsType() {
	case vocab.TypeActivityAccept, vocab.TypeActivityAcceptTentative:
		return processor.processActivityAccept(ctx, actor, activity)
	case vocab.TypeActivityReject, vocab.TypeActivityRejectTentative:
		return processor.processActivityReject(ctx, actor, activity)
	case vocab.TypeActivityAdd:
		return processor.processActivityAdd(ctx, actor, activity)
	case vocab.TypeActivityAnnounce:
		return processor.processActivityAnnounce(ctx, actor, activity)
	case vocab.TypeActivityArrive:
		return processor.processActivityArrive(ctx, actor, activity)
	case vocab.TypeActivityBlock:
		return processor.processActivityBlock(ctx, actor, activity)
	case vocab.TypeActivityCreate:
		return processor.processActivityCreate(ctx, actor, activity)
	case vocab.TypeActivityDelete:
		return processor.processActivityDelete(ctx, actor, activity)
	case vocab.TypeActivityDislike:
		return processor.processActivityDislike(ctx, actor, activity)
	case vocab.TypeActivityFlag:
		return processor.processActivityFlag(ctx, actor, activity)
	case vocab.TypeActivityFollow:
		return processor.processActivityFollow(ctx, actor, activity)
	case vocab.TypeActivityIgnore:
		return processor.processActivityIgnore(ctx, actor, activity)
	case vocab.TypeActivityInvite:
		return processor.processActivityInvite(ctx, actor, activity)
	case vocab.TypeActivityJoin:
		return processor.processActivityJoin(ctx, actor, activity)
	case vocab.TypeActivityLeave:
		return processor.processActivityLeave(ctx, actor, activity)
	case vocab.TypeActivityLike:
		return processor.processActivityLike(ctx, actor, activity)
	case vocab.TypeActivityListen:
		return processor.processActivityListen(ctx, actor, activity)
	case vocab.TypeActivityMove:
		return processor.processActivityMove(ctx, actor, activity)
	case vocab.TypeActivityOffer:
		return processor.processActivityOffer(ctx, actor, activity)
	case vocab.TypeActivityRead:
		return processor.processActivityRead(ctx, actor, activity)
	case vocab.TypeActivityRemove:
		return processor.processActivityRemove(ctx, actor, activity)
	case vocab.TypeActivityTravel:
		return processor.processActivityTravel(ctx, actor, activity)
	case vocab.TypeActivityUndo:
		return processor.processActivityUndo(ctx, actor, activity)
	case vocab.TypeActivityUpdate:
		return processor.processActivityUpdate(ctx, actor, activity)
	case vocab.TypeActivityView:
		return processor.processActivityView(ctx, actor, activity)
	default:
		processor.l.Warn().
			Str("actor", string(actor.Actor)).
			Str("type", string(activity.AsType())).
			Msg("unknown activity type")

		return false, errUnresolvableItem
	}
}

//nolint:revive
func (processor *processorImpl) processActivityAccept(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	return
}

//nolint:revive
func (processor *processorImpl) processActivityReject(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	return
}

//nolint:revive
func (processor *processorImpl) processActivityAdd(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	return
}

func (processor *processorImpl) processActivityAnnounce(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	res, retry, err := processor.resolveObject(ctx, actor, activity.Object)
	if err != nil {
		return false, err
	}

	recipients := processor.processRecipients(activity.To, activity.BTo, activity.CC, activity.BCC)

	err = processor.deliver(
		vocab.WithConfig(ctx, &processor.vocabConfigLocal),
		actor.Actor,
		recipients,
		&model.TimelineItem{
			Created:          res.Created,
			Key:              "",
			Activity:         activity.AsIRI(),
			ActivityActor:    actor.Actor,
			ActivityType:     activity.Type,
			ItemActor:        res.Actor,
			ItemReplyToItem:  res.ReplyToItem,
			ItemReplyToActor: res.ReplyToActor,
			Item:             res.Item,
			ItemContext:      res.Context,
			ItemType:         res.Data.AsType(),
			Recipients:       nil,
			Type:             model.TimelineTypeInbox,
			ItemVisibility:   res.Visibility,
			ID:               res.ID,
			ItemReply:        res.ReplyToItem != "",
			ItemShare:        true,
			ItemPinned:       false,
			ItemMedia:        res.Media,
			ItemTags:         len(res.Tags) > 0,
		},
	)
	if err != nil {
		return true, err
	}

	return
}

//nolint:revive
func (processor *processorImpl) processActivityArrive(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	return
}

//nolint:revive
func (processor *processorImpl) processActivityBlock(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	return
}

func (processor *processorImpl) processActivityCreate(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	res, retry, err := processor.resolveObject(ctx, actor, activity.Object)
	if err != nil {
		return
	}

	err = processor.deliver(
		vocab.WithConfig(ctx, &processor.vocabConfigLocal),
		actor.Actor,
		res.Recipients,
		&model.TimelineItem{
			Created:          res.Created,
			Key:              "",
			Activity:         activity.AsIRI(),
			ActivityActor:    actor.Actor,
			ActivityType:     activity.Type,
			ItemActor:        res.Actor,
			ItemReplyToItem:  res.ReplyToItem,
			ItemReplyToActor: res.ReplyToActor,
			Item:             res.Item,
			ItemContext:      res.Context,
			ItemType:         res.Data.AsType(),
			Recipients:       nil,
			Type:             model.TimelineTypeInbox,
			ItemVisibility:   res.Visibility,
			ID:               res.ID,
			ItemReply:        res.ReplyToItem != "",
			ItemShare:        false,
			ItemPinned:       false,
			ItemMedia:        res.Media,
			ItemTags:         len(res.Tags) > 0,
		},
	)
	if err != nil {
		return true, err
	}

	return
}

//nolint:revive
func (processor *processorImpl) processActivityDelete(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	return
}

//nolint:revive
func (processor *processorImpl) processActivityDislike(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	return
}

//nolint:revive
func (processor *processorImpl) processActivityFlag(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	return
}

//nolint:revive
func (processor *processorImpl) processActivityFollow(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	return
}

//nolint:revive
func (processor *processorImpl) processActivityIgnore(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	return
}

//nolint:revive
func (processor *processorImpl) processActivityInvite(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	return
}

//nolint:revive
func (processor *processorImpl) processActivityJoin(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	return
}

//nolint:revive
func (processor *processorImpl) processActivityLeave(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	return
}

//nolint:revive
func (processor *processorImpl) processActivityLike(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	return
}

//nolint:revive
func (processor *processorImpl) processActivityListen(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	return
}

//nolint:revive
func (processor *processorImpl) processActivityMove(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	return
}

//nolint:revive
func (processor *processorImpl) processActivityOffer(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	return
}

//nolint:revive
func (processor *processorImpl) processActivityQuestion(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.ActivityQuestion,
) (retry bool, err error) {
	return
}

//nolint:revive
func (processor *processorImpl) processActivityRead(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	return
}

//nolint:revive
func (processor *processorImpl) processActivityRemove(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	return
}

//nolint:revive
func (processor *processorImpl) processActivityTravel(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	return
}

//nolint:revive
func (processor *processorImpl) processActivityUndo(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	return
}

func (processor *processorImpl) processActivityUpdate(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	res, retry, err := processor.resolveObject(ctx, actor, activity.Object)
	if err != nil {
		return
	}

	err = processor.deliver(
		vocab.WithConfig(ctx, &processor.vocabConfigLocal),
		actor.Actor,
		res.Recipients,
		&model.TimelineItem{
			Created:          res.Created,
			Key:              "",
			Activity:         activity.AsIRI(),
			ActivityActor:    actor.Actor,
			ActivityType:     activity.Type,
			ItemActor:        res.Actor,
			ItemReplyToItem:  res.ReplyToItem,
			ItemReplyToActor: res.ReplyToActor,
			Item:             res.Item,
			ItemContext:      res.Context,
			ItemType:         res.Data.AsType(),
			Recipients:       nil,
			Type:             model.TimelineTypeInbox,
			ItemVisibility:   res.Visibility,
			ID:               res.ID,
			ItemReply:        res.ReplyToItem != "",
			ItemShare:        false,
			ItemPinned:       false,
			ItemMedia:        res.Media,
			ItemTags:         len(res.Tags) > 0,
		},
	)
	if err != nil {
		return true, err
	}

	return
}

//nolint:revive
func (processor *processorImpl) processActivityView(
	ctx context.Context,
	actor *model.Actor,
	activity *vocab.Activity,
) (retry bool, err error) {
	return
}
