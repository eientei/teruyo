// Package incoming provides processor for incoming federation items
package incoming

import (
	"context"
	"reflect"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/fetcher"
	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/processor"
	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"
	"gitlab.eientei.org/eientei/teruyo/internal/errcode"
	"gitlab.eientei.org/eientei/teruyo/internal/flake"
	"gitlab.eientei.org/eientei/teruyo/internal/model"
)

var errUnresolvableItem = errcode.CodeInvalidData.New("unresolvable item")

// Config for processor
type Config struct {
	QueueRepository      model.QueueRepository
	ActorRepository      model.ActorRepository
	ItemRepository       model.ItemRepository
	CollectionRepository model.CollectionRepository
	TimelineRepository   model.TimelineRepository
	Generator            flake.Generator
	Fetcher              fetcher.Fetcher
	Instance             vocab.IRI
	RetryPeriod          time.Duration
	ActorUpdatePeriod    time.Duration
}

type processorImpl struct {
	*Config
	l                     zerolog.Logger
	vocabConfigFederation vocab.Config
	vocabConfigLocal      vocab.Config
}

// NewGenerator returns new processor generator instance
func NewGenerator(config *Config) (processor.Generator, error) {
	return processor.GeneratorFunc(func(ctx context.Context) (processor.Processor, error) {
		return &processorImpl{
			Config: config,
			l:      log.With().Str("processor", "federation_incoming").Logger(),
			vocabConfigFederation: vocab.Config{
				Instance:         config.Instance,
				OperationContext: vocab.OperationContextFederation,
			},
			vocabConfigLocal: vocab.Config{
				Instance:         config.Instance,
				OperationContext: vocab.OperationContextLocal,
			},
		}, nil
	}), nil
}

func (processor *processorImpl) Close() error {
	return nil
}

func (processor *processorImpl) resolveItem(
	ctx context.Context,
	raw vocab.Item,
) (remote vocab.Item, retriable bool, err error) {
	switch {
	case raw.AsType() != "":
		remote = raw
	case raw.AsIRI() != "":
		remote, retriable, err = processor.Fetcher.Fetch(ctx, raw.AsIRI())
		if err != nil {
			return
		}
	default:
		return nil, false, errUnresolvableItem
	}

	return
}

func (processor *processorImpl) Work(ctx context.Context) error {
	msg, err := processor.QueueRepository.Dequeue(ctx, model.QueueFederationIncoming)
	if err != nil {
		return err
	}

	ctx = vocab.WithConfig(ctx, &processor.vocabConfigFederation)

	raw, err := vocab.Unmarshal(ctx, msg.Data())
	if err != nil {
		return err
	}

	remote, retry, err := processor.resolveItem(ctx, raw)
	if err != nil {
		if retry {
			processor.l.Warn().
				RawJSON("data", msg.Data()).
				Msg("unresolvable item")

			return msg.NakWithDelay(processor.RetryPeriod)
		}

		return msg.Term()
	}

	if processor.l.GetLevel() == zerolog.TraceLevel {
		processor.l.Debug().
			RawJSON("data", msg.Data()).
			Str("type", string(remote.AsType())).
			Msg("item received")
	} else {
		processor.l.Debug().
			Str("type", string(remote.AsType())).
			Msg("item received")
	}

	retry, err = processor.processItem(ctx, msg, remote)
	if err != nil {
		if retry {
			processor.l.Warn().
				RawJSON("data", msg.Data()).
				Msg("unresolvable object")

			return msg.NakWithDelay(processor.RetryPeriod)
		}

		return msg.Term()
	}

	return msg.DoubleAck(ctx)
}

func (processor *processorImpl) processItem(
	ctx context.Context,
	msg model.QueuedItem,
	remote vocab.Item,
) (retry bool, err error) {
	switch cit := remote.(type) {
	case *vocab.Actor:
		_, retry, err = processor.processActor(ctx, cit)

		return
	case *vocab.Activity:
		return processor.processActivity(ctx, cit)
	case *vocab.ActivityQuestion:
		var actor *model.Actor

		actor, retry, err = processor.resolveActor(ctx, cit.Actor)
		if err != nil {
			return
		}

		return processor.processActivityQuestion(ctx, actor, cit)
	case *vocab.Object:
		var actor *model.Actor

		actor, retry, err = processor.resolveActor(ctx, cit.AttributedTo)
		if err != nil {
			return
		}

		_, retry, err = processor.processObject(ctx, actor, cit)

		return
	default:
		processor.l.Warn().
			RawJSON("data", msg.Data()).
			Str("type", reflect.TypeOf(remote).Name()).
			Msg("unknown item type")

		return false, errUnresolvableItem
	}
}
