package processor

import (
	"context"
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
)

type testProcessor struct {
	work  func(ctx context.Context) error
	close func() error
}

func (t *testProcessor) Work(ctx context.Context) error {
	return t.work(ctx)
}

func (t *testProcessor) Close() error {
	return t.close()
}

func TestRegistryNew(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	defer cancel()

	reg, err := NewRegistry(
		ctx,
		&RegistryConfig{
			Context: ctx,
			Generators: map[string]Generator{
				"foo": GeneratorFunc(func(c context.Context) (Processor, error) {
					return &testProcessor{
						work: func(ctx context.Context) error {
							return nil
						},
						close: func() error {
							return nil
						},
					}, nil
				}),
			},
			ScaleConfig: nil,
		},
	)

	assert.NoError(t, err)
	assert.NotNil(t, reg)

	err = reg.Scale(ctx, "foo", 1)
	assert.NoError(t, err)

	err = reg.Scale(ctx, "bar", 1)
	assert.ErrorIs(t, err, errGeneratorMissing)

	err = reg.Scale(ctx, "foo", 0)
	assert.NoError(t, err)

	err = reg.Scale(ctx, "foo", 2)
	assert.NoError(t, err)

	err = reg.Close()

	assert.NoError(t, err)

	err = reg.Scale(ctx, "foo", 1)
	assert.NoError(t, err)
}

func TestRegistryNewError(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	defer cancel()

	reg, err := NewRegistry(ctx, nil)

	assert.ErrorIs(t, err, errNilConfig)
	assert.Nil(t, reg)

	reg, err = NewRegistry(
		ctx,
		&RegistryConfig{
			Context: nil,
			Generators: map[string]Generator{
				"foo": GeneratorFunc(func(c context.Context) (Processor, error) {
					return nil, io.EOF
				}),
			},
			ScaleConfig: nil,
		},
	)

	assert.ErrorIs(t, err, io.EOF)
	assert.Nil(t, reg)
}

func TestRegistryNewCancelled(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	reg, err := NewRegistry(
		ctx,
		&RegistryConfig{
			Context: nil,
			Generators: map[string]Generator{
				"foo": GeneratorFunc(func(c context.Context) (Processor, error) {
					return &testProcessor{
						work: func(ctx context.Context) error {
							return nil
						},
						close: func() error {
							return nil
						},
					}, nil
				}),
			},
			ScaleConfig: nil,
		},
	)

	assert.NoError(t, err)
	assert.NotNil(t, reg)

	cancel()

	err = reg.Scale(ctx, "foo", 0)
	assert.ErrorIs(t, err, context.Canceled)
}

func TestRegistryPauseResume(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	defer cancel()

	ch := make(chan error)

	defer close(ch)

	start := make(chan struct{})

	reg, err := NewRegistry(
		ctx,
		&RegistryConfig{
			Context: ctx,
			Generators: map[string]Generator{
				"foo": GeneratorFunc(func(c context.Context) (Processor, error) {
					return &testProcessor{
						work: func(ctx context.Context) error {
							start <- struct{}{}

							return <-ch
						},
						close: func() error {
							close(start)

							return nil
						},
					}, nil
				}),
			},
			ScaleConfig: nil,
		},
	)

	assert.NoError(t, err)
	assert.NotNil(t, reg)

	<-start
	ch <- nil

	err = reg.Pause("foo")
	assert.NoError(t, err)

	reg.processors["foo"][0].control <- controlEventPause
	reg.processors["foo"][0].control <- controlEventPause

	err = reg.Resume("foo")
	assert.NoError(t, err)

	err = reg.Resume("foo")
	assert.NoError(t, err)

	err = reg.Resume("foo")
	assert.NoError(t, err)

	<-start
	ch <- nil

	<-start
	ch <- nil
}
