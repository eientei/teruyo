// Package fetcher provides abstraction for retrieving Items
package fetcher

import (
	"context"

	"gitlab.eientei.org/eientei/teruyo/internal/activitypub/vocab"
)

// Fetcher returns remote object by IRI
type Fetcher interface {
	Fetch(ctx context.Context, iri vocab.IRI) (item vocab.Item, retriable bool, err error)
}
