package vocab

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

const somePath = "/some/path"

func TestIRIFederatedMarshal(t *testing.T) {
	const instance = "https://example.com"

	ctx := WithConfig(
		context.Background(),
		&Config{
			Instance:         instance,
			OperationContext: OperationContextFederation,
		},
	)

	local := IRI(DIDAPNodeLocal + somePath)
	absolute := IRI(instance + somePath)
	unrelated := IRI("https://foobar/some/path")

	bs, err := MarshalJSONContext(ctx, local)
	assert.NoError(t, err)

	assert.Equal(t, `"`+instance+somePath+`"`, string(bs))

	bs, err = MarshalJSONContext(ctx, absolute)
	assert.NoError(t, err)

	assert.Equal(t, `"`+instance+somePath+`"`, string(bs))

	bs, err = MarshalJSONContext(ctx, unrelated)
	assert.NoError(t, err)

	assert.Equal(t, string(`"`+unrelated+`"`), string(bs))
}

func TestIRILocalMarshal(t *testing.T) {
	const instance = "https://example.com"

	ctx := WithConfig(
		context.Background(),
		&Config{
			Instance:         instance,
			OperationContext: OperationContextLocal,
		},
	)

	local := IRI(DIDAPNodeLocal + somePath)
	absolute := IRI(instance + somePath)
	unrelated := IRI("https://foobar/some/path")

	bs, err := MarshalJSONContext(ctx, local)
	assert.NoError(t, err)

	assert.Equal(t, `"`+DIDAPNodeLocal+somePath+`"`, string(bs))

	bs, err = MarshalJSONContext(ctx, absolute)
	assert.NoError(t, err)

	assert.Equal(t, `"`+DIDAPNodeLocal+somePath+`"`, string(bs))

	bs, err = MarshalJSONContext(ctx, unrelated)
	assert.NoError(t, err)

	assert.Equal(t, string(`"`+unrelated+`"`), string(bs))
}

func TestIRIInvalidContext(t *testing.T) {
	const data = `
{
  "@context": "https://www.w3.org/ns/activitystreams",
  "type": "Object",
  "id": "did:apnode:local/object/1",
  "name": "A Simple, non-specific object"
}
`

	ctx := WithConfig(
		context.Background(),
		&Config{
			Instance:         "https://example.com",
			OperationContext: OperationContextFederation,
		},
	)

	var b Object

	err := UnmarshalJSONContext(ctx, ([]byte)(data), &b)
	assert.Error(t, err)
}
