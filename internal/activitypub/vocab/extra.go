package vocab

import (
	"context"
	"encoding/json"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

// Extra fields not directly defined in embedding objects
type Extra struct {
	Data ExtraData
	Raw  ExtraRaw
}

// Merge implementation
func (e *Extra) Merge(raw *Extra) {
	if raw.Data == nil && raw.Raw == nil {
		return
	}

	for k, v := range raw.AsData() {
		e.Data[k] = v
	}

	e.Raw = nil
}

// MarshalUnknownsFields marshals all extra fields, if there is any
func (e *Extra) MarshalUnknownsFields(ctx context.Context, w *jwriter.Writer, first bool) bool {
	switch {
	case e.Data != nil:
		return e.Data.MarshalUnknownsFields(ctx, w, first)
	case e.Raw != nil:
		return e.Raw.MarshalUnknownsFields(ctx, w, first)
	default:
		return first
	}
}

// MarshalUnknowns implementation
func (e *Extra) MarshalUnknowns(ctx context.Context, w *jwriter.Writer, first bool) {
	switch {
	case e.Data != nil:
		e.Data.MarshalUnknowns(ctx, w, first)
	case e.Raw != nil:
		e.Raw.MarshalUnknowns(ctx, w, first)
	}
}

// IsZero implementation
func (e *Extra) IsZero(context.Context) bool {
	return (e.Raw == nil || len(e.Raw) == 0) && (e.Data == nil || len(e.Data) == 0)
}

// UnmarshalJSONContext implementation
func (e *Extra) UnmarshalJSONContext(_ context.Context, r *jlexer.Lexer) {
	e.Reset()

	start := r.IsStart()

	if !JSONStart(r) {
		return
	}

	r.Delim('{')

	for r.Ok() && !r.IsDelim('}') {
		key, ok := JSONUnmarshalFieldPrefix(r)
		if !ok {
			continue
		}

		e.Raw.Append(key, r.Raw())

		r.WantComma()
	}

	r.Delim('}')

	JSONEnd(r, start)
}

// AsRaw marshals data extras as raw extras
func (e *Extra) AsRaw() ExtraRaw {
	if e.Raw != nil {
		return e.Raw
	}

	e.Raw = nil

	for k, v := range e.Data {
		bs, _ := json.Marshal(v)

		e.Raw.Append(k, bs)
	}

	return e.Raw
}

// AsData unmarshals raw extras as data extras
func (e *Extra) AsData() ExtraData {
	if e.Data != nil {
		return e.Data
	}

	e.Data = make(ExtraData)

	var r jlexer.Lexer

	for _, v := range e.Raw {
		r = jlexer.Lexer{Data: v.Value}

		e.Data[v.Key] = r.Interface()
	}

	return e.Data
}

// AsRawResetData resets data extras and returns raw extras
func (e *Extra) AsRawResetData() ExtraRaw {
	raw := e.AsRaw()
	e.Data = nil

	return raw
}

// AsDataResetRaw resets raw extras and returns data extras
func (e *Extra) AsDataResetRaw() ExtraData {
	data := e.AsData()
	e.Raw = nil

	return data
}

// Reset resets all extra data
func (e *Extra) Reset() {
	e.Data = nil
	e.Raw = nil
}

// ExtraRawField field name / raw value pair
type ExtraRawField struct {
	Key   string
	Value []byte
}

// ExtraRaw defines raw byte-encoded embedded properties
type ExtraRaw []ExtraRawField

// MarshalUnknownsFields marshals all extra fields, if there is any
func (e *ExtraRaw) MarshalUnknownsFields(_ context.Context, w *jwriter.Writer, first bool) bool {
	if *e != nil {
		return first
	}

	for _, v := range *e {
		if !first {
			w.RawByte(',')
		}

		first = false

		w.String(v.Key)
		w.RawByte(':')
		w.Raw(v.Value, nil)
	}

	return first
}

// MarshalUnknowns implementation
func (e ExtraRaw) MarshalUnknowns(ctx context.Context, w *jwriter.Writer, first bool) {
	e.MarshalUnknownsFields(ctx, w, first)
}

// Append adds both key and value
func (e *ExtraRaw) Append(k string, v []byte) {
	el := ExtraRawField{Key: k, Value: v}

	if k == "type" {
		*e = append([]ExtraRawField{el}, *e...)
	} else {
		*e = append(*e, el)
	}
}

// UnmarshalJSONContext //implementation
func (e *ExtraRaw) UnmarshalJSONContext(_ context.Context, r *jlexer.Lexer) {
	start := r.IsStart()

	if !JSONStart(r) {
		*e = ExtraRaw{}

		return
	}

	if *e == nil || cap(*e) == 0 {
		*e = make(ExtraRaw, 0, 16)
	}

	r.Delim('{')

	for r.Ok() && !r.IsDelim('}') {
		key, ok := JSONUnmarshalFieldPrefix(r)
		if !ok {
			continue
		}

		e.Append(key, r.Raw())

		r.WantComma()
	}

	r.Delim('}')

	JSONEnd(r, start)
}

// ExtraData defines raw interface{}-decoded extra properties
type ExtraData map[string]interface{}

// MarshalUnknownsFields marshals all extra fields, if there is any
func (e ExtraData) MarshalUnknownsFields(_ context.Context, w *jwriter.Writer, first bool) bool {
	if e == nil {
		return first
	}

	for k, v := range e {
		if !first {
			w.RawByte(',')
		}

		first = false

		w.String(k)
		w.RawByte(':')
		w.Raw(json.Marshal(v))
	}

	return first
}

// MarshalUnknowns implementation
func (e ExtraData) MarshalUnknowns(ctx context.Context, w *jwriter.Writer, first bool) {
	e.MarshalUnknownsFields(ctx, w, first)
}

// UnmarshalUnknown implementation
func (e *ExtraData) UnmarshalUnknown(_ context.Context, in *jlexer.Lexer, key string) {
	if *e == nil {
		*e = make(ExtraData)
	}

	(*e)[key] = in.Interface()
}
