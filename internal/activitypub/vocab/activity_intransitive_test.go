package vocab

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestIntransitiveActivityMarshalEmpty(t *testing.T) {
	link := &ActivityIntransitive{}

	bs, err := MarshalJSONContext(context.Background(), link)
	assert.NoError(t, err)

	assert.Equal(t, `{}`, string(bs))

	err = UnmarshalJSONContext(context.Background(), bs, link)
	assert.NoError(t, err)
}

func TestIntransitiveActivityMarshalFilled(t *testing.T) {
	a := ActivityIntransitive{
		Actor:      IRI("b1"),
		Object:     IRI("b2"),
		Target:     IRI("b3"),
		Result:     IRI("b4"),
		Origin:     IRI("b5"),
		Instrument: Items{IRI("b6")},
		ObjectBase: ObjectBase{
			Extra: Extra{
				Data: map[string]interface{}{
					"foo": float64(1),
					"bar": "aaa",
					"baz": map[string]interface{}{
						"abc": "efg",
						"efg": float64(34),
					},
				},
			},
			JSONLDContext: JSONLDContextString("ggg"),
			Attachment:    IRI("a1"),
			AttributedTo:  IRI("a2"),
			Content:       ValueString("a3"),
			Context:       IRI("a4"),
			Name:          ValueMap{"en": "a5"},
			Generator:     IRI("a6"),
			Icon:          IRI("a7"),
			Image:         IRI("a8"),
			InReplyTo: &Object{
				ID: "a9",
			},
			Location:  IRI("a10"),
			Preview:   IRI("a11"),
			Published: Time(time.Unix(150, 0)),
			Replies:   IRI("a13"),
			Summary:   ValueString("a14"),
			URL:       IRI("a15"),
			Likes:     IRI("a16"),
			Shares:    IRI("a17"),
			Source:    IRI("a18"),
			Audience: Items{
				IRI("a19-1"),
				&Object{
					ID: "a19-2",
				},
			},
			Tag:       Items{IRI("a20")},
			To:        Items{IRI("a21")},
			BTo:       Items{IRI("a22")},
			CC:        Items{IRI("a23")},
			BCC:       Items{IRI("a24")},
			ID:        "a25",
			Type:      "a26",
			MediaType: "a27",
			EndTime:   Time(time.Unix(100, 0)),
			StartTime: Time(time.Unix(50, 0)),
			Updated:   Time(time.Unix(75, 0)),
			Duration:  time.Minute,
		},
	}

	ctx := WithConfig(context.Background(), &Config{Instance: "example.com", OperationContext: OperationContextLocal})

	bs, err := MarshalJSONContext(ctx, &a)
	assert.NoError(t, err)

	var b ActivityIntransitive

	err = UnmarshalJSONContext(ctx, bs, &b)
	assert.NoError(t, err)

	b.AsDataResetRaw()

	assert.EqualValues(t, a, b)
}

func TestIntransitiveActivityUnmarshalExample4(t *testing.T) {
	a := &ActivityIntransitive{
		Actor: &Actor{
			ObjectBase: ObjectBase{
				Type: TypeActorPerson,
				Name: ValueString("Sally"),
			},
		},
		Object: nil,
		Target: &Object{
			Type: TypeObjectPlace,
			Name: ValueString("Work"),
		},
		Result:     nil,
		Origin:     nil,
		Instrument: nil,
		ObjectBase: ObjectBase{
			JSONLDContext: JSONLDContextString("https://www.w3.org/ns/activitystreams"),
			Type:          TypeActivityTravel,
			Summary:       ValueString("Sally went to work"),
		},
	}

	const data = `
{
  "@context": "https://www.w3.org/ns/activitystreams",
  "type": "Travel",
  "summary": "Sally went to work",
  "actor": {
    "type": "Person",
    "name": "Sally"
  },
  "target": {
    "type": "Place",
    "name": "Work"
  }
}
`

	var b ActivityIntransitive

	err := UnmarshalJSONContext(context.Background(), ([]byte)(data), &b)
	assert.NoError(t, err)

	assert.EqualValues(t, a, &b)
}
