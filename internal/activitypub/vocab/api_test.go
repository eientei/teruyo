package vocab

import (
	"context"
	"testing"
)

func BenchmarkJSONUnmarshal(t *testing.B) {
	bs := ([]byte)(`{
  "@context": "ggg",
  "attachment": "a1",
  "attributedTo": "a2",
  "content": "a3",
  "context": "a4",
  "nameMap": {
    "en": "a5"
  },
  "generator": "a6",
  "icon": "a7",
  "image": "a8",
  "inReplyTo": {
    "id": "a9"
  },
  "location": "a10",
  "preview": "a11",
  "published": "1970-01-01T03:02:30+03:00",
  "replies": "a13",
  "summary": "a14",
  "url": "a15",
  "likes": "a16",
  "shares": "a17",
  "source": "a18",
  "audience": [
    "a19-1",
    {
      "id": "a19-2"
    }
  ],
  "tag": [
    "a20"
  ],
  "to": [
    "a21"
  ],
  "bto": [
    "a22"
  ],
  "cc": [
    "a23"
  ],
  "bcc": [
    "a24"
  ],
  "id": "a25",
  "type": "Note",
  "mediaType": "a27",
  "endTime": "1970-01-01T03:01:40+03:00",
  "startTime": "1970-01-01T03:00:50+03:00",
  "updated": "1970-01-01T03:01:15+03:00",
  "duration": "PT1M",
  "foo": 1,
  "bar": "aaa",
  "baz": {
    "abc": "efg",
    "efg": 34
  },
  "actor": "b1",
  "object": "b2",
  "target": "b3",
  "result": "b4",
  "origin": "b5",
  "instrument": "b6"
}`)

	ctx := context.Background()

	for i := 0; i < t.N; i++ {
		_, _ = Unmarshal(ctx, bs)
	}
}

func BenchmarkJSONMarshal(t *testing.B) {
	ctx := context.Background()

	for i := 0; i < t.N; i++ {
		_, _ = Marshal(ctx, &dataActivityFilled)
	}
}
