package vocab

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"golang.org/x/text/language"
)

var dataLinkFilled = Link{
	Extra: Extra{
		Data: map[string]interface{}{
			"foo": float64(1),
			"bar": "aaa",
			"baz": map[string]interface{}{
				"abc": "efg",
				"efg": float64(34),
			},
		},
	},
	JSONLDContext: JSONLDContextString("ggg"),
	Name:          ValueString("a1"),
	Preview:       IRI("a2"),
	HrefLang:      language.English,
	ID:            "a3",
	Type:          "a4",
	Rel:           "a5",
	Href:          "a6",
	MediaType:     "a7",
	Height:        10,
	Width:         20,
}

const dataExample2 = `
{
  "@context": "https://www.w3.org/ns/activitystreams",
  "type": "Link",
  "href": "http://example.org/abc",
  "hreflang": "en",
  "mediaType": "text/html",
  "name": "An example link"
}
`

func TestLinkMarshalEmpty(t *testing.T) {
	link := &Link{}

	bs, err := MarshalJSONContext(context.Background(), link)
	assert.NoError(t, err)

	assert.Equal(t, `{}`, string(bs))

	err = UnmarshalJSONContext(context.Background(), bs, link)
	assert.NoError(t, err)
}

func TestLinkMarshalFilled(t *testing.T) {
	bs, err := MarshalJSONContext(context.Background(), &dataLinkFilled)
	assert.NoError(t, err)

	var b Link

	err = UnmarshalJSONContext(context.Background(), bs, &b)
	assert.NoError(t, err)

	b.AsDataResetRaw()

	assert.EqualValues(t, dataLinkFilled, b)
}

func TestLinkUnmarshalExample2(t *testing.T) {
	a := &Link{
		JSONLDContext: JSONLDContextString("https://www.w3.org/ns/activitystreams"),
		Type:          TypeLink,
		Href:          IRI("http://example.org/abc"),
		HrefLang:      language.English,
		MediaType:     MediaType("text/html"),
		Name:          ValueString("An example link"),
	}

	var b Link

	err := UnmarshalJSONContext(context.Background(), ([]byte)(dataExample2), &b)
	assert.NoError(t, err)

	assert.EqualValues(t, a, &b)
}

func FuzzLinkUnmarshal(f *testing.F) {
	bs, err := MarshalJSONContext(context.Background(), &dataLinkFilled)
	assert.NoError(f, err)

	f.Add(([]byte)(dataExample2))
	f.Add(bs)

	f.Fuzz(func(t *testing.T, data []byte) {
		var v Link

		_ = UnmarshalJSONContext(context.Background(), data, &v)
	})
}
