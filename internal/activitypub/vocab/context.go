package vocab

import (
	"context"
	"encoding/json"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

const fieldATContext = "@context"

// JSONLDContext provides combined JSON-LD interface for @context field marshalling/unmarshalling
type JSONLDContext interface {
	MarshalerContext
}

// JSONMarshalJSONLDContext marshals JSONLDContext
func JSONMarshalJSONLDContext(ctx context.Context, w *jwriter.Writer, jsldctx JSONLDContext, first bool) bool {
	if jsldctx == nil || jsldctx.IsZero(ctx) {
		return first
	}

	if first {
		w.RawByte('"')
	} else {
		w.RawString(`,"`)
	}

	w.RawString(fieldATContext)
	w.RawString(`":`)

	jsldctx.MarshalJSONContext(ctx, w)

	return false
}

// JSONUnmarshalJSONLDContext unmarshals JSONLDContext
func JSONUnmarshalJSONLDContext(ctx context.Context, r *jlexer.Lexer, value *JSONLDContext) {
	switch {
	case r.IsNull():
		r.Skip()

		*value = nil

	case r.IsDelim('{'):
		var jsldctx JSONLDContextData

		jsldctx.UnmarshalJSONContext(ctx, r)

		*value = jsldctx
	case r.IsDelim('['):
		var jsldctx JSONLDContextList

		jsldctx.UnmarshalJSONContext(ctx, r)

		*value = jsldctx
	case r.CurrentToken() == jlexer.TokenString:
		*value = JSONLDContextString(r.String())
	default:
		r.AddError(errInvalidJSON)
	}
}

// JSONLDContextString implements @context for string IRI
type JSONLDContextString string

// IsZero implementation
func (value JSONLDContextString) IsZero(context.Context) bool {
	return value == ""
}

// MarshalJSONContext implementation
func (value JSONLDContextString) MarshalJSONContext(_ context.Context, w *jwriter.Writer) {
	w.String(string(value))
}

// UnmarshalJSONContext implementation
func (value *JSONLDContextString) UnmarshalJSONContext(_ context.Context, r *jlexer.Lexer) {
	*value = JSONLDContextString(r.String())
}

// JSONLDContextData implements @context for map of context prefixes
type JSONLDContextData map[string]interface{}

// IsZero implementation
func (value JSONLDContextData) IsZero(context.Context) bool {
	return len(value) == 0
}

// MarshalJSONContext implementation
func (value JSONLDContextData) MarshalJSONContext(_ context.Context, w *jwriter.Writer) {
	if value == nil {
		w.RawString("null")

		return
	}

	w.RawByte('{')

	first := true

	for k, v := range value {
		if !first {
			w.RawByte(',')
		}

		first = false

		w.String(k)
		w.RawByte(':')
		w.Raw(json.Marshal(v))
	}

	w.RawByte('}')
}

// UnmarshalJSONContext implementation
func (value *JSONLDContextData) UnmarshalJSONContext(_ context.Context, r *jlexer.Lexer) {
	start := r.IsStart()

	if !JSONStart(r) {
		*value = nil

		return
	}

	if *value == nil {
		*value = make(JSONLDContextData)
	}

	r.Delim('{')

	for r.Ok() && !r.IsDelim('}') {
		key := r.String()

		r.WantColon()

		if r.IsNull() {
			r.Skip()
			r.WantComma()

			continue
		}

		(*value)[key] = r.Interface()

		r.WantComma()
	}

	r.Delim('}')

	JSONEnd(r, start)
}

// JSONLDContextList implements @context for list of context definitions
type JSONLDContextList []JSONLDContext

// IsZero implementation
func (value JSONLDContextList) IsZero(context.Context) bool {
	return len(value) == 0
}

// MarshalJSONContext implementation
func (value JSONLDContextList) MarshalJSONContext(ctx context.Context, w *jwriter.Writer) {
	w.RawByte('[')

	for i, v := range value {
		if i > 0 {
			w.RawByte(',')
		}

		v.MarshalJSONContext(ctx, w)
	}

	w.RawByte(']')
}

// UnmarshalJSONContext implementation
func (value *JSONLDContextList) UnmarshalJSONContext(ctx context.Context, r *jlexer.Lexer) {
	start := r.IsStart()

	if !JSONStart(r) {
		*value = nil

		return
	}

	if *value == nil {
		if !r.IsDelim(']') {
			*value = make(JSONLDContextList, 0, 4)
		} else {
			*value = JSONLDContextList{}
		}
	} else {
		*value = (*value)[:0]
	}

	r.Delim('[')

	for r.Ok() && !r.IsDelim(']') {
		var c JSONLDContext

		JSONUnmarshalJSONLDContext(ctx, r, &c)

		*value = append(*value, c)

		r.WantComma()
	}

	r.Delim(']')

	JSONEnd(r, start)
}
