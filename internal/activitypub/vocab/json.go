package vocab

import (
	"context"
	"errors"
	"time"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
	"github.com/sosodev/duration"
	"golang.org/x/text/language"
)

var errInvalidJSON = errors.New("invalid json")

var nullBytes = []byte("null")

// MarshalerContext provides json marshalling with context
type MarshalerContext interface {
	MarshalJSONContext(ctx context.Context, w *jwriter.Writer)
	IsZero(ctx context.Context) bool
}

// UnmarshalerContext provides json marshalling with context
type UnmarshalerContext interface {
	UnmarshalJSONContext(ctx context.Context, r *jlexer.Lexer)
}

// Raw deserializer
type Raw []byte

// UnmarshalJSONContext implementation
func (o *Raw) UnmarshalJSONContext(_ context.Context, r *jlexer.Lexer) {
	*o = r.Raw()
}

// JSONStart tests start of nullable top-level token, consuming rest of the input of the case
func JSONStart(r *jlexer.Lexer) bool {
	if r.IsNull() {
		if r.IsStart() {
			r.Consumed()
		}

		r.Skip()

		return false
	}

	return true
}

// JSONEnd consumes rest of the input for top-level token
func JSONEnd(r *jlexer.Lexer, start bool) {
	if start {
		r.Consumed()
	}
}

// JSONMarshalString marshals string field
func JSONMarshalString(w *jwriter.Writer, value, field string, first bool) bool {
	if value == "" {
		return first
	}

	if first {
		w.RawByte('"')
	} else {
		w.RawString(`,"`)
	}

	w.RawString(field)
	w.RawString(`":`)

	w.String(value)

	return false
}

// JSONMarshalUint marshals uint field
func JSONMarshalUint(w *jwriter.Writer, value uint, field string, first bool) bool {
	if value == 0 {
		return first
	}

	if first {
		w.RawByte('"')
	} else {
		w.RawString(`,"`)
	}

	w.RawString(field)
	w.RawString(`":`)

	w.Uint(value)

	return false
}

// JSONMarshalDuration marshals duration field in ISO 8601 format
func JSONMarshalDuration(w *jwriter.Writer, value time.Duration, field string, first bool) bool {
	if value == 0 {
		return first
	}

	return JSONMarshalString(w, duration.Format(value), field, first)
}

// JSONUnmarshalDuration unmarshals duration field from ISO 8601 format
func JSONUnmarshalDuration(r *jlexer.Lexer, res *time.Duration) {
	start := r.IsStart()

	if !JSONStart(r) {
		*res = 0

		return
	}

	dur, err := duration.Parse(r.String())
	if err != nil {
		r.AddNonFatalError(err)

		return
	}

	*res = dur.ToTimeDuration()

	JSONEnd(r, start)
}

// JSONMarshalField marshals generic json field
func JSONMarshalField(ctx context.Context, w *jwriter.Writer, t MarshalerContext, field string, first bool) bool {
	if t == nil || t.IsZero(ctx) {
		return first
	}

	if first {
		w.RawByte('"')
	} else {
		w.RawString(`,"`)
	}

	w.RawString(field)
	w.RawString(`":`)

	t.MarshalJSONContext(ctx, w)

	return false
}

// JSONUnmarshalFieldPrefix extracts JSON field token
func JSONUnmarshalFieldPrefix(r *jlexer.Lexer) (key string, ok bool) {
	key = r.String()

	r.WantColon()

	if r.IsNull() {
		r.Skip()
		r.WantComma()

		ok = false
	} else {
		ok = true
	}

	return
}

// JSONParseLanguage parses language tag
func JSONParseLanguage(r *jlexer.Lexer, lang string, tag *language.Tag) {
	t, err := language.Parse(lang)
	if err != nil {
		r.AddNonFatalError(err)

		*tag = language.Und

		return
	}

	*tag = t
}

// JSONUnmarshalLanguage unmarshals language tag
func JSONUnmarshalLanguage(r *jlexer.Lexer, tag *language.Tag) {
	start := r.IsStart()

	if !JSONStart(r) {
		*tag = language.Und

		return
	}

	JSONParseLanguage(r, r.String(), tag)

	JSONEnd(r, start)
}

// JSONMarshalLanguage marshals language tag
func JSONMarshalLanguage(w *jwriter.Writer, tag language.Tag, field string, first bool) bool {
	if tag == language.Und {
		return first
	}

	return JSONMarshalString(w, tag.String(), field, first)
}
