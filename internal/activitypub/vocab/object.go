package vocab

import (
	"context"
	"strings"
	"time"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

// ObjectBase defines common base fields for all objects
type ObjectBase struct {
	// The date and time at which the object was published
	Published Time
	// The date and time describing the actual or expected ending time of the object.
	// When used with an Activity object, for instance, the endTime property specifies the moment the activity
	// concluded or is expected to conclude.
	EndTime Time
	// The date and time describing the actual or expected starting time of the object.
	// When used with an Activity object, for instance, the startTime property specifies the moment the activity
	// began or is scheduled to begin.
	StartTime Time
	// The date and time at which the object was updated
	Updated Time
	// JSON-LD context
	JSONLDContext JSONLDContext
	// Identifies a resource attached or related to an object that potentially requires special handling.
	// The intent is to provide a model that is at least semantically similar to attachments in email.
	Attachment Item
	// Identifies one or more entities to which this object is attributed.
	// The attributed entities might not be Actors.
	// For instance, an object might be attributed to the completion of another activity.
	AttributedTo Item
	// The content or textual representation of the Object encoded as a JSON string.
	// By default, the value of content is HTML.
	// The mediaType property can be used in the object to indicate a different content type.
	//
	// The content MAY be expressed using multiple language-tagged values.
	Content Value
	// Identifies the context within which the object exists or an activity was performed.
	//
	// The notion of "context" used is intentionally vague.
	// The intended function is to serve as a means of grouping objects and activities that share a common
	// originating context or purpose. An example could be all activities relating to a common project or event.
	Context Item
	// A simple, human-readable, plain-text name for the object.
	// HTML markup MUST NOT be included.
	// The name MAY be expressed using multiple language-tagged values.
	Name Value
	// Identifies the entity (e.g. an application) that generated the object.
	Generator Item
	// Indicates an entity that describes an icon for this object.
	// The image should have an aspect ratio of one (horizontal) to one (vertical) and should be suitable
	// for presentation at a small size.
	Icon Item
	// Indicates an entity that describes an image for this object.
	// Unlike the icon property, there are no aspect ratio or display size limitations assumed.
	Image Item
	// Indicates one or more entities for which this object is considered a response.
	InReplyTo Item
	// Indicates one or more physical or logical locations associated with the object.
	Location Item
	// Identifies an entity that provides a preview of this object.
	Preview Item
	// Identifies a Collection containing objects considered to be responses to this object.
	Replies Item
	// A natural language summarization of the object encoded as HTML.
	// Multiple language tagged summaries MAY be provided.
	Summary Value
	// Identifies one or more links to representations of the object
	URL Item
	// Every object MAY have a likes collection.
	// This is a list of all Like activities with this object as the object property, added as a side effect.
	// The likes collection MUST be either an CollectionOrdered or a Collection and MAY be filtered on privileges of
	// an authenticated user or as appropriate when no authentication is given.
	Likes Item
	// Every object MAY have a shares collection.
	// This is a list of all Announce activities with this object as the object property, added as a side effect.
	// The shares collection MUST be either an CollectionOrdered or a Collection and MAY be filtered on privileges of
	// an authenticated user or as appropriate when no authentication is given.
	Shares Item
	// The source property is intended to convey some sort of source from which the content markup was derived,
	// as a form of provenance, or to support future editing by clients.
	// In general, clients do the conversion from source to content, not the other way around.
	Source Item
	// Identifies message intended for mentioned recipients
	DirectMessage Item
	// Identifies message intended for list recipients
	ListMessage Item
	// Snesitive status, typically NSFW
	Sensitive Item
	// Identifies one or more entities that represent the total population of entities for which the object can
	// considered to be relevant.
	Audience Item
	// One or more "tags" that have been associated with an objects.
	// A tag can be any kind of Object. The key difference between attachment and tag is that the former implies
	// association by inclusion, while the latter implies associated by reference.
	Tag Item
	// Identifies an entity considered to be part of the public primary audience of an Object
	To Item
	// Identifies an Object that is part of the private primary audience of this Object.
	BTo Item
	// Identifies an Object that is part of the public secondary audience of this Object.
	CC Item
	// Identifies one or more Objects that are part of the private secondary audience of this Object.
	BCC Item
	// When used on a Link, identifies the MIME media type of the referenced resource.
	//
	// When used on an Object, identifies the MIME media type of the value of the content property.
	// If not specified, the content property is assumed to contain text/html content.
	MediaType MediaType
	// Identifies the Object or Link type. Multiple values may be specified.
	Type Type
	// Provides the globally unique identifier for an Object or Link.
	ID IRI
	// Extra extended properties
	Extra
	// When the object describes a time-bound resource, such as an audio or video, a meeting, etc,
	// the duration property indicates the object's approximate duration.
	// The value MUST be expressed as an xsd:duration as defined by [ xmlschema11-2],
	// section 3.3.6 (e.g. a period of 5 seconds is represented as "PT5S").
	Duration time.Duration
}

// Merge implementation
//
//nolint:gocognit,gocyclo
func (o *ObjectBase) Merge(other *ObjectBase) {
	if other == nil {
		return
	}

	o.Extra.Merge(&other.Extra)

	if other.Attachment != nil {
		o.Attachment = other.Attachment
	}

	if other.AttributedTo != nil {
		o.AttributedTo = other.AttributedTo
	}

	if other.Content != nil {
		o.Content = other.Content
	}

	if other.Context != nil {
		o.Context = other.Context
	}

	if other.Name != nil {
		o.Name = other.Name
	}

	if other.Generator != nil {
		o.Generator = other.Generator
	}

	if other.Icon != nil {
		o.Icon = other.Icon
	}

	if other.Image != nil {
		o.Image = other.Image
	}

	if other.InReplyTo != nil {
		o.InReplyTo = other.InReplyTo
	}

	if other.Location != nil {
		o.Location = other.Location
	}

	if other.Preview != nil {
		o.Preview = other.Preview
	}

	if !other.Published.AsTime().IsZero() {
		o.Published = other.Published
	}

	if other.Replies != nil {
		o.Replies = other.Replies
	}

	if other.Summary != nil {
		o.Summary = other.Summary
	}

	if other.URL != nil {
		o.URL = other.URL
	}

	if other.Likes != nil {
		o.Likes = other.Likes
	}

	if other.Shares != nil {
		o.Shares = other.Shares
	}

	if other.Source != nil {
		o.Source = other.Source
	}

	if other.DirectMessage != nil {
		o.DirectMessage = other.DirectMessage
	}

	if other.ListMessage != nil {
		o.ListMessage = other.ListMessage
	}

	if other.MediaType != "" {
		o.MediaType = other.MediaType
	}

	if !other.EndTime.AsTime().IsZero() {
		o.EndTime = other.EndTime
	}

	if !other.StartTime.AsTime().IsZero() {
		o.StartTime = other.StartTime
	}

	if !other.Updated.AsTime().IsZero() {
		o.Updated = other.Updated
	}

	if other.Audience != nil {
		o.Audience = other.Audience
	}

	if other.Tag != nil {
		o.Tag = other.Tag
	}

	if other.To != nil {
		o.To = other.To
	}

	if other.BTo != nil {
		o.BTo = other.BTo
	}

	if other.CC != nil {
		o.CC = other.CC
	}

	if other.BCC != nil {
		o.BCC = other.BCC
	}

	o.Duration = other.Duration
}

// AsType implementation
func (o *ObjectBase) AsType() Type {
	return o.Type
}

// Visibility infers visibility scope of the object
func (o *ObjectBase) Visibility() Visibility {
	var followers, local bool

	tos, _ := o.To.(Items)

	for _, t := range tos {
		if ActorIsPublic(t.AsIRI()) {
			return VisibilityPublic
		}

		if strings.Contains(string(t.AsIRI()), "/followers") {
			followers = true
		}

		if t.AsIRI() == ActorLocal {
			local = true
		}
	}

	ccs, _ := o.CC.(Items)

	for _, t := range ccs {
		if ActorIsPublic(t.AsIRI()) {
			return VisibilityUnlisted
		}
	}

	if local {
		return VisibilityLocal
	}

	if followers {
		return VisibilityPrivate
	}

	if o.DirectMessage.AsBool() {
		return VisibilityDirect
	}

	if o.ListMessage.AsBool() {
		return VisibilityList
	}

	if len(ccs) > 0 {
		return VisibilityPrivate
	}

	return VisibilityDirect
}

// Object Describes an object of any kind.
// The Object type serves as the base type for most of the other kinds of objects defined in the Activity Vocabulary,
// including other Core types such as Activity, ActivityIntransitive, Collection and CollectionOrdered.
//
// Disjoint With: Link
//
// https://www.w3.org/TR/activitystreams-vocabulary/#dfn-object
type Object ObjectBase

// Visibility proxy
func (o *Object) Visibility() Visibility {
	return (*ObjectBase)(o).Visibility()
}

// Merge implementation
func (o *Object) Merge(raw Item) {
	other, _ := raw.(*Object)
	if other != nil {
		(*ObjectBase)(o).Merge((*ObjectBase)(other))
	}
}

// AsIRI implementation
func (o *Object) AsIRI() IRI {
	return o.ID
}

// AsType implementation
func (o *Object) AsType() Type {
	return (*ObjectBase)(o).AsType()
}

// AsBool implementation
func (o *Object) AsBool() bool {
	return o != nil && o.ID != ""
}

// IsZero implementation
//
//nolint:gocyclo
func (o *Object) IsZero(ctx context.Context) bool {
	if o == nil {
		return true
	}

	cfg := ConfigFromContext(ctx)

	return (o.JSONLDContext == nil || o.JSONLDContext.IsZero(ctx)) &&
		(o.AttributedTo == nil || o.AttributedTo.IsZero(ctx)) &&
		(o.Attachment == nil || o.Attachment.IsZero(ctx)) &&
		(o.Context == nil || o.Context.IsZero(ctx)) &&
		(o.Content == nil || o.Content.IsZero(ctx)) &&
		(o.Name == nil || o.Name.IsZero(ctx)) &&
		(o.Generator == nil || o.Generator.IsZero(ctx)) &&
		(o.Icon == nil || o.Icon.IsZero(ctx)) &&
		(o.Image == nil || o.Image.IsZero(ctx)) &&
		(o.InReplyTo == nil || o.InReplyTo.IsZero(ctx)) &&
		(o.Location == nil || o.Location.IsZero(ctx)) &&
		(o.Preview == nil || o.Preview.IsZero(ctx)) &&
		(o.Replies == nil || o.Replies.IsZero(ctx)) &&
		(o.Summary == nil || o.Summary.IsZero(ctx)) &&
		(o.URL == nil || o.URL.IsZero(ctx)) &&
		(o.Likes == nil || o.Likes.IsZero(ctx)) &&
		(o.Shares == nil || o.Shares.IsZero(ctx)) &&
		(o.Source == nil || o.Source.IsZero(ctx)) &&
		(o.Audience == nil || o.Audience.IsZero(ctx)) &&
		(o.Tag == nil || o.Tag.IsZero(ctx)) &&
		(o.To == nil || o.To.IsZero(ctx)) &&
		(o.CC == nil || o.CC.IsZero(ctx)) &&
		(cfg.OperationContext == OperationContextFederation || (o.BTo == nil || o.BTo.IsZero(ctx))) &&
		(cfg.OperationContext == OperationContextFederation || (o.BCC == nil || o.BCC.IsZero(ctx))) &&
		o.ID.IsZero(ctx) &&
		o.Type.IsZero(ctx) &&
		(o.DirectMessage == nil || o.DirectMessage.IsZero(ctx)) &&
		(o.ListMessage == nil || o.ListMessage.IsZero(ctx)) &&
		o.Published.IsZero(ctx) &&
		o.MediaType.IsZero(ctx) &&
		o.EndTime.IsZero(ctx) &&
		o.StartTime.IsZero(ctx) &&
		o.Updated.IsZero(ctx) &&
		o.Duration == 0 &&
		o.Extra.IsZero(ctx)
}

// MarshalFields implementation
func (o *ObjectBase) MarshalFields(ctx context.Context, w *jwriter.Writer, first bool) bool {
	cfg := ConfigFromContext(ctx)

	first = JSONMarshalJSONLDContext(ctx, w, o.JSONLDContext, first)

	if cfg.OperationContext == OperationContextLocal {
		first = JSONMarshalField(ctx, w, o.BTo, "bto", first)
		first = JSONMarshalField(ctx, w, o.BCC, "bcc", first)
	}

	first = JSONMarshalField(ctx, w, o.Attachment, "attachment", first)
	first = JSONMarshalField(ctx, w, o.AttributedTo, "attributedTo", first)
	first = JSONMarshalField(ctx, w, o.Content, "content", first)
	first = JSONMarshalField(ctx, w, o.Context, "context", first)
	first = JSONMarshalField(ctx, w, o.Name, "name", first)
	first = JSONMarshalField(ctx, w, o.Generator, "generator", first)
	first = JSONMarshalField(ctx, w, o.Icon, "icon", first)
	first = JSONMarshalField(ctx, w, o.Image, "image", first)
	first = JSONMarshalField(ctx, w, o.InReplyTo, "inReplyTo", first)
	first = JSONMarshalField(ctx, w, o.Location, "location", first)
	first = JSONMarshalField(ctx, w, o.Preview, "preview", first)
	first = JSONMarshalField(ctx, w, o.Published, "published", first)
	first = JSONMarshalField(ctx, w, o.Replies, "replies", first)
	first = JSONMarshalField(ctx, w, o.Summary, "summary", first)
	first = JSONMarshalField(ctx, w, o.URL, "url", first)
	first = JSONMarshalField(ctx, w, o.Likes, "likes", first)
	first = JSONMarshalField(ctx, w, o.Shares, "shares", first)
	first = JSONMarshalField(ctx, w, o.Source, "source", first)
	first = JSONMarshalField(ctx, w, o.Audience, "audience", first)
	first = JSONMarshalField(ctx, w, o.Tag, "tag", first)
	first = JSONMarshalField(ctx, w, o.To, "to", first)
	first = JSONMarshalField(ctx, w, o.CC, "cc", first)
	first = JSONMarshalField(ctx, w, o.ID, "id", first)
	first = JSONMarshalField(ctx, w, o.Type, fieldType, first)
	first = JSONMarshalField(ctx, w, o.DirectMessage, "directMessage", first)
	first = JSONMarshalField(ctx, w, o.ListMessage, "listMessage", first)
	first = JSONMarshalField(ctx, w, o.MediaType, "mediaType", first)
	first = JSONMarshalField(ctx, w, o.EndTime, "endTime", first)
	first = JSONMarshalField(ctx, w, o.StartTime, "startTime", first)
	first = JSONMarshalField(ctx, w, o.Updated, "updated", first)
	first = JSONMarshalDuration(w, o.Duration, "duration", first)
	first = o.MarshalUnknownsFields(ctx, w, first)

	return first
}

// UnmarshalFields implementation
//
//nolint:gocyclo
func (o *ObjectBase) UnmarshalFields(ctx context.Context, r *jlexer.Lexer) {
	var nr jlexer.Lexer

	var nraw ExtraRaw

	for _, v := range o.Extra.Raw {
		nr = jlexer.Lexer{Data: v.Value}

		switch v.Key {
		case fieldATContext:
			JSONUnmarshalJSONLDContext(ctx, &nr, &o.JSONLDContext)
		case "attachment":
			JSONUnmarshalItem(ctx, &nr, &o.Attachment, false)
		case "attributedTo":
			JSONUnmarshalItem(ctx, &nr, &o.AttributedTo, false)
		case "content", "contentMap":
			JSONUnmarshalValue(ctx, &nr, &o.Content)
		case "context":
			JSONUnmarshalItem(ctx, &nr, &o.Context, false)
		case "name", "nameMap":
			JSONUnmarshalValue(ctx, &nr, &o.Name)
		case "generator":
			JSONUnmarshalItem(ctx, &nr, &o.Generator, false)
		case "icon":
			JSONUnmarshalItem(ctx, &nr, &o.Icon, false)
		case "image":
			JSONUnmarshalItem(ctx, &nr, &o.Image, false)
		case "inReplyTo":
			JSONUnmarshalItem(ctx, &nr, &o.InReplyTo, false)
		case "location":
			JSONUnmarshalItem(ctx, &nr, &o.Location, false)
		case "preview":
			JSONUnmarshalItem(ctx, &nr, &o.Preview, false)
		case "published":
			o.Published.UnmarshalJSONContext(ctx, &nr)
		case "replies":
			JSONUnmarshalItem(ctx, &nr, &o.Replies, false)
		case "summary", "summaryMap":
			JSONUnmarshalValue(ctx, &nr, &o.Summary)
		case "url":
			JSONUnmarshalItem(ctx, &nr, &o.URL, false)
		case "likes":
			JSONUnmarshalItem(ctx, &nr, &o.Likes, false)
		case "shares":
			JSONUnmarshalItem(ctx, &nr, &o.Shares, false)
		case "source":
			JSONUnmarshalItem(ctx, &nr, &o.Source, false)
		case "audience":
			JSONUnmarshalItem(ctx, &nr, &o.Audience, false)
		case "tag":
			JSONUnmarshalItem(ctx, &nr, &o.Tag, false)
		case "to":
			JSONUnmarshalItem(ctx, &nr, &o.To, false)
		case "bto":
			JSONUnmarshalItem(ctx, &nr, &o.BTo, false)
		case "cc":
			JSONUnmarshalItem(ctx, &nr, &o.CC, false)
		case "bcc":
			JSONUnmarshalItem(ctx, &nr, &o.BCC, false)
		case "id":
			var n IRI

			n.UnmarshalJSONContext(ctx, &nr)

			o.ID = n
		case "type":
			var n Type

			n.UnmarshalJSONContext(ctx, &nr)

			o.Type = n
		case "directMessage":
			JSONUnmarshalItem(ctx, &nr, &o.DirectMessage, false)
		case "listMessage":
			JSONUnmarshalItem(ctx, &nr, &o.ListMessage, false)
		case "mediaType":
			var n MediaType

			n.UnmarshalJSONContext(ctx, &nr)

			o.MediaType = n
		case "endTime":
			o.EndTime.UnmarshalJSONContext(ctx, &nr)
		case "startTime":
			o.StartTime.UnmarshalJSONContext(ctx, &nr)
		case "updated":
			o.Updated.UnmarshalJSONContext(ctx, &nr)
		case "duration":
			JSONUnmarshalDuration(&nr, &o.Duration)
		default:
			nraw.Append(v.Key, v.Value)
		}

		r.AddError(nr.Error())
	}

	o.Extra.Raw = nraw
}

// MarshalJSONContext implementation
func (o *Object) MarshalJSONContext(ctx context.Context, w *jwriter.Writer) {
	w.RawByte('{')

	(*ObjectBase)(o).MarshalFields(ctx, w, true)

	w.RawByte('}')
}

// UnmarshalJSONContext implementation
func (o *Object) UnmarshalJSONContext(ctx context.Context, r *jlexer.Lexer) {
	o.Extra.UnmarshalJSONContext(ctx, r)
	(*ObjectBase)(o).UnmarshalFields(ctx, r)
}
