// Package vocab provides activitypub/activitystreams vocabulary as defined by
// https://www.w3.org/TR/activitystreams-vocabulary/
package vocab

import (
	"context"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

// DIDAPNodeLocal activity pub node-local domain, SHOULD NOT be federated
const DIDAPNodeLocal = "did:apnode:local"

// LocalTag IRI
func LocalTag(tag string) IRI {
	return DIDAPNodeLocal + "/tags/" + IRI(tag)
}

// LocalActor IRI
func LocalActor(user string) IRI {
	return DIDAPNodeLocal + "/users/" + IRI(user)
}

// LocalObject IRI
func LocalObject(obj string) IRI {
	return DIDAPNodeLocal + "/objects/" + IRI(obj)
}

// LocalContext IRI
func LocalContext(obj string) IRI {
	return DIDAPNodeLocal + "/contexts/" + IRI(obj)
}

type (
	contextKeyConfigT struct{}
)

var (
	contextKeyConfig = contextKeyConfigT{}
)

// OperationContext enum
type OperationContext int

// OperationContext values
const (
	OperationContextUnknown OperationContext = iota
	// OperationContextLocal if Config is specified, would replace all Instance IRI with did:apinstance:local
	OperationContextLocal
	// OperationContextFederation if Config is specified, would replace all did:apinstance:local with Instance IRI
	OperationContextFederation
)

// Config for de/serialization
type Config struct {
	Instance IRI
	OperationContext
}

// WithConfig creates context with serialization config
func WithConfig(ctx context.Context, config *Config) context.Context {
	return context.WithValue(ctx, contextKeyConfig, config)
}

// ConfigFromContext extracts de/serialization config from context
func ConfigFromContext(ctx context.Context) *Config {
	v, _ := ctx.Value(contextKeyConfig).(*Config)
	if v == nil {
		v = &Config{}
	}

	return v
}

// Unmarshal unmarshals item value
func Unmarshal(ctx context.Context, bs []byte) (item Item, err error) {
	r := jlexer.Lexer{Data: bs}

	JSONUnmarshalItem(ctx, &r, &item, false)

	err = r.Error()

	return
}

// Marshal marshals item value
func Marshal(ctx context.Context, item Item) (bs []byte, err error) {
	return MarshalJSONContext(ctx, item)
}

// UnmarshalJSONContext unmarshals generic value
func UnmarshalJSONContext(ctx context.Context, bs []byte, item UnmarshalerContext) (err error) {
	r := jlexer.Lexer{Data: bs}

	item.UnmarshalJSONContext(ctx, &r)

	err = r.Error()

	return
}

// MarshalJSONContext marshals generic value
func MarshalJSONContext(ctx context.Context, item MarshalerContext) (bs []byte, err error) {
	if item == nil {
		return nullBytes, nil
	}

	w := jwriter.Writer{}

	item.MarshalJSONContext(ctx, &w)

	return w.BuildBytes()
}

// MergeItemList merges two lists of items by their IRIs
func MergeItemList(a, b Items) Items {
	if len(a) == 0 {
		return b
	}

	for _, bel := range b {
		id := bel.AsIRI()

		var found bool

		for _, ael := range a {
			if ael.AsIRI() == id {
				found = true

				break
			}
		}

		if !found {
			a = append(a, bel)
		}
	}

	return a
}
