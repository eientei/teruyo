package vocab

import (
	"context"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

// well-known public actor
const (
	ActorPublicFull   IRI = "https://www.w3.org/ns/activitystreams#Public"
	ActorPublicSchema IRI = "as:Public"
	ActorPublicShort  IRI = "Public"
)

// ActorLocal instance-local actor, SHOULD NOT be federated, can be replaced with current local domain when rendering.
const ActorLocal IRI = DIDAPNodeLocal + "/#Public"

var actorPublic = map[IRI]struct{}{
	ActorPublicFull:   {},
	ActorPublicSchema: {},
	ActorPublicShort:  {},
}

// ActorIsPublic returns true if actor is public
func ActorIsPublic(actor IRI) (ok bool) {
	_, ok = actorPublic[actor]

	return
}

// Actor types are Object types that are capable of performing activities.
type Actor struct {
	Inbox             Item
	Outbox            Item
	Following         Item
	Followers         Item
	Liked             Item
	Streams           Item
	PreferredUsername string
	Endpoints         ActorEndpoints
	ObjectBase
}

// Merge implementation
func (o *Actor) Merge(raw Item) {
	other, _ := raw.(*Actor)
	if other == nil {
		return
	}

	if other.Inbox != nil {
		o.Inbox = other.Inbox
	}

	if other.Outbox != nil {
		o.Outbox = other.Outbox
	}

	if other.Following != nil {
		o.Following = other.Following
	}

	if other.Followers != nil {
		o.Followers = other.Followers
	}

	if other.Liked != nil {
		o.Liked = other.Liked
	}

	if other.Streams != nil {
		o.Streams = other.Streams
	}

	if other.PreferredUsername != "" {
		o.PreferredUsername = other.PreferredUsername
	}

	o.ObjectBase.Merge(&other.ObjectBase)
}

// AsIRI implementation
func (o *Actor) AsIRI() IRI {
	return o.ID
}

// AsType implementation
func (o *Actor) AsType() Type {
	return o.ObjectBase.AsType()
}

// AsBool implementation
func (o *Actor) AsBool() bool {
	return o.ID != ""
}

// IsZero implementation
func (o *Actor) IsZero(ctx context.Context) bool {
	if o == nil {
		return true
	}

	return o.ObjectBase.IsZero(ctx) &&
		(o.Inbox == nil || o.Inbox.IsZero(ctx)) &&
		(o.Outbox == nil || o.Outbox.IsZero(ctx)) &&
		(o.Following == nil || o.Following.IsZero(ctx)) &&
		(o.Followers == nil || o.Followers.IsZero(ctx)) &&
		(o.Likes == nil || o.Likes.IsZero(ctx)) &&
		(o.Streams == nil || o.Streams.IsZero(ctx)) &&
		o.PreferredUsername == "" &&
		o.Endpoints.IsZero(ctx)
}

// MarshalFields implementation
func (o *Actor) MarshalFields(ctx context.Context, w *jwriter.Writer, first bool) bool {
	first = o.ObjectBase.MarshalFields(ctx, w, first)
	first = JSONMarshalField(ctx, w, o.Inbox, "inbox", first)
	first = JSONMarshalField(ctx, w, o.Outbox, "outbox", first)
	first = JSONMarshalField(ctx, w, o.Following, "following", first)
	first = JSONMarshalField(ctx, w, o.Followers, "followers", first)
	first = JSONMarshalField(ctx, w, o.Liked, "liked", first)
	first = JSONMarshalField(ctx, w, o.Streams, "streams", first)
	first = JSONMarshalString(w, o.PreferredUsername, "preferredUsername", first)
	first = JSONMarshalField(ctx, w, &o.Endpoints, "endpoints", first)

	return first
}

// UnmarshalFields implementation
func (o *Actor) UnmarshalFields(ctx context.Context, r *jlexer.Lexer) {
	o.ObjectBase.UnmarshalFields(ctx, r)

	var nr jlexer.Lexer

	var nraw ExtraRaw

	for _, v := range o.Extra.Raw {
		nr = jlexer.Lexer{Data: v.Value}

		switch v.Key {
		case "inbox":
			JSONUnmarshalItem(ctx, &nr, &o.Inbox, false)
		case "outbox":
			JSONUnmarshalItem(ctx, &nr, &o.Outbox, false)
		case "following":
			JSONUnmarshalItem(ctx, &nr, &o.Following, false)
		case "followers":
			JSONUnmarshalItem(ctx, &nr, &o.Followers, false)
		case "liked":
			JSONUnmarshalItem(ctx, &nr, &o.Liked, false)
		case "streams":
			JSONUnmarshalItem(ctx, &nr, &o.Streams, false)
		case "preferredUsername":
			o.PreferredUsername = nr.String()
		case "endpoints":
			o.Endpoints.UnmarshalJSONContext(ctx, &nr)
		default:
			nraw.Append(v.Key, v.Value)

			continue
		}

		r.AddError(nr.Error())
	}

	o.Extra.Raw = nraw
}

// MarshalJSONContext implementation
func (o *Actor) MarshalJSONContext(ctx context.Context, w *jwriter.Writer) {
	w.RawByte('{')

	o.MarshalFields(ctx, w, true)

	w.RawByte('}')
}

// UnmarshalJSONContext implementation
func (o *Actor) UnmarshalJSONContext(ctx context.Context, r *jlexer.Lexer) {
	o.Extra.UnmarshalJSONContext(ctx, r)
	o.UnmarshalFields(ctx, r)
}

// ActorEndpoints A json object which maps additional (typically server/domain-wide) endpoints which may be useful
// either for this actor or someone referencing this actor. This mapping may be nested inside the actor document as
// the value or may be a link to a JSON-LD document with these properties.
type ActorEndpoints struct {
	// JSON-LD context
	JSONLDContext JSONLDContext
	// Endpoint URI so this actor's clients may access remote ActivityStreams objects which require
	// authentication to access. To use this endpoint, the client posts an x-www-form-urlencoded
	// id parameter with the value being the id of the requested ActivityStreams object.
	ProxyURL string
	// If OAuth 2.0 bearer tokens [RFC6749] [RFC6750] are being used for authenticating client to server interactions,
	// this endpoint specifies a URI at which a browser-authenticated user may obtain a new authorization grant.
	OauthAuthorizationEndpoint string
	// If OAuth 2.0 bearer tokens [RFC6749] [RFC6750] are being used for authenticating client to server interactions,
	// this endpoint specifies a URI at which a client may acquire an access token.
	OauthTokenEndpoint string
	// If Linked Data Signatures and HTTP Signatures are being used for authentication and authorization,
	// this endpoint specifies a URI at which browser-authenticated users may authorize a client's public key for
	// client to server interactions.
	ProvideClientKey string
	// If Linked Data Signatures and HTTP Signatures are being used for authentication and authorization,
	// this endpoint specifies a URI at which a client key may be signed by the actor's key for a time window to
	// act on behalf of the actor in interacting with foreign servers.
	SignClientKey string
	// An optional endpoint used for wide delivery of publicly addressed activities and activities sent to followers.
	// sharedInbox endpoints SHOULD also be publicly readable OrderedCollection objects containing objects addressed to
	// the Public special collection. Reading from the sharedInbox endpoint MUST NOT present objects which are not
	// addressed to the Public endpoint.
	SharedInbox string
	// Extra extended properties
	Extra
}

// IsZero implementation
func (o *ActorEndpoints) IsZero(ctx context.Context) bool {
	return o.OauthAuthorizationEndpoint == "" &&
		o.OauthTokenEndpoint == "" &&
		o.ProxyURL == "" &&
		o.SharedInbox == "" &&
		o.SignClientKey == "" &&
		o.ProvideClientKey == "" &&
		o.JSONLDContext == nil &&
		o.Extra.IsZero(ctx)
}

// MarshalJSONContext implementation
func (o *ActorEndpoints) MarshalJSONContext(ctx context.Context, w *jwriter.Writer) {
	w.RawByte('{')

	first := true

	first = JSONMarshalJSONLDContext(ctx, w, o.JSONLDContext, first)
	first = JSONMarshalString(w, o.ProxyURL, "proxyUrl", first)
	first = JSONMarshalString(w, o.OauthAuthorizationEndpoint, "oauthAuthorizationEndpoint", first)
	first = JSONMarshalString(w, o.OauthTokenEndpoint, "oauthTokenEndpoint", first)
	first = JSONMarshalString(w, o.ProvideClientKey, "provideClientKey", first)
	first = JSONMarshalString(w, o.SignClientKey, "signClientKey", first)
	first = JSONMarshalString(w, o.SharedInbox, "sharedInbox", first)
	o.MarshalUnknownsFields(ctx, w, first)

	w.RawByte('}')
}

// UnmarshalJSONContext implementation
func (o *ActorEndpoints) UnmarshalJSONContext(ctx context.Context, r *jlexer.Lexer) {
	start := r.IsStart()

	if !JSONStart(r) {
		*o = ActorEndpoints{}

		return
	}

	r.Delim('{')

	o.Extra.Reset()

	for r.Ok() && !r.IsDelim('}') {
		key, ok := JSONUnmarshalFieldPrefix(r)
		if !ok {
			continue
		}

		switch key {
		case fieldATContext:
			JSONUnmarshalJSONLDContext(ctx, r, &o.JSONLDContext)
		case "proxyUrl":
			o.ProxyURL = r.String()
		case "oauthAuthorizationEndpoint":
			o.OauthAuthorizationEndpoint = r.String()
		case "oauthTokenEndpoint":
			o.OauthTokenEndpoint = r.String()
		case "provideClientKey":
			o.ProvideClientKey = r.String()
		case "signClientKey":
			o.SignClientKey = r.String()
		case "sharedInbox":
			o.SharedInbox = r.String()
		default:
			o.Raw.Append(key, r.Raw())
		}

		r.WantComma()
	}

	r.Delim('}')

	JSONEnd(r, start)
}
