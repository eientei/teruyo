package vocab

import (
	"context"
	"testing"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
	"github.com/stretchr/testify/assert"
)

type testctx struct {
	JSONLDContext
}

func (o *testctx) MarshalJSONContext(ctx context.Context, w *jwriter.Writer) {
	w.RawByte('{')

	JSONMarshalJSONLDContext(ctx, w, o.JSONLDContext, true)

	w.RawByte('}')
}

func (o *testctx) UnmarshalJSONContext(_ context.Context, r *jlexer.Lexer) {
	start := r.IsStart()

	if !JSONStart(r) {
		*o = testctx{}

		return
	}

	r.Delim('{')

	for r.Ok() && !r.IsDelim('}') {
		key := r.String()

		r.WantColon()

		if r.IsNull() {
			r.Skip()
			r.WantComma()

			continue
		}

		if key == "@context" {
			JSONUnmarshalJSONLDContext(context.Background(), r, &o.JSONLDContext)
		}

		r.WantComma()
	}

	r.Delim('}')

	JSONEnd(r, start)
}

func TestJSONLDContextString(t *testing.T) {
	a := testctx{
		JSONLDContext: JSONLDContextString("foo"),
	}

	bs, err := MarshalJSONContext(context.Background(), &a)
	assert.NoError(t, err)

	var b testctx

	err = UnmarshalJSONContext(context.Background(), bs, &b)
	assert.NoError(t, err)

	assert.EqualValues(t, &a, &b)
}

func TestJSONLDContextData(t *testing.T) {
	a := testctx{
		JSONLDContext: JSONLDContextData{
			"foo": "bar",
		},
	}

	bs, err := MarshalJSONContext(context.Background(), &a)
	assert.NoError(t, err)

	var b testctx

	err = UnmarshalJSONContext(context.Background(), bs, &b)
	assert.NoError(t, err)

	assert.EqualValues(t, &a, &b)
}

func TestJSONLDContextList(t *testing.T) {
	a := testctx{
		JSONLDContext: JSONLDContextList{
			JSONLDContextString("foo"),
			JSONLDContextString("bar"),
		},
	}

	bs, err := MarshalJSONContext(context.Background(), &a)
	assert.NoError(t, err)

	var b testctx

	err = UnmarshalJSONContext(context.Background(), bs, &b)
	assert.NoError(t, err)

	assert.EqualValues(t, &a, &b)
}
