package vocab

import (
	"context"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

// Type provides enumeration of all core and descendant types
type Type string

// Type values
const (
	TypeUnknown                 Type = ""
	TypeObject                  Type = "Object"
	TypeObjectArticle           Type = "Article"
	TypeObjectAudio             Type = "Audio"
	TypeObjectDocument          Type = "Document"
	TypeObjectEvent             Type = "Event"
	TypeObjectImage             Type = "Image"
	TypeObjectNote              Type = "Note"
	TypeObjectPage              Type = "Page"
	TypeObjectPlace             Type = "Place"
	TypeObjectProfile           Type = "Profile"
	TypeObjectRelationship      Type = "Relationship"
	TypeObjectTombstone         Type = "Tombstone"
	TypeObjectVideo             Type = "Video"
	TypeLink                    Type = "Link"
	TypeLinkMention             Type = "Mention"
	TypeLinkHashtag             Type = "Hashtag"
	TypeActivity                Type = "Activity"
	TypeActivityIntransitive    Type = "IntransitiveActivity"
	TypeActivityAccept          Type = "Accept"
	TypeActivityAcceptTentative Type = "TentativeAccept"
	TypeActivityAdd             Type = "Add"
	TypeActivityAnnounce        Type = "Announce"
	TypeActivityArrive          Type = "Arrive"
	TypeActivityBlock           Type = "Block"
	TypeActivityCreate          Type = "Create"
	TypeActivityDelete          Type = "Delete"
	TypeActivityDislike         Type = "Dislike"
	TypeActivityFlag            Type = "Flag"
	TypeActivityFollow          Type = "Follow"
	TypeActivityIgnore          Type = "Ignore"
	TypeActivityInvite          Type = "Invite"
	TypeActivityJoin            Type = "Join"
	TypeActivityLeave           Type = "Leave"
	TypeActivityLike            Type = "Like"
	TypeActivityListen          Type = "Listen"
	TypeActivityMove            Type = "Move"
	TypeActivityOffer           Type = "Offer"
	TypeActivityQuestion        Type = "Question"
	TypeActivityReject          Type = "Reject"
	TypeActivityRejectTentative Type = "TentativeReject"
	TypeActivityRead            Type = "Read"
	TypeActivityRemove          Type = "Remove"
	TypeActivityTravel          Type = "Travel"
	TypeActivityUndo            Type = "Undo"
	TypeActivityUpdate          Type = "Update"
	TypeActivityView            Type = "View"
	TypeActor                   Type = "Actor"
	TypeActorApplication        Type = "Application"
	TypeActorGroup              Type = "Group"
	TypeActorOrganization       Type = "Organization"
	TypeActorPerson             Type = "Person"
	TypeActorService            Type = "Service"
	TypeCollection              Type = "Collection"
	TypeCollectionOrdered       Type = "OrderedCollection"
	TypeCollectionPage          Type = "CollectionPage"
	TypeCollectionOrderedPage   Type = "OrderedCollectionPage"
)

// Root returns root core type
func (t Type) Root() Type {
	switch t {
	case TypeActivity,
		TypeActivityAccept,
		TypeActivityAdd,
		TypeActivityAnnounce,
		TypeActivityArrive,
		TypeActivityBlock,
		TypeActivityCreate,
		TypeActivityDelete,
		TypeActivityDislike,
		TypeActivityFlag,
		TypeActivityFollow,
		TypeActivityIgnore,
		TypeActivityInvite,
		TypeActivityJoin,
		TypeActivityLeave,
		TypeActivityLike,
		TypeActivityListen,
		TypeActivityMove,
		TypeActivityOffer,
		TypeActivityQuestion,
		TypeActivityReject,
		TypeActivityRead,
		TypeActivityRemove,
		TypeActivityRejectTentative,
		TypeActivityAcceptTentative,
		TypeActivityTravel,
		TypeActivityUndo,
		TypeActivityUpdate,
		TypeActivityView,
		TypeActivityIntransitive:
		return TypeActivity
	case TypeActor,
		TypeActorApplication,
		TypeActorGroup,
		TypeActorOrganization,
		TypeActorPerson,
		TypeActorService:
		return TypeActor
	case TypeCollection,
		TypeCollectionOrdered,
		TypeCollectionPage,
		TypeCollectionOrderedPage:
		return TypeCollection
	case TypeObject,
		TypeObjectArticle,
		TypeObjectAudio,
		TypeObjectDocument,
		TypeObjectEvent,
		TypeObjectImage,
		TypeObjectNote,
		TypeObjectPage,
		TypeObjectPlace,
		TypeObjectProfile,
		TypeObjectRelationship,
		TypeObjectTombstone,
		TypeObjectVideo:
		return TypeObject
	case TypeLink,
		TypeLinkMention,
		TypeLinkHashtag:
		return TypeLink
	default:
		return TypeUnknown
	}
}

// Core returns corresponding core type
func (t Type) Core() Type {
	switch t {
	case TypeObject,
		TypeLink,
		TypeActor,
		TypeActivity,
		TypeActivityIntransitive,
		TypeActivityQuestion,
		TypeCollection,
		TypeCollectionOrdered,
		TypeCollectionPage,
		TypeCollectionOrderedPage:
		return t
	default:
		return t.Root()
	}
}

// Parent returns corresponding parent type
func (t Type) Parent() Type {
	switch t {
	case TypeCollectionOrderedPage:
		return TypeCollectionPage
	case TypeCollectionPage,
		TypeCollectionOrdered:
		return TypeCollection
	case TypeActivityArrive,
		TypeActivityTravel,
		TypeActivityQuestion:
		return TypeActivityIntransitive
	case TypeActivityAcceptTentative:
		return TypeActivityAccept
	case TypeActivityRejectTentative:
		return TypeActivityReject
	case TypeActivityInvite:
		return TypeActivityOffer
	case TypeActivityBlock:
		return TypeActivityIgnore
	default:
		return t.Root()
	}
}

// IsZero implementation
func (t Type) IsZero(context.Context) bool {
	return t == ""
}

// MarshalJSONContext implementation
func (t Type) MarshalJSONContext(_ context.Context, w *jwriter.Writer) {
	w.String(string(t))
}

// UnmarshalJSONContext implementation
func (t *Type) UnmarshalJSONContext(_ context.Context, j *jlexer.Lexer) {
	*t = Type(j.String())
}
