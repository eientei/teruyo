package vocab

import (
	"context"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
	"golang.org/x/text/language"
)

// A Link is an indirect, qualified reference to a resource identified by a URL.
// The fundamental model for links is established by [ RFC5988]. Many of the properties defined by the
// Activity Vocabulary allow values that are either instances of Object or Link. When a Link is used,
// it establishes a qualified relation connecting the subject (the containing object) to the resource
// identified by the href. Properties of the Link are properties of the reference as opposed to properties
// of the resource.
//
// Disjoint With: Object
//
// https://www.w3.org/TR/activitystreams-vocabulary/#dfn-link
type Link struct {
	// JSON-LD context
	JSONLDContext JSONLDContext
	// A simple, human-readable, plain-text name for the object.
	// HTML markup MUST NOT be included.
	// The name MAY be expressed using multiple language-tagged values.
	Name Value
	// Identifies an entity that provides a preview of this object.
	Preview Item
	// Hints as to the language used by the target resource. Value MUST be a [BCP47] Language-Tag.
	HrefLang language.Tag
	// Provides the globally unique identifier for an Object or Link.
	ID IRI
	// Identifies the Object or Link type. Multiple values may be specified.
	Type Type
	// A link relation associated with a Link.
	// The value MUST conform to both the [HTML5] and [RFC5988] "link relation" definitions.
	//
	// In the [HTML5], any string not containing the
	// "space" U+0020, "tab" (U+0009), "LF" (U+000A), "FF" (U+000C), "CR" (U+000D) or "," (U+002C) characters
	// can be used as a valid link relation.
	Rel IRI
	// The target resource pointed to by a Link.
	Href IRI
	// When used on a Link, identifies the MIME media type of the referenced resource.
	//
	// When used on an Object, identifies the MIME media type of the value of the content property.
	// If not specified, the content property is assumed to contain text/html content.
	MediaType MediaType
	// Extra extended properties
	Extra
	// On a Link, specifies a hint as to the rendering height in device-independent pixels of the linked resource.
	Height uint
	// On a Link, specifies a hint as to the rendering width in device-independent pixels of the linked resource.
	Width uint
}

// Merge implementation
func (o *Link) Merge(raw Item) {
	other, _ := raw.(*Link)
	if other == nil {
		return
	}

	if other.Preview != nil {
		o.Preview = other.Preview
	}

	if other.Name != nil {
		o.Name = other.Name
	}

	if other.Preview != nil {
		o.Preview = other.Preview
	}

	if other.HrefLang != language.Und {
		o.HrefLang = other.HrefLang
	}

	if other.Rel != "" {
		o.Rel = other.Rel
	}

	if other.Href != "" {
		o.Href = other.Href
	}

	if other.MediaType != "" {
		o.MediaType = other.MediaType
	}

	o.Extra.Merge(&other.Extra)

	o.Width = other.Width
	o.Height = other.Height
}

// AsIRI implementation
func (o *Link) AsIRI() IRI {
	return o.ID
}

// AsType implementation
func (o *Link) AsType() Type {
	return o.Type
}

// AsBool implementation
func (o *Link) AsBool() bool {
	return o.ID != "" || o.Href != ""
}

// IsZero implementation
func (o *Link) IsZero(ctx context.Context) bool {
	if o == nil {
		return true
	}

	return (o.JSONLDContext == nil || o.JSONLDContext.IsZero(ctx)) &&
		o.Type.IsZero(ctx) &&
		o.ID.IsZero(ctx) &&
		o.Rel.IsZero(ctx) &&
		o.Href.IsZero(ctx) &&
		o.HrefLang == language.Und &&
		o.MediaType.IsZero(ctx) &&
		(o.Name == nil || o.Name.IsZero(ctx)) &&
		(o.Preview == nil || o.Preview.IsZero(ctx)) &&
		o.Width == 0 &&
		o.Height == 0 &&
		o.Extra.IsZero(ctx)
}

// MarshalFields implementation
func (o *Link) MarshalFields(ctx context.Context, w *jwriter.Writer, first bool) bool {
	first = JSONMarshalJSONLDContext(ctx, w, o.JSONLDContext, first)
	first = JSONMarshalField(ctx, w, o.Type, fieldType, first)
	first = JSONMarshalField(ctx, w, o.ID, "id", first)
	first = JSONMarshalField(ctx, w, o.Rel, "rel", first)
	first = JSONMarshalField(ctx, w, o.Href, "href", first)
	first = JSONMarshalLanguage(w, o.HrefLang, "hreflang", first)
	first = JSONMarshalField(ctx, w, o.MediaType, "mediaType", first)
	first = JSONMarshalField(ctx, w, o.Name, "name", first)
	first = JSONMarshalField(ctx, w, o.Preview, "preview", first)
	first = JSONMarshalUint(w, o.Width, "width", first)
	first = JSONMarshalUint(w, o.Height, "height", first)
	first = o.MarshalUnknownsFields(ctx, w, first)

	return first
}

// UnmarshalFields implementation
func (o *Link) UnmarshalFields(ctx context.Context, r *jlexer.Lexer) {
	var nr jlexer.Lexer

	var nraw ExtraRaw

	for _, v := range o.Extra.Raw {
		nr = jlexer.Lexer{Data: v.Value}

		switch v.Key {
		case fieldATContext:
			JSONUnmarshalJSONLDContext(ctx, &nr, &o.JSONLDContext)
		case fieldType:
			var t Type

			t.UnmarshalJSONContext(ctx, &nr)

			o.Type = t
		case "id":
			var t IRI

			t.UnmarshalJSONContext(ctx, &nr)

			o.ID = t
		case "rel":
			var t IRI

			t.UnmarshalJSONContext(ctx, &nr)

			o.Rel = t
		case "href":
			var t IRI

			t.UnmarshalJSONContext(ctx, &nr)

			o.Href = t
		case "hreflang":
			JSONUnmarshalLanguage(&nr, &o.HrefLang)
		case "mediaType":
			o.MediaType = MediaType(nr.String())
		case "name", "nameMap":
			JSONUnmarshalValue(ctx, &nr, &o.Name)
		case "preview":
			JSONUnmarshalItem(ctx, &nr, &o.Preview, false)
		case "width":
			o.Width = nr.Uint()
		case "height":
			o.Height = nr.Uint()
		default:
			nraw.Append(v.Key, v.Value)

			continue
		}

		r.AddError(nr.Error())
	}

	o.Extra.Raw = nraw
}

// MarshalJSONContext implementation
func (o *Link) MarshalJSONContext(ctx context.Context, w *jwriter.Writer) {
	w.RawByte('{')

	o.MarshalFields(ctx, w, true)

	w.RawByte('}')
}

// UnmarshalJSONContext implementation
func (o *Link) UnmarshalJSONContext(ctx context.Context, r *jlexer.Lexer) {
	o.Extra.UnmarshalJSONContext(ctx, r)
	o.UnmarshalFields(ctx, r)
}
