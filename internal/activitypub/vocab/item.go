package vocab

import (
	"context"
	"time"

	"github.com/mailru/easyjson/jlexer"
)

const fieldType = "type"

// Item a common interface implemented by all activity pub types
type Item interface {
	AsIRI() IRI
	AsBool() bool
	AsType() Type
	Merge(o Item)
	MarshalerContext
}

// JSONUnmarshalItem unmarshals Item field
func JSONUnmarshalItem(ctx context.Context, r *jlexer.Lexer, item *Item, attemptTime bool) {
	switch {
	case r.IsNull():
		r.Skip()

		*item = nil

	case r.IsDelim('{'):
		var nraw ExtraRaw

		nraw.UnmarshalJSONContext(ctx, r)

		var t string

		if len(nraw) > 0 && nraw[0].Key == fieldType {
			t = (&jlexer.Lexer{Data: nraw[0].Value}).String()
		}

		switch Type(t).Core() {
		case TypeLink:
			obj := Link{
				Extra: Extra{
					Raw: nraw,
				},
			}

			obj.UnmarshalFields(ctx, r)

			*item = &obj
		case TypeActor:
			obj := Actor{
				ObjectBase: ObjectBase{
					Extra: Extra{
						Raw: nraw,
					},
				},
			}

			obj.UnmarshalFields(ctx, r)

			*item = &obj
		case TypeActivity:
			obj := Activity{
				ObjectBase: ObjectBase{
					Extra: Extra{
						Raw: nraw,
					},
				},
			}

			obj.UnmarshalFields(ctx, r)

			*item = &obj
		case TypeActivityIntransitive:
			obj := ActivityIntransitive{
				ObjectBase: ObjectBase{
					Extra: Extra{
						Raw: nraw,
					},
				},
			}

			obj.UnmarshalFields(ctx, r)

			*item = &obj
		case TypeActivityQuestion:
			obj := ActivityQuestion{
				ActivityIntransitive: ActivityIntransitive{
					ObjectBase: ObjectBase{
						Extra: Extra{
							Raw: nraw,
						},
					},
				},
			}

			obj.UnmarshalFields(ctx, r)

			*item = &obj
		case TypeCollection:
			obj := Collection{
				ObjectBase: ObjectBase{
					Extra: Extra{
						Raw: nraw,
					},
				},
			}

			obj.UnmarshalFields(ctx, r)

			*item = &obj
		case TypeCollectionOrdered:
			obj := CollectionOrdered{
				ObjectBase: ObjectBase{
					Extra: Extra{
						Raw: nraw,
					},
				},
			}

			obj.UnmarshalFields(ctx, r)

			*item = &obj
		case TypeCollectionPage:
			obj := CollectionPage{
				Collection: Collection{
					ObjectBase: ObjectBase{
						Extra: Extra{
							Raw: nraw,
						},
					},
				},
			}

			obj.UnmarshalFields(ctx, r)

			*item = &obj
		case TypeCollectionOrderedPage:
			obj := CollectionOrderedPage{
				Collection: Collection{
					ObjectBase: ObjectBase{
						Extra: Extra{
							Raw: nraw,
						},
					},
				},
			}

			obj.UnmarshalFields(ctx, r)

			*item = &obj
		default:
			obj := Object{
				Extra: Extra{
					Raw: nraw,
				},
			}

			(*ObjectBase)(&obj).UnmarshalFields(ctx, r)

			*item = &obj
		}
	case r.IsDelim('['):
		r.Delim('[')

		var arr Items

		if *item == nil {
			if !r.IsDelim(']') {
				arr = make(Items, 0, 4)
			} else {
				arr = Items{}
			}
		} else if item != nil {
			if oarr, ok := (*item).(Items); ok {
				arr = oarr[:0]
			} else {
				arr = make(Items, 0, 4)
			}
		}

		for r.Ok() && !r.IsDelim(']') {
			var el Item

			JSONUnmarshalItem(ctx, r, &el, attemptTime)

			arr = append(arr, el)

			r.WantComma()
		}

		*item = arr

		r.Delim(']')
	case r.CurrentToken() == jlexer.TokenString:
		s := r.UnsafeString()

		if attemptTime {
			ts, err := time.Parse(time.RFC3339, s)
			if err == nil {
				*item = Time(ts)
			} else {
				*item = IRI(s)
			}
		} else {
			*item = IRI(s)
		}
	case r.CurrentToken() == jlexer.TokenBool:
		*item = Bool(r.Bool())
	default:
		r.AddError(errInvalidJSON)
	}
}
