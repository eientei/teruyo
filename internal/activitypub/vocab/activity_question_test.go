package vocab

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestActivityQuestionUnmarshalExample40(t *testing.T) {
	a := &ActivityQuestion{
		OneOf: Items{
			&Object{
				Type: TypeObjectNote,
				Name: ValueString("Option A"),
			},
			&Object{
				Type: TypeObjectNote,
				Name: ValueString("Option B"),
			},
		},
		AnyOf:  nil,
		Closed: nil,
		ActivityIntransitive: ActivityIntransitive{
			Actor:      nil,
			Object:     nil,
			Target:     nil,
			Result:     nil,
			Origin:     nil,
			Instrument: nil,
			ObjectBase: ObjectBase{
				JSONLDContext: JSONLDContextString("https://www.w3.org/ns/activitystreams"),
				Type:          TypeActivityQuestion,
				Name:          ValueString("What is the answer?"),
			},
		},
	}

	const data = `
{
  "@context": "https://www.w3.org/ns/activitystreams",
  "type": "Question",
  "name": "What is the answer?",
  "oneOf": [
    {
      "type": "Note",
      "name": "Option A"
    },
    {
      "type": "Note",
      "name": "Option B"
    }
  ]
}
`

	var b ActivityQuestion

	err := UnmarshalJSONContext(context.Background(), ([]byte)(data), &b)
	assert.NoError(t, err)

	assert.EqualValues(t, a, &b)
}

func TestActivityQuestionUnmarshalExample41(t *testing.T) {
	a := &ActivityQuestion{
		OneOf:  nil,
		AnyOf:  nil,
		Closed: Time(time.Date(2016, time.May, 10, 0, 0, 0, 0, time.UTC)),
		ActivityIntransitive: ActivityIntransitive{
			Actor:      nil,
			Object:     nil,
			Target:     nil,
			Result:     nil,
			Origin:     nil,
			Instrument: nil,
			ObjectBase: ObjectBase{
				JSONLDContext: JSONLDContextString("https://www.w3.org/ns/activitystreams"),
				Type:          TypeActivityQuestion,
				Name:          ValueString("What is the answer?"),
			},
		},
	}

	const data = `
{
  "@context": "https://www.w3.org/ns/activitystreams",
  "type": "Question",
  "name": "What is the answer?",
  "closed": "2016-05-10T00:00:00Z"
}
`

	var b ActivityQuestion

	err := UnmarshalJSONContext(context.Background(), ([]byte)(data), &b)
	assert.NoError(t, err)

	assert.EqualValues(t, a, &b)
}
