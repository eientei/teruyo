package vocab

import (
	"context"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

// ActivityIntransitive Instances of ActivityIntransitive are a subtype of Activity representing intransitive actions.
// The object property is therefore inappropriate for these activities.
//
// https://www.w3.org/TR/activitystreams-vocabulary/#dfn-intransitiveactivity
type ActivityIntransitive Activity

// Merge implementation
func (o *ActivityIntransitive) Merge(raw Item) {
	intr, _ := raw.(*ActivityIntransitive)
	if intr != nil {
		raw = (*Activity)(intr)
	}

	(*Activity)(o).Merge(raw)
}

// AsIRI implementation
func (o *ActivityIntransitive) AsIRI() IRI {
	return (*Activity)(o).AsIRI()
}

// AsType implementation
func (o *ActivityIntransitive) AsType() Type {
	return (*Activity)(o).AsType()
}

// AsBool implementation
func (o *ActivityIntransitive) AsBool() bool {
	return (*Activity)(o).AsBool()
}

// IsZero implementation
func (o *ActivityIntransitive) IsZero(ctx context.Context) bool {
	return (*Activity)(o).IsZero(ctx)
}

// MarshalFields implementation
func (o *ActivityIntransitive) MarshalFields(ctx context.Context, w *jwriter.Writer, first bool) bool {
	o.Object = nil

	return (*Activity)(o).MarshalFields(ctx, w, first)
}

// UnmarshalFields implementation
func (o *ActivityIntransitive) UnmarshalFields(ctx context.Context, r *jlexer.Lexer) {
	(*Activity)(o).UnmarshalFields(ctx, r)

	o.Object = nil
}

// MarshalJSONContext implementation
func (o *ActivityIntransitive) MarshalJSONContext(ctx context.Context, w *jwriter.Writer) {
	o.Object = nil

	(*Activity)(o).MarshalJSONContext(ctx, w)
}

// UnmarshalJSONContext implementation
func (o *ActivityIntransitive) UnmarshalJSONContext(ctx context.Context, r *jlexer.Lexer) {
	(*Activity)(o).UnmarshalJSONContext(ctx, r)

	o.Object = nil
}
