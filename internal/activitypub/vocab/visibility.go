package vocab

// Visibility object visibility
type Visibility int

const (
	// VisibilityUnknown undetermined visibility scope
	VisibilityUnknown Visibility = 0
	// VisibilityPublic public scope https://www.w3.org/ns/activitystreams#Public mentioned in `to`
	VisibilityPublic Visibility = 1
	// VisibilityLocal instance-local scope, not federated
	VisibilityLocal Visibility = 2
	// VisibilityUnlisted public scope https://www.w3.org/ns/activitystreams#Public only mentioned in `cc`
	VisibilityUnlisted Visibility = 3
	// VisibilityPrivate delivered to followers only
	VisibilityPrivate Visibility = 4
	// VisibilityList delivered to list members only
	VisibilityList Visibility = 5
	// VisibilityDirect delivered to mentioned recipients only
	VisibilityDirect Visibility = 6
)

// String implementation
func (v Visibility) String() string {
	return visibilityValueName[v]
}

// ParseVisibility string parser implementation
func ParseVisibility(s string) Visibility {
	return visibilityNameValue[s]
}

var visibilityValueName = map[Visibility]string{
	VisibilityUnknown:  "",
	VisibilityPublic:   "public",
	VisibilityLocal:    "local",
	VisibilityUnlisted: "unlisted",
	VisibilityPrivate:  "private",
	VisibilityList:     "list",
	VisibilityDirect:   "direct",
}

var visibilityNameValue = map[string]Visibility{
	"":         VisibilityUnknown,
	"public":   VisibilityPublic,
	"local":    VisibilityLocal,
	"unlisted": VisibilityUnlisted,
	"private":  VisibilityPrivate,
	"list":     VisibilityList,
	"direct":   VisibilityDirect,
}
