package vocab

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestCollectionOrderedMarshalEmpty(t *testing.T) {
	link := &CollectionOrdered{}

	bs, err := MarshalJSONContext(context.Background(), link)
	assert.NoError(t, err)

	assert.Equal(t, `{}`, string(bs))

	err = UnmarshalJSONContext(context.Background(), bs, link)
	assert.NoError(t, err)
}

func TestCollectionOrderedMarshalFilled(t *testing.T) {
	a := CollectionOrdered{
		Current: IRI("b1"),
		First:   IRI("b2"),
		Last:    IRI("b3"),
		Items:   Items{IRI("b4")},
		ObjectBase: ObjectBase{
			Extra: Extra{
				Data: map[string]interface{}{
					"foo": float64(1),
					"bar": "aaa",
					"baz": map[string]interface{}{
						"abc": "efg",
						"efg": float64(34),
					},
				},
			},
			JSONLDContext: JSONLDContextString("ggg"),
			Attachment:    IRI("a1"),
			AttributedTo:  IRI("a2"),
			Content:       ValueString("a3"),
			Context:       IRI("a4"),
			Name:          ValueMap{"en": "a5"},
			Generator:     IRI("a6"),
			Icon:          IRI("a7"),
			Image:         IRI("a8"),
			InReplyTo: &Object{
				ID: "a9",
			},
			Location:  IRI("a10"),
			Preview:   IRI("a11"),
			Published: Time(time.Unix(150, 0)),
			Replies:   IRI("a13"),
			Summary:   ValueString("a14"),
			URL:       IRI("a15"),
			Likes:     IRI("a16"),
			Shares:    IRI("a17"),
			Source:    IRI("a18"),
			Audience: Items{
				IRI("a19-1"),
				&Object{
					ID: "a19-2",
				},
			},
			Tag:       Items{IRI("a20")},
			To:        Items{IRI("a21")},
			BTo:       Items{IRI("a22")},
			CC:        Items{IRI("a23")},
			BCC:       Items{IRI("a24")},
			ID:        "a25",
			Type:      "a26",
			MediaType: "a27",
			EndTime:   Time(time.Unix(100, 0)),
			StartTime: Time(time.Unix(50, 0)),
			Updated:   Time(time.Unix(75, 0)),
			Duration:  time.Minute,
		},
		TotalItems: 1,
	}

	ctx := WithConfig(context.Background(), &Config{Instance: "example.com", OperationContext: OperationContextLocal})

	bs, err := MarshalJSONContext(ctx, &a)
	assert.NoError(t, err)

	var b CollectionOrdered

	err = UnmarshalJSONContext(ctx, bs, &b)
	assert.NoError(t, err)

	b.AsDataResetRaw()

	assert.EqualValues(t, a, b)
}

func TestCollectionOrderedUnmarshalExample6(t *testing.T) {
	a := &CollectionOrdered{
		Items: Items{
			&Object{
				Type: TypeObjectNote,
				Name: ValueString("A Simple Note"),
			},
			&Object{
				Type: TypeObjectNote,
				Name: ValueString("Another Simple Note"),
			},
		},
		ObjectBase: ObjectBase{
			JSONLDContext: JSONLDContextString("https://www.w3.org/ns/activitystreams"),
			Type:          TypeCollectionOrdered,
			Summary:       ValueString("Sally's notes"),
		},
		TotalItems: 2,
	}

	const data = `
{
  "@context": "https://www.w3.org/ns/activitystreams",
  "summary": "Sally's notes",
  "type": "OrderedCollection",
  "totalItems": 2,
  "orderedItems": [
    {
      "type": "Note",
      "name": "A Simple Note"
    },
    {
      "type": "Note",
      "name": "Another Simple Note"
    }
  ]
}
`

	var b CollectionOrdered

	err := UnmarshalJSONContext(context.Background(), ([]byte)(data), &b)
	assert.NoError(t, err)

	assert.EqualValues(t, a, &b)
}
