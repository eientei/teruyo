package vocab

import (
	"context"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

// Bool provides wrapper for bool implementing Item and Closed interfaces
type Bool bool

// AsBool returns wrapped bool
func (t Bool) AsBool() bool {
	return bool(t)
}

// IsZero implementation
func (t Bool) IsZero(context.Context) bool {
	return !bool(t)
}

// Closed implementation
func (t Bool) Closed() bool {
	return bool(t)
}

// AsIRI implementation
func (t Bool) AsIRI() IRI {
	if t {
		return "true"
	}

	return "false"
}

// Merge implementation
func (t Bool) Merge(Item) {

}

// AsType implementation
func (t Bool) AsType() Type {
	return ""
}

// MarshalJSONContext implementation
func (t Bool) MarshalJSONContext(_ context.Context, w *jwriter.Writer) {
	w.Bool(bool(t))
}

// UnmarshalJSONContext implementation
func (t *Bool) UnmarshalJSONContext(_ context.Context, w *jlexer.Lexer) {
	*t = Bool(w.Bool())
}
