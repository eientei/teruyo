package vocab

import (
	"context"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

// A Collection is a subtype of Object that represents ordered or unordered sets of Object or Link instances.
//
// https://www.w3.org/TR/activitystreams-vocabulary/#dfn-collection
type Collection struct {
	// In a paged Collection, indicates the page that contains the most recently updated member items.
	Current Item
	// In a paged Collection, indicates the furthest preceeding page of items in the collection.
	First Item
	// In a paged Collection, indicates the furthest proceeding page of the collection.
	Last Item
	// Identifies the items contained in a collection. The items might be ordered or unordered.
	Items Item
	// ObjectBase inherited fields
	ObjectBase
	// A non-negative integer specifying the total number of objects contained by the logical view of the collection.
	// This number might not reflect the actual number of items serialized within the Collection object instance.
	TotalItems uint
}

// Merge implementation
func (o *Collection) Merge(raw Item) {
	other, _ := raw.(*Collection)
	if other == nil {
		return
	}

	if other.Current != nil {
		o.Current = other.Current
	}

	if other.First != nil {
		o.First = other.First
	}

	if other.Last != nil {
		o.Last = other.Last
	}

	if other.Items != nil {
		o.Items = other.Items
		o.TotalItems = other.TotalItems
	}

	o.ObjectBase.Merge(&other.ObjectBase)
}

// AsIRI implementation
func (o *Collection) AsIRI() IRI {
	return o.ID
}

// AsType implementation
func (o *Collection) AsType() Type {
	return o.ObjectBase.AsType()
}

// AsBool implementation
func (o *Collection) AsBool() bool {
	return o.ID != ""
}

// IsZero implementation
func (o *Collection) IsZero(ctx context.Context) bool {
	if o == nil {
		return true
	}

	return o.ObjectBase.IsZero(ctx) &&
		(o.Current == nil || o.Current.IsZero(ctx)) &&
		(o.First == nil || o.First.IsZero(ctx)) &&
		(o.Last == nil || o.Last.IsZero(ctx)) &&
		(o.Items == nil || o.Items.IsZero(ctx)) &&
		o.TotalItems == 0
}

// MarshalFields implementation
func (o *Collection) MarshalFields(ctx context.Context, w *jwriter.Writer, first, ordered bool) bool {
	first = o.ObjectBase.MarshalFields(ctx, w, first)
	first = JSONMarshalField(ctx, w, o.Current, "current", first)
	first = JSONMarshalField(ctx, w, o.First, "first", first)
	first = JSONMarshalField(ctx, w, o.Last, "last", first)

	if ordered {
		first = JSONMarshalField(ctx, w, o.Items, "orderedItems", first)
	} else {
		first = JSONMarshalField(ctx, w, o.Items, "items", first)
	}

	first = JSONMarshalUint(w, o.TotalItems, "totalItems", first)

	return first
}

// UnmarshalFields implementation
func (o *Collection) UnmarshalFields(ctx context.Context, r *jlexer.Lexer) {
	o.ObjectBase.UnmarshalFields(ctx, r)

	var nr jlexer.Lexer

	var nraw ExtraRaw

	for _, v := range o.Extra.Raw {
		nr = jlexer.Lexer{Data: v.Value}

		switch v.Key {
		case "current":
			JSONUnmarshalItem(ctx, &nr, &o.Current, false)
		case "first":
			JSONUnmarshalItem(ctx, &nr, &o.First, false)
		case "last":
			JSONUnmarshalItem(ctx, &nr, &o.Last, false)
		case "items", "orderedItems":
			JSONUnmarshalItem(ctx, &nr, &o.Items, false)
		case "totalItems":
			o.TotalItems = nr.Uint()
		default:
			nraw.Append(v.Key, v.Value)

			continue
		}

		r.AddError(nr.Error())
	}

	o.Extra.Raw = nraw
}

// MarshalJSONContext implementation
func (o *Collection) MarshalJSONContext(ctx context.Context, w *jwriter.Writer) {
	w.RawByte('{')

	o.MarshalFields(ctx, w, true, false)

	w.RawByte('}')
}

// UnmarshalJSONContext implementation
func (o *Collection) UnmarshalJSONContext(ctx context.Context, r *jlexer.Lexer) {
	o.Extra.UnmarshalJSONContext(ctx, r)
	o.UnmarshalFields(ctx, r)
}
