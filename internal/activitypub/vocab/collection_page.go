package vocab

import (
	"context"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

// CollectionPage Used to represent distinct subsets of items from a Collection.
//
// https://www.w3.org/TR/activitystreams-vocabulary/#dfn-collectionpage
type CollectionPage struct {
	// Identifies the Collection to which a CollectionPage objects items belong.
	PartOf Item
	// In a paged Collection, indicates the next page of items.
	Next Item
	// In a paged Collection, identifies the previous page of items.
	Prev Item
	// Collection inherited fields
	Collection
	// A non-negative integer value identifying the relative position within the logical view of
	// a strictly ordered collection.
	StartIndex uint
}

// Merge implementation
func (o *CollectionPage) Merge(raw Item) {
	other, _ := raw.(*CollectionPage)
	if other == nil {
		return
	}

	if other.PartOf != nil {
		o.PartOf = other.PartOf
	}

	if other.Next != nil {
		o.Next = other.Next
	}

	if other.Prev != nil {
		o.Prev = other.Prev
	}

	o.StartIndex = other.StartIndex

	o.Collection.Merge(&other.Collection)
}

// AsIRI implementation
func (o *CollectionPage) AsIRI() IRI {
	return o.Collection.AsIRI()
}

// AsType implementation
func (o *CollectionPage) AsType() Type {
	return o.ObjectBase.AsType()
}

// AsBool implementation
func (o *CollectionPage) AsBool() bool {
	return o.Collection.AsBool()
}

// IsZero implementation
func (o *CollectionPage) IsZero(ctx context.Context) bool {
	if o == nil {
		return true
	}

	return o.Collection.IsZero(ctx) &&
		(o.PartOf == nil || o.PartOf.IsZero(ctx)) &&
		(o.Next == nil || o.Next.IsZero(ctx)) &&
		(o.Preview == nil || o.Preview.IsZero(ctx)) &&
		o.StartIndex == 0
}

// MarshalFields implementation
func (o *CollectionPage) MarshalFields(ctx context.Context, w *jwriter.Writer, first, ordered bool) bool {
	first = o.Collection.MarshalFields(ctx, w, first, ordered)
	first = JSONMarshalField(ctx, w, o.PartOf, "partOf", first)
	first = JSONMarshalField(ctx, w, o.Next, "next", first)
	first = JSONMarshalField(ctx, w, o.Prev, "prev", first)
	first = JSONMarshalUint(w, o.StartIndex, "startIndex", first)

	return first
}

// UnmarshalFields implementation
func (o *CollectionPage) UnmarshalFields(ctx context.Context, r *jlexer.Lexer) {
	o.Collection.UnmarshalFields(ctx, r)

	var nr jlexer.Lexer

	var nraw ExtraRaw

	for _, v := range o.Extra.Raw {
		nr = jlexer.Lexer{Data: v.Value}

		switch v.Key {
		case "partOf":
			JSONUnmarshalItem(ctx, &nr, &o.PartOf, false)
		case "next":
			JSONUnmarshalItem(ctx, &nr, &o.Next, false)
		case "prev":
			JSONUnmarshalItem(ctx, &nr, &o.Prev, false)
		case "startIndex":
			o.StartIndex = nr.Uint()
		default:
			nraw.Append(v.Key, v.Value)

			continue
		}

		r.AddError(nr.Error())
	}

	o.Extra.Raw = nraw
}

// MarshalJSONContext implementation
func (o *CollectionPage) MarshalJSONContext(ctx context.Context, w *jwriter.Writer) {
	w.RawByte('{')

	o.MarshalFields(ctx, w, true, false)

	w.RawByte('}')
}

// UnmarshalJSONContext implementation
func (o *CollectionPage) UnmarshalJSONContext(ctx context.Context, r *jlexer.Lexer) {
	o.Extra.UnmarshalJSONContext(ctx, r)
	o.UnmarshalFields(ctx, r)
}
