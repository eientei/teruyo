package vocab

import (
	"context"
	"errors"
	"net/url"
	"strings"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

var errInvalidIRI = errors.New("invalid IRI")

// IRI Internationalized Resource Identifier as defined by RFC3987
type IRI string

// Merge implementation
func (iri IRI) Merge(Item) {

}

// AsIRI implementation
func (iri IRI) AsIRI() IRI {
	return iri
}

// AsType implementation
func (iri IRI) AsType() Type {
	return ""
}

// AsBool implementation
func (iri IRI) AsBool() bool {
	return iri != ""
}

// IsZero implementation
func (iri IRI) IsZero(context.Context) bool {
	return iri == ""
}

// IsLocal returns true if IRI is referencing local node
func (iri IRI) IsLocal() bool {
	return strings.HasPrefix(string(iri), DIDAPNodeLocal)
}

// MarshalJSONContext implementation
func (iri IRI) MarshalJSONContext(ctx context.Context, w *jwriter.Writer) {
	cfg := ConfigFromContext(ctx)

	s := string(iri)

	switch {
	case cfg.OperationContext == OperationContextLocal && strings.HasPrefix(s, string(cfg.Instance)):
		w.String(DIDAPNodeLocal + strings.TrimPrefix(s, string(cfg.Instance)))

		return
	case cfg.OperationContext == OperationContextFederation && iri.IsLocal():
		w.String(string(cfg.Instance) + strings.TrimPrefix(s, DIDAPNodeLocal))

		return
	}

	w.String(string(iri))
}

// UnmarshalJSONContext implementation
func (iri *IRI) UnmarshalJSONContext(ctx context.Context, w *jlexer.Lexer) {
	cfg := ConfigFromContext(ctx)

	parsed := IRI(w.String())

	switch {
	case cfg.OperationContext == OperationContextFederation && parsed.IsLocal():
		w.AddError(errInvalidIRI)
	case cfg.OperationContext == OperationContextLocal && strings.HasPrefix(string(parsed), string(cfg.Instance)):
		parsed = DIDAPNodeLocal + IRI(strings.TrimPrefix(string(parsed), string(cfg.Instance)))
	}

	*iri = parsed
}

// Domain part of IRI
func (iri IRI) Domain() IRI {
	if iri.IsLocal() {
		return DIDAPNodeLocal
	}

	u, err := url.Parse(string(iri))
	if err != nil {
		return iri
	}

	if u.Host != "" {
		return IRI(u.Host)
	}

	if u.Opaque != "" && u.Scheme == "did" {
		idx := strings.IndexAny(string(iri), "/?#")
		if idx == -1 {
			return iri
		}

		return iri[:idx]
	}

	return iri
}

// IsCollection returns true if iri last segment matches
func (iri IRI) IsCollection(s string) bool {
	return strings.HasSuffix(strings.TrimRight(string(iri), "/"), "/"+s)
}

// Parent IRI path
func (iri IRI) Parent() IRI {
	s := strings.TrimRight(string(iri), "/")
	idx := strings.LastIndexByte(s, '/')

	if idx < 0 {
		return ""
	}

	return IRI(s[:idx])
}
