package vocab

import (
	"context"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

// CollectionOrderedPage Used to represent ordered subsets of items from an OrderedCollection.
// Refer to the Activity Streams 2.0 Core for a complete description of the OrderedCollectionPage object.
//
// https://www.w3.org/TR/activitystreams-vocabulary/#dfn-orderedcollectionpage
type CollectionOrderedPage CollectionPage

// Merge implementation
func (o *CollectionOrderedPage) Merge(raw Item) {
	other, _ := raw.(*CollectionOrderedPage)
	if other != nil {
		raw = (*CollectionPage)(other)
	}

	(*CollectionPage)(o).Merge(raw)
}

// AsIRI implementation
func (o *CollectionOrderedPage) AsIRI() IRI {
	return (*CollectionPage)(o).AsIRI()
}

// AsType implementation
func (o *CollectionOrderedPage) AsType() Type {
	return (*CollectionPage)(o).AsType()
}

// IsZero implementation
func (o *CollectionOrderedPage) IsZero(ctx context.Context) bool {
	return (*CollectionPage)(o).IsZero(ctx)
}

// MarshalFields implementation
func (o *CollectionOrderedPage) MarshalFields(ctx context.Context, w *jwriter.Writer, first, ordered bool) bool {
	return (*CollectionPage)(o).MarshalFields(ctx, w, first, ordered)
}

// UnmarshalFields implementation
func (o *CollectionOrderedPage) UnmarshalFields(ctx context.Context, r *jlexer.Lexer) {
	(*CollectionPage)(o).UnmarshalFields(ctx, r)
}

// MarshalJSONContext implementation
func (o *CollectionOrderedPage) MarshalJSONContext(ctx context.Context, w *jwriter.Writer) {
	w.RawByte('{')

	(*CollectionPage)(o).MarshalFields(ctx, w, true, true)

	w.RawByte('}')
}

// UnmarshalJSONContext implementation
func (o *CollectionOrderedPage) UnmarshalJSONContext(ctx context.Context, r *jlexer.Lexer) {
	o.Extra.UnmarshalJSONContext(ctx, r)
	o.UnmarshalFields(ctx, r)
}
