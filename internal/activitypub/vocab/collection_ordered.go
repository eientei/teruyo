package vocab

import (
	"context"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

// CollectionOrdered A subtype of Collection in which members of the logical collection are assumed to
// always be strictly ordered.
//
// https://www.w3.org/TR/activitystreams-vocabulary/#dfn-orderedcollection
type CollectionOrdered Collection

// Merge implementation
func (o *CollectionOrdered) Merge(raw Item) {
	other, _ := raw.(*CollectionOrdered)
	if other != nil {
		raw = (*Collection)(other)
	}

	(*Collection)(o).Merge(raw)
}

// AsIRI implementation
func (o *CollectionOrdered) AsIRI() IRI {
	return (*Collection)(o).AsIRI()
}

// AsType implementation
func (o *CollectionOrdered) AsType() Type {
	return (*Collection)(o).AsType()
}

// AsBool implementation
func (o *CollectionOrdered) AsBool() bool {
	return (*Collection)(o).AsBool()
}

// IsZero implementation
func (o *CollectionOrdered) IsZero(ctx context.Context) bool {
	return (*Collection)(o).IsZero(ctx)
}

// MarshalFields implementation
func (o *CollectionOrdered) MarshalFields(ctx context.Context, w *jwriter.Writer, first, ordered bool) bool {
	return (*Collection)(o).MarshalFields(ctx, w, first, ordered)
}

// UnmarshalFields implementation
func (o *CollectionOrdered) UnmarshalFields(ctx context.Context, r *jlexer.Lexer) {
	(*Collection)(o).UnmarshalFields(ctx, r)
}

// MarshalJSONContext implementation
func (o *CollectionOrdered) MarshalJSONContext(ctx context.Context, w *jwriter.Writer) {
	w.RawByte('{')

	(*Collection)(o).MarshalFields(ctx, w, true, true)

	w.RawByte('}')
}

// UnmarshalJSONContext implementation
func (o *CollectionOrdered) UnmarshalJSONContext(ctx context.Context, r *jlexer.Lexer) {
	o.Extra.UnmarshalJSONContext(ctx, r)
	o.UnmarshalFields(ctx, r)
}
