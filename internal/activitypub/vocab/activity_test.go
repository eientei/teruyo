package vocab

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

var dataActivityFilled = Activity{
	Actor:      IRI("b1"),
	Object:     IRI("b2"),
	Target:     IRI("b3"),
	Result:     IRI("b4"),
	Origin:     IRI("b5"),
	Instrument: Items{IRI("b6")},
	ObjectBase: ObjectBase(dataObjectFilled),
}

const dataExample3 = `
{
  "@context": "https://www.w3.org/ns/activitystreams",
  "type": "Activity",
  "summary": "Sally did something to a note",
  "actor": {
    "type": "Person",
    "name": "Sally"
  },
  "object": {
    "type": "Note",
    "name": "A Note"
  }
}
`

func TestActivityMarshalEmpty(t *testing.T) {
	link := &Activity{}

	bs, err := MarshalJSONContext(context.Background(), link)
	assert.NoError(t, err)

	assert.Equal(t, `{}`, string(bs))

	err = UnmarshalJSONContext(context.Background(), bs, link)
	assert.NoError(t, err)
}

func TestActivityMarshalFilled(t *testing.T) {
	ctx := WithConfig(context.Background(), &Config{Instance: "example.com", OperationContext: OperationContextLocal})

	bs, err := MarshalJSONContext(ctx, &dataActivityFilled)
	assert.NoError(t, err)

	var b Activity

	err = UnmarshalJSONContext(ctx, bs, &b)
	assert.NoError(t, err)

	b.AsDataResetRaw()

	assert.EqualValues(t, dataActivityFilled, b)
}

func TestActivityUnmarshalExample3(t *testing.T) {
	a := &Activity{
		Actor: &Actor{
			ObjectBase: ObjectBase{
				Type: TypeActorPerson,
				Name: ValueString("Sally"),
			},
		},
		Object: &Object{
			Type: TypeObjectNote,
			Name: ValueString("A Note"),
		},
		Target:     nil,
		Result:     nil,
		Origin:     nil,
		Instrument: nil,
		ObjectBase: ObjectBase{
			JSONLDContext: JSONLDContextString("https://www.w3.org/ns/activitystreams"),
			Type:          TypeActivity,
			Summary:       ValueString("Sally did something to a note"),
		},
	}

	var b Activity

	err := UnmarshalJSONContext(context.Background(), ([]byte)(dataExample3), &b)
	assert.NoError(t, err)

	assert.EqualValues(t, a, &b)
}

func FuzzActivityUnmarshal(f *testing.F) {
	bs, err := MarshalJSONContext(context.Background(), &dataActivityFilled)
	assert.NoError(f, err)

	f.Add(([]byte)(dataExample3))
	f.Add(bs)

	f.Fuzz(func(t *testing.T, data []byte) {
		var v Activity

		_ = UnmarshalJSONContext(context.Background(), data, &v)
	})
}
