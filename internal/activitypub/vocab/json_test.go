package vocab

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestJSONStart(t *testing.T) {
	var v Object

	err := UnmarshalJSONContext(context.Background(), ([]byte)(`null`), &v)
	assert.NoError(t, err)
}

func TestJSONEnd(t *testing.T) {
	var v Object

	err := UnmarshalJSONContext(context.Background(), ([]byte)(``), &v)
	assert.Error(t, err)
}

func TestJSONUnmarshalTime(t *testing.T) {
	var v Object

	err := UnmarshalJSONContext(context.Background(), ([]byte)(`{"startTime": "abc"}`), &v)
	assert.Error(t, err)
}

func TestJSONUnmarshalDuration(t *testing.T) {
	var v Object

	err := UnmarshalJSONContext(context.Background(), ([]byte)(`{"duration": "abc"}`), &v)
	assert.Error(t, err)
}
