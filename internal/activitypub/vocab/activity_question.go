package vocab

import (
	"context"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

// ActivityQuestion Represents a question being asked. Question objects are an extension of IntransitiveActivity.
// That is, the Question object is an Activity, but the direct object is the question itself and therefore it
// would not contain an object property.
//
// Either of the anyOf and oneOf properties MAY be used to express possible answers,
// but a Question object MUST NOT have both properties.
type ActivityQuestion struct {
	// Identifies an exclusive option for a Question.
	// Use of oneOf implies that the Question can have only a single answer.
	// To indicate that a Question can have multiple answers, use anyOf.
	OneOf Item
	// Identifies an inclusive option for a Question.
	// Use of anyOf implies that the Question can have multiple answers.
	// To indicate that a Question can have only one answer, use oneOf.
	AnyOf Item
	// Indicates that a question has been closed, and answers are no longer accepted.
	Closed Item
	// ActivityIntransitive inherited fields
	ActivityIntransitive
}

// Merge implementation
func (o *ActivityQuestion) Merge(raw Item) {
	other, _ := raw.(*ActivityQuestion)
	if other == nil {
		return
	}

	if other.OneOf != nil {
		o.OneOf = other.OneOf
	}

	if other.AnyOf != nil {
		o.AnyOf = other.AnyOf
	}

	if other.Closed != nil {
		o.Closed = other.Closed
	}

	o.ActivityIntransitive.Merge(&other.ActivityIntransitive)
}

// AsIRI implementation
func (o *ActivityQuestion) AsIRI() IRI {
	return o.ActivityIntransitive.AsIRI()
}

// AsType implementation
func (o *ActivityQuestion) AsType() Type {
	return o.ActivityIntransitive.Type
}

// AsBool implementation
func (o *ActivityQuestion) AsBool() bool {
	return o.ActivityIntransitive.AsBool()
}

// IsZero implementation
func (o *ActivityQuestion) IsZero(ctx context.Context) bool {
	return o.ActivityIntransitive.IsZero(ctx) &&
		(o.OneOf == nil || o.OneOf.IsZero(ctx)) &&
		(o.AnyOf == nil || o.AnyOf.IsZero(ctx)) &&
		o.Closed.IsZero(ctx)
}

// MarshalFields implementation
func (o *ActivityQuestion) MarshalFields(ctx context.Context, w *jwriter.Writer, first bool) bool {
	first = o.ObjectBase.MarshalFields(ctx, w, first)
	first = JSONMarshalField(ctx, w, o.OneOf, "oneOf", first)
	first = JSONMarshalField(ctx, w, o.AnyOf, "anyOf", first)
	first = JSONMarshalField(ctx, w, o.Closed, "closed", first)

	return first
}

// UnmarshalFields implementation
func (o *ActivityQuestion) UnmarshalFields(ctx context.Context, r *jlexer.Lexer) {
	o.ObjectBase.UnmarshalFields(ctx, r)

	var nr jlexer.Lexer

	var nraw ExtraRaw

	for _, v := range o.Extra.Raw {
		nr = jlexer.Lexer{Data: v.Value}

		switch v.Key {
		case "oneOf":
			JSONUnmarshalItem(ctx, &nr, &o.OneOf, false)
		case "anyOf":
			JSONUnmarshalItem(ctx, &nr, &o.AnyOf, false)
		case "closed":
			JSONUnmarshalItem(ctx, &nr, &o.Closed, true)
		default:
			nraw.Append(v.Key, v.Value)

			continue
		}

		r.AddError(nr.Error())
	}

	o.Extra.Raw = nraw
}

// MarshalJSONContext implementation
func (o *ActivityQuestion) MarshalJSONContext(ctx context.Context, w *jwriter.Writer) {
	w.RawByte('{')

	o.MarshalFields(ctx, w, true)

	w.RawByte('}')
}

// UnmarshalJSONContext implementation
func (o *ActivityQuestion) UnmarshalJSONContext(ctx context.Context, r *jlexer.Lexer) {
	o.Extra.UnmarshalJSONContext(ctx, r)
	o.UnmarshalFields(ctx, r)
}
