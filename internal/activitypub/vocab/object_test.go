package vocab

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

var dataObjectFilled = Object{
	Extra: Extra{
		Data: map[string]interface{}{
			"foo": float64(1),
			"bar": "aaa",
			"baz": map[string]interface{}{
				"abc": "efg",
				"efg": float64(34),
			},
		},
	},
	JSONLDContext: JSONLDContextString("ggg"),
	Attachment:    IRI("a1"),
	AttributedTo:  IRI("a2"),
	Content:       ValueString("a3"),
	Context:       IRI("a4"),
	Name:          ValueMap{"en": "a5"},
	Generator:     IRI("a6"),
	Icon:          IRI("a7"),
	Image:         IRI("a8"),
	InReplyTo: &Object{
		ID: "a9",
	},
	Location:      IRI("a10"),
	Preview:       IRI("a11"),
	Published:     Time(time.Unix(150, 0)),
	Replies:       IRI("a13"),
	Summary:       ValueString("a14"),
	URL:           IRI("a15"),
	Likes:         IRI("a16"),
	Shares:        IRI("a17"),
	Source:        IRI("a18"),
	ID:            "a25",
	Type:          "a26",
	DirectMessage: IRI("e1"),
	ListMessage:   IRI("e2"),
	MediaType:     "a27",
	EndTime:       Time(time.Unix(100, 0)),
	StartTime:     Time(time.Unix(50, 0)),
	Updated:       Time(time.Unix(75, 0)),
	Audience: Items{
		IRI("a19-1"),
		&Object{
			ID: "a19-2",
		},
	},
	Tag:      Items{IRI("a20")},
	To:       Items{IRI("a21")},
	BTo:      Items{IRI("a22")},
	CC:       Items{IRI("a23")},
	BCC:      Items{IRI("a24")},
	Duration: time.Minute,
}

const dataExample1 = `
{
  "@context": "https://www.w3.org/ns/activitystreams",
  "type": "Object",
  "id": "http://www.test.example/object/1",
  "name": "A Simple, non-specific object"
}
`

func TestObjectMarshalEmpty(t *testing.T) {
	obj := &Object{}

	bs, err := MarshalJSONContext(context.Background(), obj)
	assert.NoError(t, err)

	assert.Equal(t, `{}`, string(bs))

	err = UnmarshalJSONContext(context.Background(), bs, obj)
	assert.NoError(t, err)
}

func TestObjectMarshalFilled(t *testing.T) {
	ctx := WithConfig(context.Background(), &Config{Instance: "example.com", OperationContext: OperationContextLocal})

	bs, err := MarshalJSONContext(ctx, &dataObjectFilled)
	assert.NoError(t, err)

	var b Object

	err = UnmarshalJSONContext(ctx, bs, &b)
	assert.NoError(t, err)

	b.AsDataResetRaw()

	assert.EqualValues(t, dataObjectFilled, b)
}

func TestObjectUnmarshalFilled(t *testing.T) {
	a := &Object{
		Extra: Extra{
			Data: map[string]interface{}{
				"foo": float64(1),
				"bar": "aaa",
				"baz": map[string]interface{}{
					"abc": "efg",
					"efg": float64(34),
				},
			},
		},
		JSONLDContext: JSONLDContextString("ggg"),
		Attachment:    IRI("a1"),
		AttributedTo:  IRI("a2"),
		Content:       ValueString("a3"),
		Context:       IRI("a4"),
		Name:          ValueMap{"en": "a5"},
		Generator:     IRI("a6"),
		Icon:          IRI("a7"),
		Image:         IRI("a8"),
		InReplyTo: &Object{
			ID: "a9",
		},
		Location:      IRI("a10"),
		Preview:       IRI("a11"),
		Published:     Time(time.Unix(150, 0)),
		Replies:       nil,
		Summary:       ValueString("a14"),
		URL:           IRI("a15"),
		Likes:         IRI("a16"),
		Shares:        IRI("a17"),
		Source:        IRI("a18"),
		ID:            "a25",
		Type:          "a26",
		DirectMessage: IRI("e1"),
		ListMessage:   IRI("e2"),
		MediaType:     "a27",
		EndTime:       Time(time.Unix(100, 0)),
		StartTime:     Time(time.Unix(50, 0)),
		Updated:       Time(time.Unix(75, 0)),
		Audience: Items{
			IRI("a19-1"),
			&Object{
				ID: "a19-2",
			},
		},
		Tag:      &Object{ID: "a20"},
		To:       Items{IRI("a21")},
		BTo:      Items{IRI("a22")},
		CC:       IRI("a23"),
		BCC:      Items{IRI("a24")},
		Duration: time.Minute,
	}

	const data = `
{
  "@context": "ggg",
  "attachment": "a1",
  "attributedTo": "a2",
  "content": "a3",
  "context": "a4",
  "nameMap": {
    "en": "a5"
  },
  "generator": "a6",
  "icon": "a7",
  "image": "a8",
  "inReplyTo": {
    "id": "a9"
  },
  "location": "a10",
  "preview": "a11",
  "published": "1970-01-01T03:02:30+03:00",
  "replies": null,
  "summary": "a14",
  "url": "a15",
  "likes": "a16",
  "shares": "a17",
  "source": "a18",
  "audience": [
    "a19-1",
    {
      "id": "a19-2"
    }
  ],
  "tag": {
    "id": "a20"
  },
  "to": [
    "a21"
  ],
  "bto": [
    "a22"
  ],
  "cc": "a23",
  "bcc": [
    "a24"
  ],
  "id": "a25",
  "type": "a26",
  "mediaType": "a27",
  "directMessage": "e1",
  "listMessage": "e2",
  "endTime": "1970-01-01T03:01:40+03:00",
  "startTime": "1970-01-01T03:00:50+03:00",
  "updated": "1970-01-01T03:01:15+03:00",
  "duration": "PT1M",
  "foo": 1,
  "bar": "aaa",
  "baz": {
    "abc": "efg",
    "efg": 34
  }
}
`

	var b Object

	err := UnmarshalJSONContext(context.Background(), ([]byte)(data), &b)
	assert.NoError(t, err)

	b.AsDataResetRaw()

	assert.EqualValues(t, a, &b)
}

func TestObjectUnmarshalExample1(t *testing.T) {
	a := &Object{
		JSONLDContext: JSONLDContextString("https://www.w3.org/ns/activitystreams"),
		Type:          TypeObject,
		ID:            IRI("http://www.test.example/object/1"),
		Name:          ValueString("A Simple, non-specific object"),
	}

	var b Object

	err := UnmarshalJSONContext(context.Background(), ([]byte)(dataExample1), &b)
	assert.NoError(t, err)

	assert.EqualValues(t, a, &b)
}

func FuzzObjectUnmarshal(f *testing.F) {
	bs, err := MarshalJSONContext(context.Background(), &dataObjectFilled)
	assert.NoError(f, err)

	f.Add(([]byte)(dataExample1))
	f.Add(bs)

	f.Fuzz(func(t *testing.T, data []byte) {
		var v Activity

		_ = UnmarshalJSONContext(context.Background(), data, &v)
	})
}
