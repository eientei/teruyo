package vocab

import (
	"context"

	"github.com/mailru/easyjson/jwriter"
)

// Items in-line collections of Items
type Items []Item

// AsIRI implementation
func (items Items) AsIRI() IRI {
	if len(items) == 1 {
		return items[0].AsIRI()
	}

	return ""
}

// AsBool implementation
func (items Items) AsBool() bool {
	if len(items) == 1 {
		return items[0].AsBool()
	}

	return len(items) > 0
}

// AsType implementation
func (items Items) AsType() Type {
	if len(items) == 1 {
		return items[0].AsType()
	}

	return TypeCollection
}

// Merge implementation
func (items Items) Merge(Item) {

}

// MarshalJSONContext implementation
func (items Items) MarshalJSONContext(ctx context.Context, w *jwriter.Writer) {
	w.RawString(`[`)

	for i, item := range items {
		if i > 0 {
			w.RawByte(',')
		}

		item.MarshalJSONContext(ctx, w)
	}

	w.RawByte(']')
}

// IsZero implementation
func (items Items) IsZero(context.Context) bool {
	return items == nil
}
