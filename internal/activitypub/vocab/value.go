package vocab

import (
	"context"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

// Value represents potentially language-mapped value
type Value interface {
	String(lang string) string
	Exists(lang string) bool
	MarshalerContext
}

// JSONUnmarshalValue unmarshals Value
func JSONUnmarshalValue(ctx context.Context, r *jlexer.Lexer, value *Value) {
	switch {
	case r.IsNull():
		r.Skip()

		*value = nil

	case r.IsDelim('{'):
		var obj ValueMap

		obj.UnmarshalJSONContext(ctx, r)

		*value = obj
	case r.CurrentToken() == jlexer.TokenString:
		var obj ValueString

		obj.UnmarshalJSONContext(ctx, r)

		*value = obj
	default:
		r.AddError(errInvalidJSON)
	}
}

// JSONMarshalValues marshals list of Value
func JSONMarshalValues(ctx context.Context, w *jwriter.Writer, values []Value, field string, first bool) bool {
	if len(values) == 0 {
		return first
	}

	if first {
		w.RawByte('"')
	} else {
		w.RawString(`,"`)
	}

	w.RawString(field)
	w.RawString(`":[`)

	for i, value := range values {
		if i > 0 {
			w.RawByte(',')
		}

		value.MarshalJSONContext(ctx, w)
	}

	w.RawByte(']')

	return false
}

// JSONUnmarshalValues unmarshals list of Value
func JSONUnmarshalValues(ctx context.Context, r *jlexer.Lexer, res *[]Value) {
	start := r.IsStart()

	if !JSONStart(r) {
		*res = nil

		return
	}

	switch {
	case r.IsDelim('{'):
		var value Value

		JSONUnmarshalValue(ctx, r, &value)

		*res = []Value{value}
	case r.CurrentToken() == jlexer.TokenString:
		*res = []Value{ValueString(r.String())}
	case r.IsDelim('['):
		r.Delim('[')

		if *res == nil {
			if !r.IsDelim(']') {
				*res = make([]Value, 0, 4)
			} else {
				*res = []Value{}
			}
		} else {
			*res = (*res)[:0]
		}

		for r.Ok() && !r.IsDelim(']') {
			var value Value

			JSONUnmarshalValue(ctx, r, &value)

			*res = append(*res, value)

			r.WantComma()
		}

		r.Delim(']')
	default:
		r.AddError(errInvalidJSON)
	}

	JSONEnd(r, start)
}

// ValueString implements Value for simple unmapped string
type ValueString string

// IsZero implementation
func (value ValueString) IsZero(context.Context) bool {
	return value == ""
}

// String will always return itself
func (value ValueString) String(string) string {
	return string(value)
}

// Exists will always return true
func (value ValueString) Exists(string) bool {
	return true
}

// MarshalJSONContext implementation
func (value ValueString) MarshalJSONContext(_ context.Context, w *jwriter.Writer) {
	w.String(string(value))
}

// UnmarshalJSONContext implementation
func (value *ValueString) UnmarshalJSONContext(_ context.Context, r *jlexer.Lexer) {
	*value = ValueString(r.String())
}

// ValueMap implements language-mapped value dispatch
type ValueMap map[string]string

// IsZero implementation
func (value ValueMap) IsZero(context.Context) bool {
	return len(value) == 0
}

// String will return matching value or empty string
func (value ValueMap) String(lang string) string {
	return value[lang]
}

// Exists will return true if mapping for lang is defined
func (value ValueMap) Exists(lang string) (ok bool) {
	_, ok = value[lang]

	return
}

// MarshalJSONContext implementation
func (value ValueMap) MarshalJSONContext(_ context.Context, w *jwriter.Writer) {
	if value == nil {
		w.RawString("null")

		return
	}

	w.RawByte('{')

	first := true

	for k, v := range value {
		if !first {
			w.RawByte(',')
		}

		first = false

		w.String(k)
		w.RawByte(':')
		w.String(v)
	}

	w.RawByte('}')
}

// UnmarshalJSONContext implementation
func (value *ValueMap) UnmarshalJSONContext(_ context.Context, r *jlexer.Lexer) {
	start := r.IsStart()

	if !JSONStart(r) {
		*value = nil

		return
	}

	if *value == nil {
		*value = make(ValueMap)
	}

	r.Delim('{')

	for r.Ok() && !r.IsDelim('}') {
		key := r.String()

		r.WantColon()

		if r.IsNull() {
			r.Skip()
			r.WantComma()

			continue
		}

		(*value)[key] = r.String()

		r.WantComma()
	}

	r.Delim('}')

	JSONEnd(r, start)
}
