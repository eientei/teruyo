package vocab

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestActorMarshalEmpty(t *testing.T) {
	link := &Actor{}

	bs, err := MarshalJSONContext(context.Background(), link)
	assert.NoError(t, err)

	assert.Equal(t, `{}`, string(bs))

	err = UnmarshalJSONContext(context.Background(), bs, link)
	assert.NoError(t, err)
}

func TestActorMarshalFilled(t *testing.T) {
	a := Actor{
		Inbox:             IRI("b1"),
		Outbox:            IRI("b2"),
		Following:         IRI("b3"),
		Followers:         IRI("b4"),
		Liked:             IRI("b5"),
		Streams:           Items{IRI("b6")},
		PreferredUsername: "b7",
		Endpoints: ActorEndpoints{
			Extra: Extra{
				Data: ExtraData{
					"a": float64(34),
				},
			},
			JSONLDContext:              JSONLDContextString("qq"),
			ProxyURL:                   "c1",
			OauthAuthorizationEndpoint: "c2",
			OauthTokenEndpoint:         "c3",
			ProvideClientKey:           "c4",
			SignClientKey:              "c5",
			SharedInbox:                "c6",
		},
		ObjectBase: ObjectBase(dataObjectFilled),
	}

	ctx := WithConfig(context.Background(), &Config{Instance: "example.com", OperationContext: OperationContextLocal})

	bs, err := MarshalJSONContext(ctx, &a)
	assert.NoError(t, err)

	var b Actor

	err = UnmarshalJSONContext(ctx, bs, &b)
	assert.NoError(t, err)

	b.AsDataResetRaw()
	b.Endpoints.AsDataResetRaw()

	assert.EqualValues(t, a, b)
}
