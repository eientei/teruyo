package vocab

import (
	"context"
	"time"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

// Time provides wrapper for time.Time implementing Item and Closed interface
type Time time.Time

// Merge implementation
func (t Time) Merge(Item) {

}

// AsTime returns time.Time
func (t Time) AsTime() time.Time {
	return time.Time(t)
}

// AsBool implementation
func (t Time) AsBool() bool {
	return time.Time(t).Unix() > 0
}

// IsZero implementation
func (t Time) IsZero(context.Context) bool {
	return time.Time(t).Unix() <= 0
}

// String implementation
func (t Time) String() string {
	return time.Time(t).Format(time.RFC3339)
}

// AsIRI implementation
func (t Time) AsIRI() IRI {
	return IRI(t.String())
}

// AsType implementation
func (t Time) AsType() Type {
	return ""
}

// MarshalJSONContext implementation
func (t Time) MarshalJSONContext(_ context.Context, w *jwriter.Writer) {
	w.String(t.String())
}

// UnmarshalJSONContext implementation
func (t *Time) UnmarshalJSONContext(_ context.Context, r *jlexer.Lexer) {
	start := r.IsStart()

	if !JSONStart(r) {
		*t = Time{}

		return
	}

	c, err := time.Parse(time.RFC3339, r.String())
	if err != nil {
		r.AddNonFatalError(err)

		return
	}

	*t = Time(c)

	JSONEnd(r, start)
}
