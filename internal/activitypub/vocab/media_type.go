package vocab

import (
	"context"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

// MediaType MIME type
//
// When used on a Link, identifies the MIME media type of the referenced resource.
//
// When used on an Object, identifies the MIME media type of the value of the content property. If not specified,
// the content property is assumed to contain text/html content.
type MediaType string

// IsZero implementation
func (t MediaType) IsZero(context.Context) bool {
	return t == ""
}

// MarshalJSONContext implementation
func (t MediaType) MarshalJSONContext(_ context.Context, w *jwriter.Writer) {
	w.String(string(t))
}

// UnmarshalJSONContext implementation
func (t *MediaType) UnmarshalJSONContext(_ context.Context, j *jlexer.Lexer) {
	*t = MediaType(j.String())
}
