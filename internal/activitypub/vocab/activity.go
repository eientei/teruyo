package vocab

import (
	"context"

	"github.com/mailru/easyjson/jlexer"
	"github.com/mailru/easyjson/jwriter"
)

// Activity is a subtype of Object that describes some form of action that may happen, is currently happening,
// or has already happened. The Activity type itself serves as an abstract base type for all types of activities.
// It is important to note that the Activity type itself does not carry any specific semantics about the kind of
// action being taken.
//
// https://www.w3.org/TR/activitystreams-vocabulary/#dfn-activity
type Activity struct {
	// Describes one or more entities that either performed or are expected to perform the activity.
	// Any single activity can have multiple actors. The actor MAY be specified using an indirect Link.
	Actor Item
	// Describes an object of any kind.
	// The Object type serves as the base type for most of the other kinds of objects defined in the
	// Activity Vocabulary, including other Core types such as Activity, ActivityIntransitive, Collection and
	// CollectionOrdered.
	Object Item
	// Describes the indirect object, or target, of the activity.
	// The precise meaning of the target is largely dependent on the type of action being described but will
	// often be the object of the English preposition "to". For instance, in the activity "John added a movie to
	// his wishlist", the target of the activity is John's wishlist. An activity can have more than one target.
	Target Item
	// Describes the result of the activity.
	// For instance, if a particular action results in the creation of a new resource, the result property can
	// be used to describe that new resource.
	Result Item
	// Describes an indirect object of the activity from which the activity is directed.
	// The precise meaning of the origin is the object of the English preposition "from".
	// For instance, in the activity "John moved an item to List B from List A", the origin of the activity is
	// "List A".
	Origin Item
	// Identifies one or more objects used (or to be used) in the completion of an Activity.
	Instrument Item
	// ObjectBase inherited fields
	ObjectBase
}

// AsIRI implementation
func (o *Activity) AsIRI() IRI {
	return o.ID
}

// AsType implementation
func (o *Activity) AsType() Type {
	return o.ObjectBase.AsType()
}

// AsBool implementation
func (o *Activity) AsBool() bool {
	return o.ID != ""
}

// IsZero implementation
func (o *Activity) IsZero(ctx context.Context) bool {
	if o == nil {
		return true
	}

	return o.ObjectBase.IsZero(ctx) &&
		(o.Actor == nil || o.Actor.IsZero(ctx)) &&
		(o.Object == nil || o.Object.IsZero(ctx)) &&
		(o.Target == nil || o.Target.IsZero(ctx)) &&
		(o.Result == nil || o.Result.IsZero(ctx)) &&
		(o.Origin == nil || o.Origin.IsZero(ctx)) &&
		(o.Instrument == nil || o.InReplyTo.IsZero(ctx))
}

// Merge implementation
func (o *Activity) Merge(raw Item) {
	other, _ := raw.(*Activity)
	if other == nil {
		return
	}

	if other.Actor != nil {
		o.Actor = other.Actor
	}

	if other.Object != nil {
		o.Object = other.Object
	}

	if other.Target != nil {
		o.Target = other.Target
	}

	if other.Result != nil {
		o.Result = other.Result
	}

	if other.Origin != nil {
		o.Origin = other.Origin
	}

	if other.Instrument != nil {
		o.Instrument = other.Instrument
	}
}

// MarshalFields implementation
func (o *Activity) MarshalFields(ctx context.Context, w *jwriter.Writer, first bool) bool {
	first = o.ObjectBase.MarshalFields(ctx, w, first)
	first = JSONMarshalField(ctx, w, o.Actor, "actor", first)
	first = JSONMarshalField(ctx, w, o.Object, "object", first)
	first = JSONMarshalField(ctx, w, o.Target, "target", first)
	first = JSONMarshalField(ctx, w, o.Result, "result", first)
	first = JSONMarshalField(ctx, w, o.Origin, "origin", first)
	first = JSONMarshalField(ctx, w, o.Instrument, "instrument", first)

	return first
}

// UnmarshalFields implementation
func (o *Activity) UnmarshalFields(ctx context.Context, r *jlexer.Lexer) {
	o.ObjectBase.UnmarshalFields(ctx, r)

	var nr jlexer.Lexer

	var nraw ExtraRaw

	for _, v := range o.Extra.Raw {
		nr = jlexer.Lexer{Data: v.Value}

		switch v.Key {
		case "actor":
			JSONUnmarshalItem(ctx, &nr, &o.Actor, false)
		case "object":
			JSONUnmarshalItem(ctx, &nr, &o.Object, false)
		case "target":
			JSONUnmarshalItem(ctx, &nr, &o.Target, false)
		case "result":
			JSONUnmarshalItem(ctx, &nr, &o.Result, false)
		case "origin":
			JSONUnmarshalItem(ctx, &nr, &o.Origin, false)
		case "instrument":
			JSONUnmarshalItem(ctx, &nr, &o.Instrument, false)
		default:
			nraw.Append(v.Key, v.Value)

			continue
		}

		r.AddError(nr.Error())
	}

	o.Extra.Raw = nraw
}

// MarshalJSONContext implementation
func (o *Activity) MarshalJSONContext(ctx context.Context, w *jwriter.Writer) {
	w.RawByte('{')

	o.MarshalFields(ctx, w, true)

	w.RawByte('}')
}

// UnmarshalJSONContext implementation
func (o *Activity) UnmarshalJSONContext(ctx context.Context, r *jlexer.Lexer) {
	o.Extra.UnmarshalJSONContext(ctx, r)
	o.UnmarshalFields(ctx, r)
}
